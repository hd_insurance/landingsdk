module.exports = {
  webpack5: true,
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.(jpe?g|png|gif|svg)$/i,
      loader: "file-loader",
    });

    return config;
  },
};
