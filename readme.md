## 🎉 Cấu trúc thư mục:
------------------------------

##### ***Main_Landing_Sdk***
- ***backend:*** Chứa code backend dùng chung cho landing
- ***components:*** Code các component dùng chung cho landing, ví dụ header, footer, faq ...
- ***landing_modules:*** chứa các module landing theo từng project
- ***pages:*** chứa các page
- ***views:*** Chứa các thư viện, input ...vvv
- ***sdk:***
- -	***src***
- - - ***sdk_component:*** chứa các component dùng cho sdk
- - - ***sdk_modules:*** chứa các module sdk theo từng project
- ***index.js:*** file index out sdk dùng cho landing và jquery libs``

-------------------------------


## Git:
- Các modules sdk và landing có thể tách riêng thành các git repo, config tại ***.gitmodules***
- Câu lệnh update: 
- - git submodule update
- Hoặc add 1 module
- - git submodule add [link git] [module name]
> Ví dụ: git submodule add https://hoangchimto@bitbucket.org/birdviet/sdk.git sdk



## 🖐 Getting Started

Node: 14.17.6
npm: 6.14.15

- Clone project ( điều kiện cần có ssh)
```bash
  git clone git@bitbucket.org:birdviet/landingsdk.git
  cd landingsdk
  git submodule update --init --recursive 

  or ---------

  git submodule update --remote
  git submodule -q foreach git pull -q origin master
  git submodule update --init
```

```
ERROR .gitmodule tren landing_modules

cd landing_moulde
git checkout master
git submodule update --init
Neu submodule van lay link hoangChimto thi merge HEAD voi master:
git checkout HEAD => git merge master => git submodule update --init
```
- Đảm bảo tất cả các repo on master

- Vào landingsdk
```bash
  cd landingsdk
  npm install
```
- Vào thư mục views 
```bash
  cd views
  sudo npm install
  sudo npm link ../node_modules/react
  sudo npm link react-pdf@5.4.0
  sudo npm link 
```
- Vào thư mục sdk
```bash
  sudo npm install
  sudo npm link ../node_modules/react
  sudo npm link ../views
  sudo npm link react-pdf@5.4.0
  sudo npm link 
  
```
- Ra thư mục landingsdk
```bash
  cd ..
  sudo npm link views ( or npm link uikit )
  sudo npm link sdk ( or npm link sdk-hdi )
  npm run dev
```



## Note
Nếu có lỗi xảy ra thử các cách sau:
```
  npm install react
  npm config set legacy-peer-deps true
  rm -rf .next
```
## Deploy product
```
  sudo ssh -i path_to_file/hdi-devpem.pem ec2-user@10.28.3.78
  (exp: sudo ssh -i /Users/user/Documents/HDI/Tools/hdi-devpem.pem ec2-user@10.28.3.78)
  cd to project
  pull code tu bitbucket
  npm run build
  npm start
  ( pm2 restart _ID )

```