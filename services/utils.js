import globalStore from "./globalStore";
import axios from "axios";
import https from "https";

const sdkdomain = `http://127.0.0.1:${process.env.PORT|| 6886}`

const getPageConfig = (path, pageconfig) => {
 if(pageconfig){
    const page_info = pageconfig.pages.filter(function (page) {
      return page.permalink == path.toLowerCase().trim();
    });
    const object_1 = globalStore.get("pageconfig").webconfig;
    const object_2 = page_info[0] ? page_info[0] : {};
    const object_out = { ...object_1, ...object_2 };
    return object_out;
  }else{
    return null
  }
  
};

const initPage = (domainhost, domainpath, language = "vi") => {
  return new Promise(async (resolve, reject) => {
    try {

      
      var domain = process.env.DM1;
      const pageProps = {};

      var web_instance_id = process.env.NEXT_PUBLIC_DEFAULT_WEB_INSTANCE; //default
      var version = "1.0.2"; //default
      const instance = axios.create({
        httpsAgent: new https.Agent({
          rejectUnauthorized: false,
        }),
      });

      const webinstances = await instance.get(`${sdkdomain}/api/pages-instance`);

      const instance_info = webinstances.data.data.filter(function (page) {
        if (domainhost) {
          return page.domain == domainhost.toLowerCase().trim();
        }
      });

      

      if (instance_info[0]) {
        web_instance_id = instance_info[0]._id;
        pageProps.page_org = instance_info[0].default_org;
        pageProps.page_version = instance_info[0].version;
        version = instance_info[0].version;
      } else {
        pageProps.page_org = process.env.NEXT_PUBLIC_DEFAULT_ORG_INSTANCE;
        version = process.env.NEXT_PUBLIC_DEFAULT_PAGE_VERSION;
        pageProps.page_version = version;
      }

      

      const list_allpage = await instance.get(`${sdkdomain}/api/get-all-page/${web_instance_id}`);


      const page_info = list_allpage.data.data.filter(function (page) {
        if (domainhost) {
          return page.permalink == domainpath.toLowerCase().trim();
        }
      });

      if(page_info[0]){
        const page_config_info = await instance.get(
          `${sdkdomain}/landing-api/page/${page_info[0]._id}?lng=${language}&version=${version}`
        );
        
        return resolve(page_config_info.data.data);
      }else{
        console.log("PAGE NOT FOUND ", domainhost, domainpath.toLowerCase().trim())
        return resolve({});
      }

      
    } catch (e) {
      console.log("GET INIT ERROR ", e);
      return resolve({});
    }
  });
};

const getPageComponent = (layout_id, selfhost = false) => {
  return new Promise(async (resolve, reject) => {
    try {
     
      const instance = axios.create({
        httpsAgent: new https.Agent({
          rejectUnauthorized: false,
        }),
      });
      var __server = sdkdomain
      if(selfhost){
        __server = ""
      }
      const component_object = await instance.get(`${__server}/api/layout-define/${layout_id}`);
     
      return resolve(component_object.data);
    } catch (e) {
      console.log("GET component_object error ", e.toJSON());
      return resolve({});
    }
  });
};

const getFormSDK = (form_id, selfhost = false, language = "vi") => {
  return new Promise(async (resolve, reject) => {
    try {
     
      const instance = axios.create({
        httpsAgent: new https.Agent({
          rejectUnauthorized: false,
        }),
      });
      var __server = sdkdomain
      if(selfhost){
        __server = ""
      }
      const component_object = await instance.get(`${__server}/landing-api/formsdk/${form_id}?lng=${language}`);
      
      return resolve(component_object.data.data);
    } catch (e) {
      console.log("GET getFormSDK error ", e);
      return resolve({});
    }
  });
};

const parseTrackingCode = (tracking_code)=>{
  console.log("get tracking code ", tracking_code)
}


const getLanguage = ()=>{
  if(typeof document !== 'undefined'){
    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)current_language\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    if(cookieValue){
      return cookieValue;
    }
  }
  return "vi"
}
export default {
  getPageConfig,
  initPage,
  getPageComponent,
  getFormSDK,
  parseTrackingCode,
  getLanguage
};
