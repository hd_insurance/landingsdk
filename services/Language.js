import ReactHtmlParser from 'react-html-parser';
import viLang from './lang/vi.json';
import enLang from './lang/en.json';




export default class Language {
  constructor() {
  	this.loadJSON = this.loadJSON.bind(this);
  	this.setLang = this.setLang.bind(this);
  	this.getLang = this.getLang.bind(this);
  	this.g = this.g.bind(this);
    this.ghtml = this.ghtml.bind(this);
  }


  setLang(lang){
  	if (typeof window !== 'undefined') {
    	localStorage.setItem('hl', lang);
	}
  	
  }
  getLang(){
  	if (typeof window !== 'undefined') {
  		return localStorage.getItem('hl')||"vi"
  	}else{
  		return null;
  	}
  }




  

  g(key){
  	var crL = this.getLang() == "vi"?viLang:enLang
  	
  	if(key){
  		var arrkey = key.split(".");
  		arrkey.forEach((item, index)=>{
  			crL = crL[item]
  		})
  		return crL
  	}else{
  		return ""
  	}
  }
  ghtml(key){
    return ReactHtmlParser(this.g(key))
  }


  async loadJSON(){
  	let currentLang = localStorage.getItem('myValueInLocalStorage')
  }

}