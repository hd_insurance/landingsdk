const Router = require('express').Router();
const { celebrate, Joi, errors } = require('celebrate');
const Controller = require('../controller/formbuy');
const config = require('../config');

Router.post('/api/formbuy/', Controller.submit)

Router.use(errors());
module.exports = Router;