const Router = require('express').Router();
const { celebrate, Joi, errors } = require('celebrate');
const Controller = require('../controller/car');
const config = require('../config');

Router.get('/api/car/define', Controller.getDefine)
Router.post('/api/car/fee', Controller.calculatorFee)
Router.post('/api/car/create-order', Controller.createOrderCar)
Router.post('/api/car/preview-gcn', Controller.previewGCN)


Router.use(errors());
module.exports = Router;