const Router = require('express').Router();
const { celebrate, Joi, errors } = require('celebrate');
const Controller = require('../controller/ComponentController');
const ControllerEditor = require('../controller/LandingEditorController');
const config = require('../config');


Router.get('/api/layout/:layout_id', Controller.getLayout)

Router.get('/api/layout-define/:id', Controller.getLayoutDefine)
Router.get('/api/form-define/:org/:formid', Controller.getForm)
Router.get('/api/pages-instance', Controller.getPageInstance)

Router.get('/api/faq/:faq_id', Controller.getPageFAQ)

Router.post('/api/request-contact/submit', Controller.sendUserRequestContact)


//V2
Router.get('/api/get-all-page/:landingpage_id', Controller.getPageConfig)

Router.get('/landing-api/page/:page_id', Controller.getPage)

Router.get('/landing-api/formsdk/:form_sdk_id', Controller.getForm)

Router.get('/landing-api/current-language', Controller.getCurrentLanguage)
Router.get('/landing-api/current-language/:lang', Controller.changeLanguage)

Router.get('/api/get-relation-product', Controller.getPageRelationProduct)

Router.get('/api/get-product/:group_id', Controller.getPageProduct)


//dev
Router.get('/landing-api/dev-faq/:faq_id', Controller.getPageFAQDEV)

Router.get('/landing-api/dev-lang/:group', Controller.getDevLang)


Router.get('/get-tracking', Controller.testgetTracking)

Router.get('/api/get-layout-editor/:layout_id', ControllerEditor.getLayoutEditor)


Router.use(errors());
module.exports = Router;