const Router = require("express").Router();
const { celebrate, Joi, errors } = require("celebrate");
const Controller = require("../controller/CrossSelling");
const config = require("../config");

Router.get(
  "/api/sdk-form/define/:org_code/:product_code/:channel/:language",
  Controller.getDefine
);

Router.get(
  "/api/sdk-form/cusinfor",
  Controller.gettempcusinfo
);

Router.post(
  "/api/sdk-form/createOrder/:action/:product_code/:channel/:category/:username",
  Controller.createOder
);
Router.post(
  "/api/sdk-form/checkCMND/:product_code/:org_code/:username",
  Controller.checkCMND
);
Router.post(
  "/api/sdk-form/registerVCX/:org_code",
  Controller.registerVCX
);

Router.get(
  "/api/product-list/master/:id",
  Controller.getLandingMultipleProduct
);
Router.get(
  "/api/product-detail/:id/:detail_id",
  Controller.getDetailProduct
);


Router.use(errors());
module.exports = Router;
