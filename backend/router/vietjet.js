const Router = require('express').Router();
const { celebrate, Joi, errors } = require('celebrate');
const Controller = require('../controller/VJController');
const config = require('../config');

Router.get('/api/vj/listdocument/:prodcode', Controller.getListDocument)
Router.get('/api/airport/:utype/:code', Controller.getAirport)
Router.post('/api/get-vj-flight', Controller.getFlightSchedule)
Router.post('/api/vj/search-insur-info/:prod', Controller.searchInsurInfo)
Router.post('/api/vj/get-search-insur-info/:prod', Controller.initDataSearchInsurInfo)

Router.post('/api/vj/billing-request', Controller.billingRequest)


Router.post('/api/upload', Controller.uploadDocument)

Router.post('/api/claim/:prod', Controller.submitClaim)


Router.use(errors());
module.exports = Router;