const Router = require('express').Router();
const { celebrate, Joi, errors } = require('celebrate');
const Controller = require('../controller/visa');



Router.post('/api/visa/search-gcn', Controller.searchGCN)


Router.use(errors());
module.exports = Router;