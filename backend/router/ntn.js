const Router = require('express').Router();
const { celebrate, Joi, errors } = require('celebrate');
const Controller = require('../controller/ntn');
const config = require('../config');


Router.post('/api/nha-tu-nhan/calc/fee', Controller.calcFeeNhaTuNhan);

Router.use(errors());
module.exports = Router;