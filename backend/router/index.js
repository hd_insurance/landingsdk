const Router = require('express').Router();
const vjRoutes = require('./vietjet');
const bhskRoutes = require('./bhsk');
const commonRoutes = require('./common');
const carRoutes = require('./car');
const visaRoutes = require('./visa');
const sdkRoutes = require('./sdk');
const landingsdk = require('./landingsdk');
const ntnRoutes = require('./ntn');
const eclaimRoutes = require('./eclaim');

Router.use(commonRoutes);
Router.use(bhskRoutes);
Router.use(vjRoutes);
Router.use(carRoutes);
Router.use(visaRoutes);
Router.use(sdkRoutes);
Router.use(landingsdk);
Router.use(ntnRoutes);
Router.use(eclaimRoutes);


module.exports = Router;