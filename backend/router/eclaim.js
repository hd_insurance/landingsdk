const Router = require('express').Router();
const { celebrate, Joi, errors } = require('celebrate');
const Controller = require('../controller/Eclaim');
const config = require('../config');


Router.get('/api/eclaim/product-list/define', Controller.getDefine);
Router.get('/api/eclaim/product-list/define-key/:define_code', Controller.getDefineByKey);
Router.post('/api/eclaim/get-order-info/:cat/:product', Controller.getOderInfo);
Router.post('/api/eclaim/search', Controller.searchClaim);
Router.post('/api/eclaim/submit', Controller.submitClaim);

Router.post('/api/eclaim/get-otp', Controller.getOTP);
Router.post('/api/eclaim/list-document/:prodcode', Controller.getListDocument);
Router.post('/api/eclaim/status/:prodcode/:claim_id', Controller.updateStatusClaim);
Router.post('/api/eclaim/document/:prodcode/:claim_id', Controller.updateDocumentClaim);
Router.post('/api/eclaim/update/:prodcode/:claim_id', Controller.updateBeneInfoClaim);

Router.use(errors());
module.exports = Router;