const Router = require('express').Router();
const { celebrate, Joi, errors } = require('celebrate');
const Controller = require('../controller/BHSK');
const config = require('../config');

Router.get('/api/bhsl/packages/:org_code', Controller.getPackList)
Router.post('/api/bhsk/register', Controller.register)
Router.post('/api/bhsk/updateregister', Controller.updateregister)
Router.get('/api/bhsk/register/:cus_id', Controller.getRegister)
Router.get('/api/bhsk/verify/:cus_id', Controller.verifyregister)
Router.post('/api/bhsk/vouchervalid', Controller.voucherValidation)
Router.post('/api/bhsk/vcrml', Controller.voucherRemoveLock)
Router.get('/api/bhsk/faq/:lang/:org_code', Controller.getFAQ)

Router.post('/api/bhsk/calc/fee', Controller.calculatorFee);

Router.use(errors());
module.exports = Router;