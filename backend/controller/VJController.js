const HDIRequest = require('../service/request');
const config = require('../config');
const moment = require('moment');
const {vietjet} = require('../constant');

const getAirport = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain1+'/open-api', {p_utype: req.params.utype, p_code:req.params.code}, vietjet.get_airport)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send({data:[]})
	}
}



const getFlightSchedule = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain1+'/open-api', req.body, vietjet.get_flight_schedule)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send({data:[]})
	}
}

const searchInsurInfo = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const searchParams = {
			A1: req.body.a1||"",
			A2: req.body.a2||"",
			A3: req.body.a3||"",
			A4: req.body.a4||"",
			A5: req.body.a5||"",
			A6: req.body.a6||"",
			A7: req.body.a7||"",
			A8: req.body.a8||"",
			A9: req.body.a9||"",
			A10: req.body.a10||"",
			A11: req.body.a11||"",
			A12: req.body.a12||"",
			A13: req.body.a13||"",
			A14: req.body.a14||"",
			A15:req.body.a15||""
		}
		const params = {
			p_project: "",
			p_username: "VIETJET_VN",
			p_org_code: "VIETJET_VN",
			p_prodcode: req.params.prod,
			p_o_business: JSON.stringify(searchParams),
		}
		const response = await rq.send(config.domain2+'/OpenApi/GetCerInfo', params, vietjet.search_insur_info)
		res.send({data:response.Data})
	}catch(e){
		console.log("RQ EROR", e)
		res.send({data:[]})
	}
}

const initDataSearchInsurInfo = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const searchParams = {
			A1: req.body.a1||"",
			A2: req.body.a2||"",
			A3: req.body.a3||"",
			A4: req.body.a4||"",
			A5: req.body.a5||"",
			A6: req.body.a6||"",
			A7: req.body.a7||"",
			A8: req.body.a8||"",
			A9: req.body.a9||"",
			A10: req.body.a10||"",
			A11: req.body.a11||"",
			A12: req.body.a12||"",
			A13: req.body.a13||"",
			A14: req.body.a14||"",
			A15:req.body.a15||""
		}
		const params = {
			p_project: "",
			p_username: "VIETJET_VN",
			p_org_code: "VIETJET_VN",
			p_prodcode: req.params.prod,
			p_o_business: JSON.stringify(searchParams),
		}
		const response = await rq.send(config.domain2+'/OpenApi/Post', params, vietjet.search_insur_info)
		res.send({data:response.Data})
	}catch(e){
		console.log("RQ EROR", e)
		res.send({data:[]})
	}
}

const submitClaim = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const params = {
			p_project: "",
			p_username: "VIETJET_VN",
			p_org_code: "VIETJET_VN",
			p_prodcode: req.params.prod,
			p_o_business: JSON.stringify(req.body),
		}
		const response = await rq.send(config.domain1+'/open-api', params, vietjet.submit_claim)



		if(response.data[0][0]){
			const response_result = response.data[0][0]
			const emaildata = {
				"DATE_CLAIM": response_result.DATE_CLAIM,
				"DATE_REQUIRED": response_result.DATE_REQUIRED,
				"CERTIFICATE_NO": response_result.CERTIFICATE_NO,
				"INSURED_NAME": response_result.INSURED_NAME,
				"DECLARE_NAME": response_result.DECLARE_NAME,
				"REQUIRED_AMOUNT": response_result.REQUIRED_AMOUNT,
				"PRODUCT_NAME": response_result.PRODUCT_NAME,
				"LINK": response_result.LINK,
				"URL_CLAIM": response_result.URL_CLAIM,
				"CLAIM_NO": response_result.CLAIM_NO,
				"TEMP": [
				  {
					"TEMP_CODE": "BT_TNHAN",
					"PRODUCT_CODE": "BT_TNHAN",
					"ORG_CODE": "HDI",
					"TYPE": "EMAIL",
					"TO": response_result.EMAIL,
					"CC": response_result.INSURED_EMAIL,
					"BCC": ""
				  },
				  {
					"TEMP_CODE": "BT_TBAO",
					"PRODUCT_CODE": "BT_TBAO",
					"ORG_CODE": "HDI",
					"TYPE": "EMAIL",
					"TO": response_result.CLAIM_EMAIL,
					"CC": "",
					"BCC": ""
				  }
				]
			  }


			const response_send_email = await rq.send(config.domain2+'/hdi/service/sendMailWithJson', emaildata, vietjet.hdi_sendmail)
		}

		res.send({data: response.data[0][0]})
	}catch(e){
		console.log("RQ EROR", e)
		res.send({success:false})
	}
}


const getListDocument = async (req, res)=>{
	try{
		let lang = "vi"
	    if(req.cookies.current_language){
	      lang = req.cookies.current_language
	    }
		const rq = new HDIRequest()
		const data = {
			p_project: "",
			p_username: "",
			p_org_code: "VIETJET_VN",
			p_prodcode: req.params.prodcode.toUpperCase(),
			p_language: lang =="vi"?"VN":"EN"
		}
		const response = await rq.send(config.domain1+'/open-api', data, vietjet.get_list_docs)
		// console.log("response",response)
		res.send(response)
	}catch(e){
		console.log("loi get list document",e)
		res.send({data:[]})
	}
}

const uploadDocument = async (req, res)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.sendFile(config.domain1+'/upload', req.files)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send({data:[]})
	}
}

const billingRequest = async (req, res) =>{
	try {
		const rq = new HDIRequest();
		const data = {
			p_project: "",
			p_username: "WEB",
			p_org_code: "VIETJET_VN",
			p_o_business: JSON.stringify(req.body)
		}
		const response = await rq.send(config.domain1+'/open-api', data, vietjet.billing_request)
		res.send(response)
	}catch (e) {
		console.log(e)
		res.send({success:false})
	}
}

module.exports = {
	getFlightSchedule,
	searchInsurInfo,
	getAirport,
	getListDocument,
	uploadDocument,
	submitClaim,
	initDataSearchInsurInfo,
	billingRequest
}
