const HDIRequest = require("../service/request");
const config = require("../config");
const RedisConnector = require("../service/redis");

const calcFeeNhaTuNhan = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const data = req.body;
    console.log('=====> data', data);
    const response = await rq.send(
      config.domain2 + "/OpenApi/insur/calculate/v3/fees",
      data,
      "APIZTPI8X1"
    );
    res.send(response);
  } catch (err) {
    console.log('error nè',err);
  }
};

module.exports = {
  calcFeeNhaTuNhan,
};
