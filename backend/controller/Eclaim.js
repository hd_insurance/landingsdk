const HDIRequest = require("../service/request");
const LogService = require("../service/LogService");

const config = require("../config");
const RedisConnector = require("../service/redis");
const { sdk } = require("../constant");

const ttl = 20;

const getDefine = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const response = await rq.send(
      config.domain1 + "/open-api",
      {
        P_PROJECT: "ECLAIM",
        p_username: "LANDING_SDK",
        p_keyword: "",
        p_status: "APPROVED",
        p_page: 0
      },
      "API6ERHTCJ"
    );
    if (response.success) {
      res.send(response);
    } else {
      console.log("GET PACKAGE ERROR 1");
      res.send([]);
    }
  } catch (e) {
    console.log("GET PACKAGE ERROR ", e);
    res.send([]);
  }
};


const getDefineByKey = async (req, res, next) => {
  const req_id = LogService.makeid()
  let log_request = {
    method: "post",
    url: "/open-api"
  }
  try {
    const rq = new HDIRequest();
    const response = await rq.send(
      config.domain1 + "/open-api",
      {
       p_Project:"",
       p_UserName:"",
       p_Keyword:"",
       p_Page:"",
       p_DefineCode: req.params.define_code
      },
      "HDI_37"
    );
    if (response.success) {
      res.send(response);
    } else {
      console.log("GET PACKAGE ERROR 1");
      res.send([]);
    }
  } catch (e) {
    LogService.insert(req_id, `[${process.env.NODE_ENV}] - Search claim error`, "error", "request", log_request)
    console.log("GET PACKAGE ERROR ", e);
    res.send([]);
  }
};



const searchClaim = async (req, res, next) => {
  const req_id = LogService.makeid()
  let log_request = {
    method: "post",
    url: "/open-api"
  }
  try {
    let lang = "vi"
    if(req.cookies.current_language){
      lang = req.cookies.current_language
    }
    const rq = new HDIRequest();
    const defineParams = {
      A8: req.body.field1,
      A9: req.body.field2,
      A10: req.body.field1,
      A11: "DETAIL",
      A18: lang=="vi"?"VN":"EN"
    }
    log_request.request = defineParams
    const response = await rq.send(
      config.domain1 + "/open-api",
      {
        P_PROJECT: "LANDING_SDK",
        P_USERNAME: "X",
        P_ORG_CODE: "HDI",
        P_O_BUSINESS: JSON.stringify(defineParams)
      },
      "APIHHKFEBW"
    );
    log_request.response = response
    if (response.success) {
      res.send(response);
    } else {
      console.log("SEARCH CLAIM ERROR 1");
      try{
        LogService.insert(req_id, "Search Claim Error", "error", "request", log_request)
      }catch(e){

      }

      res.send([]);
    }
  } catch (e) {
    try{
      log_request.data = e.toString()
      LogService.insert(req_id, "Search Claim Error", "error", "request", log_request)
    }catch(e){}
    console.log("SEARCH CLAIM ERROR ", e);
    res.send([]);
  }
};

const getOderInfo = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const query_param = {
      A1: req.body.CERTIFICATE_NO,
      A2: req.body.ID_CARD,
      A3: req.body.NAME
    }
    const response = await rq.send(
      config.domain1 + "/open-api",
      { 
        P_CHANNEL: "",
        P_USERNAME: "",
        P_ORG_CODE: "HDI",
        P_CATEGORY: "GENERAL",
        P_PRODUCT_CODE: req.params.product,
        P_EFF: null,
        P_OBJ: JSON.stringify(query_param)
      },
      "APIG05WJJW"
    );

    if (response.success) {
      res.send(response);
    } else {
      console.log("SEARCH CLAIM ERROR 1");
      res.send([]);
    }
  } catch (e) {
    console.log("SEARCH CLAIM ERROR ", e);
    res.send([]);
  }
};

const submitClaim = async (req, res, next) => {
  const req_id = LogService.makeid()
  let req_content = {
    url: "/open-api?action_code=APILWA4PQD"
  }
  try {
    const rq = new HDIRequest();
    const data = {
      P_PROJECT: "",
      P_USERNAME:"WEB_HDI",
      P_ORG_CODE: "HDI",
      P_PRODCODE: req.body.product_code.toUpperCase(),
      P_O_BUSINESS: JSON.stringify(req.body),
    }
    req_content.request = data
    const response = await rq.send(config.domain1 + "/open-api", data, "APILWA4PQD");
    req_content.response = response
    if (response.success) {
      if(response.data[0][0].TYPE != 'SUCCESS'){
        LogService.insert(req_id, `[${process.env.NODE_ENV}] Create claim error #1`, "error", "request", req_content)
        return res.send({success: false, message: response.data[0][0].RESULT});
      }
      const response_result = response.data[0][0]
      const emaildata = {
        "DATE_CLAIM": response_result.DATE_CLAIM,
        "DATE_REQUIRED": response_result.DATE_REQUIRED,
        "CERTIFICATE_NO": response_result.CERTIFICATE_NO,
        "INSURED_NAME": response_result.INSURED_NAME,
        "DECLARE_NAME": response_result.DECLARE_NAME,
        "REQUIRED_AMOUNT": response_result.REQUIRED_AMOUNT,
        "PRODUCT_NAME": response_result.PRODUCT_NAME,
        "LINK": response_result.LINK,
        "URL_CLAIM": response_result.URL_CLAIM,
        "CLAIM_NO": response_result.CLAIM_NO,
        "TEMP": [
          {
            "TEMP_CODE": "BT_TNHAN",
            "PRODUCT_CODE": "BT_TNHAN",
            "ORG_CODE": "HDI",
            "TYPE": "EMAIL",
            "TO": response_result.EMAIL,
            "CC": response_result.INSURED_EMAIL,
            "BCC": ""
          },
          {
            "TEMP_CODE": "BT_TBAO",
            "PRODUCT_CODE": "BT_TBAO",
            "ORG_CODE": "HDI",
            "TYPE": "EMAIL",
            "TO": response_result.CLAIM_EMAIL,
            "CC": "",
            "BCC": ""
          }
        ]
      }
      var req_email = {
        url: config.domain2+'/hdi/service/sendMailWithJson',
        request: emaildata
      }
      try{
        const response_send_email = await rq.send(config.domain2+'/hdi/service/sendMailWithJson', emaildata, "HDI_EMAIL_JSON")
        req_email.response = response_send_email
      }catch(e){
        req_email.note = e.toString()
        LogService.insert(req_id, `[${process.env.NODE_ENV}] Send Email Claim Error`, "error", "request", req_email)
      }
      res.send(response);
    } else {
      console.log("SUBMIT CLAIM ERROR 1 ", response);
      LogService.insert(req_id, `[${process.env.NODE_ENV}] Create claim error`, "error", "request", req_content)
      res.send(response);
    }
  } catch (e) {
    console.log("SUBMIT CLAIM ERROR > ", e);
    req_content.response = e
    LogService.insert(req_id, `[${process.env.NODE_ENV}] Create claim error`, "error", "request", req_content)
    res.send({success: false});
  }
};

const getOTP = async (req, res, next) => {
  const req_id = LogService.makeid()
  try {
    const rq = new HDIRequest();
    const data = {
      "TYPE_CODE": "CLAIM",
      "PARAM_VALUE": req.body.claimCode,
      "TYPE_SEND": "EMAIL",
      "LENG": "6"
    }
    const response = await rq.send(config.domain2 + "/hdi/service/getTokenVerify", data, "APILO3NM1O");
    if (response.Success) {
      res.send(response);
    } else {
      console.log("SUBMIT CLAIM ERROR 1 ", response);
      res.send(response);
    }
  } catch (e) {
    console.log("SUBMIT CLAIM ERROR ", e);
    try{
      LogService.insert(req_id, `[${process.env.NODE_ENV}] Claim: GET OTP Error`, "warning", "request", e.toString())
    }catch(e){
      
    }
    
    res.send({
      Success: false,
      Data: {Message: e.Data?e.Data.Message:""}
    });
  }
};

const verifyOTP = (claimCode, otp_token) => {
  return new Promise(async (resolve, rejected)=>{
     try {
        const rq = new HDIRequest();
        const data = {
          "TYPE_CODE": "CLAIM",
          "PARAM_VALUE": claimCode,
          "TYPE_SEND": "EMAIL",
          "TOKEN": otp_token
        }
       
        const response = await rq.send(config.domain2 + "/hdi/service/verifyToken", data, "APIU640R0R");
        if (response.Success) {
          return resolve(true)
        } else {
          console.log("SUBMIT OTP ERROR 1 ", response);
          return resolve(false)
        }
      } catch (e) {
        console.log("UBMIT OTP ERROR 1 ", e);
        return resolve(false)
      }
  })
 
};


const getListDocument = async (req, res)=>{
  try{
    let lang = "vi"
    if(req.cookies.current_language){
      lang = req.cookies.current_language
    }
    const rq = new HDIRequest()
    const data = {
      p_project: "",
      p_username: "",
      p_org_code: "HDI",
      p_prodcode: req.params.prodcode.toUpperCase(),
      p_bencode: req.body.ben,
      p_language: lang=="vi"?"VN":"EN"
    }

    const response = await rq.send(config.domain1+'/open-api', data, "APIHM5AVE4")
    // console.log("response",response)
    res.send(response)
  }catch(e){
    console.log("loi get list document",e)
    res.send({data:[]})
  }
}


const updateStatusClaim = async (req, res)=>{
  try{
    const rq = new HDIRequest()
    const otp = req.body.otp
    const data = {
      P_PROJECT: "WEB_CLAIM",
      P_USERNAME: "WEB_CLAIM",
      P_ORG_CODE: "HDI",
      P_PRODCODE: req.params.prodcode.toUpperCase(),
      P_CLAIMTRANS: req.params.claim_id,
      P_WORKID: req.body.accept?"KHDY":"KHTC",
      P_STAFFCODE: "CUS",
      P_STAFFNAME: "HDI-LANDING-CLAIM",
      P_CONTENT: req.body.content||(req.body.accept?"Khách hàng xác nhận đồng ý":"Khách hàng xác nhận không đồng ý"),
      P_PAYDATE: null,
      P_O_BUSINESS: ""
    }
    const otp_verify = await verifyOTP(req.params.claim_id, otp)
    if(otp_verify || otp == "000000000"){
      const response = await rq.send(config.domain1+'/open-api', data, "APIBGIH3HN")
      if(response.data[0][0].TYPE == "ERROR"){
        res.send({success:true, otp: true, submit: false, message: response.data[0][0].RESULT })
      }else{
        res.send({success:true, otp: true, submit: true })
      }
      
    }else{
      res.send({success:true, otp:false })
    }
  }catch(e){
    console.log("loi update status claim ",e)
    res.send({success:false, otp: true })
  }
}

const updateDocumentClaim = async (req, res)=>{
  const req_id = LogService.makeid()
  let req_update_claim = {
    method: "post",
    url: "/open-api"
  }
  try{
    const rq = new HDIRequest()
    
    const ctdata = {
      "channel": "WEB",
      "username": "WEB_CLAIM",
      "orgcode": "HDI",
      "product_code": req.params.prodcode.toUpperCase(),
      "package_code": "",
      "p_claimtrans": req.params.claim_id,
      "p_workid": "DABS",
      "p_staffcode": "CUS",
      "p_staffname": "HDI LANDING CLAIM",
      "p_content": "Bổ sung hồ sơ giấy tờ",
      "p_paydate": null,
      "file_attach_more": req.body
    }
    const data = {
      P_PROJECT: "WEB_CLAIM",
      P_USERNAME: "WEB_CLAIM",
      P_ORG_CODE: "HDI",
      P_PRODCODE: req.params.prodcode.toUpperCase(),
      P_CLAIMTRANS: req.params.claim_id,
      P_WORKID: "DABS",
      P_STAFFCODE: "CUS",
      P_STAFFNAME: "HDI LANDING CLAIM",
      P_CONTENT: "Bổ sung hồ sơ giấy tờ",
      P_PAYDATE: null,
      P_O_BUSINESS: JSON.stringify(ctdata)
    }
    req_update_claim.request = data
    const response = await rq.send(config.domain1+'/open-api', data, "APIBGIH3HN")
    req_update_claim.response = response
      if(response.data[0][0].TYPE == "ERROR"){
        try{
          LogService.insert(req_id, `[${process.env.NODE_ENV}] Update Document claim error`, "error", "request", req_update_claim)
        }catch(e){}
        res.send({success:true, submit: false, message: response.data[0][0].RESULT })
      }else{
        res.send({success:true, submit: true })
      }
  }catch(e){
    console.log("loi update status claim ",e)
    LogService.insert(req_id, `[${process.env.NODE_ENV}] Update Document claim error`, "error", "request", req_update_claim)
    res.send({success:false, otp: true })
  }
}

const updateBeneInfoClaim = async (req, res)=>{
  const req_id = LogService.makeid()
  let req_update_claim = {}
  try{
    const rq = new HDIRequest()
    const data = {
      P_PROJECT: "WEB_CLAIM",
      P_USERNAME: "WEB_CLAIM",
      P_ORG_CODE: "HDI",
      P_PRODCODE: req.params.prodcode.toUpperCase(),
      P_O_BUSINESS: JSON.stringify(req.body)
    }
      req_update_claim.request = req.body
      const response = await rq.send(config.domain1+'/open-api', data, "APIDNHFACM")
      req_update_claim.response = response
      if(response.data[0][0].TYPE == "ERROR"){
        res.send({success:true, submit: false, message: response.data[0][0].RESULT })
      }else{
        res.send({success:true, submit: true })
      }
  }catch(e){
    console.log("loi update status claim ",e)
    req_update_claim.note = e.toString()
    LogService.insert(req_id, `[${process.env.NODE_ENV}] Update Bene Claim Error`, "error", "request", {e: e.toString(), request:req_update_claim})
    res.send({success:false, otp: true })
  }
}








module.exports = {
  getDefine,
  searchClaim,
  getDefineByKey,
  getOderInfo,
  submitClaim,
  getOTP,
  getListDocument,
  updateStatusClaim,
  updateDocumentClaim,
  updateBeneInfoClaim
};
