const HDIRequest = require("../service/request");
const config = require("../config");
const {visa} = require("../constant");

const searchGCN = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const searchParams = {
      A1: req.body.a1 || "",
      A2: req.body.a2 || "",
      A3: req.body.a3 || "",
      A4: req.body.a4 || "",
      A5: req.body.a5 || "",
      A6: req.body.a6 || "",
      A7: req.body.a7 || "",
      A8: req.body.a8 || "",
      A9: req.body.a9 || "",
      A10: req.body.a10 || "",
      A11: req.body.a11 || "",
      A12: req.body.a12 || "",
      A13: req.body.a13 || "",
      A14: req.body.a14 || "",
      A15: req.body.a15 || "",
    };
    const params = {
      p_project: "",
      p_username: "",
      p_org_code: "",
      p_prodcode: "VISA",
      p_o_business: JSON.stringify(searchParams),
    };
    // console.log(params);
    const response = await rq.send(
      config.domain1 + "/open-api",
      params,
      visa.search_gcn
    );
    if (response.success) {
      // console.log("haha", response.data);
      if (response.data.length > 0) {
        if (response.data[0].length > 0) {
          res.send({ data: response.data[0] });
        } else {
          res.send({ data: [] });
        }
      } else {
        res.send({ data: [] });
      }
    } else {
      res.send({ data: [] });
    }
  } catch (e) {
    console.log("RQ EROR", e);
    res.send({ data: [] });
  }
};

module.exports = {
  searchGCN,
};
