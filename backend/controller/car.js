const HDIRequest = require("../service/request");
const config = require("../config");
const RedisConnector = require("../service/redis");
const { car } = require("../constant");

const getDefine = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const response = await rq.send(
      config.domain1 + "/open-api",
      {
        channel: "SDK_CAR",
        username: "",
        org_code: "HDI",
        product_code: "",
        language: "",
      },
      car.get_define
    );
    if (response.data) {
      // console.log(response);
      res.send(response.data);
    } else {
      res.send([]);
    }
  } catch (e) {
    console.log("GET DEFINE ERROR ", e);
    res.send([]);
  }
};

const calculatorFee = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const productInfor = req.body;
    // console.log("hihi", productInfor);
    const response = await rq.send(
      config.domain2 + "/OpenApi/mask_cal/v1/fees",
      {
        channel: "WEB_B2B",
        userName: "0981448929",
        ORG_CODE: "HDI",
        PRODUCT_INFO: productInfor,
        GIFTS: null,
      },
      car.act_car
    );
    if (response.Data) {
      // console.log(response);
      res.send(response.Data);
    } else {
      res.send([]);
    }
  } catch (e) {
    console.log("GET DEFINE ERROR ", e);
    res.send([]);
  }
};

const createOrderCar = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const {
      ORG_CODE,
      CHANNEL,
      USERNAME,
      ACTION,
      VEHICLE_INSUR,
      BUYER,
      BILL_INFO,
      PAY_INFO,
      ORD_SKU,
      ORD_PARTNER
    } = req.body;
    
    var data = {
        ORG_CODE: ORG_CODE,
        CHANNEL: CHANNEL,
        USERNAME: USERNAME,
        ACTION: ACTION,
        BUYER: BUYER,
        BILL_INFO: BILL_INFO,
        VEHICLE_INSUR: [...VEHICLE_INSUR],
        PAY_INFO: PAY_INFO,
        ORD_PARTNER: ORD_PARTNER,
      }
    if(ORD_SKU){
      data.ORD_SKU = ORD_SKU
    }
    const response = await rq.send(
      config.domain2 + "/OpenApi/ver2/insur/vehicle/create_pay",
      data,
      car.act_car
    );

    if (response) {
      // console.log("da vao day", response);
      res.send(response);
    } else {
      res.send([]);
    }
  } catch (e) {
    console.log("GET DEFINE ERROR ", e);
    res.send(e);
  }
};

const previewGCN = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const data = req.body;
    const response = await rq.send(
      config.domain2 + "/hdi/service/viewCertificate",
      data,
      car.preview_gcn
    );
    if (response) {
      // console.log("da vao day", response);
      res.send(response);
    } else {
      res.send([]);
    }
  } catch (e) {
    console.log("PREVIEW ERROR ", e);
    res.send(e);
  }
};

module.exports = {
  getDefine,
  calculatorFee,
  createOrderCar,
  previewGCN,
};
