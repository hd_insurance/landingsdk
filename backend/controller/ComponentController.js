const HDIRequest = require('../service/request');
const MongoConnector = require("../service/MongoConnector");
const config = require('../config');
const {ObjectId} = require('mongodb');
const MongoClient = require('mongodb').MongoClient;
var forEach = require('async-foreach').forEach;
var jwt = require('jsonwebtoken');
var _ = require('lodash');
const assert = require('assert');
const { bhsk } = require('../constant');
const LogService = require("../service/LogService");

var clientdb = null
var allcomponent = null

const langposition = {
	"vi": 0,
	"en": 1,
	"cn": 2
}


// const createConnection = ()=>{
// 		return new Promise(async (resolve, rejected)=>{
// 			const url = `mongodb://${config.mongo.host}:${config.mongo.port}/`
// 			try{
// 				const client = await MongoClient.connect(url, { useNewUrlParser: true,  useUnifiedTopology: true });
// 				return resolve(client)
// 			}catch(e){
// 				const req_id = LogService.makeid()
// 				LogService.insert(req_id, "MongoDB Connect Error", "error", "system", {e: e.toString(), connect_string: url})
// 				console.log("MongoDB Error: ", e)
// 				return rejected(e)
// 			}
// 		})
// 	}


const getPage = async (req, res, next)=>{
	try {
		let lang = req.query.lng || "vi"
		if(req.cookies.current_language){
			lang = req.cookies.current_language
		}
	 	const database = MongoConnector.database();
	 	const page_cl = database.collection('page');
	 	const web_instance_cl = database.collection('web_instance');
	 	const layout_cl = database.collection('layout');
	 	const component_cl = database.collection('component');
	 	const all_cmpnt = await component_cl.find({}).toArray()

	 	if(allcomponent){

	 	}else{
	 		allcomponent = {}
	 		forEach(all_cmpnt, (item, index)=>{
	 			allcomponent[item._type] = item
	 		}, function(){
	 			
	 		})
	 	}

	 	const page_info = await page_cl.find({_id: ObjectId(req.params.page_id)}).toArray()
	 	if(page_info[0]){

		 	const aggregate_opts = [
			    	{	$match:{
			    		_id: page_info[0].web_instance_id
			    	}},
			    	{$lookup:{
				            from: "org",
				            localField : "org",
				            foreignField : "_id",
				            as : "org_info"
			        	}
			    	}
			]
	 		const web_instance_info = await web_instance_cl.aggregate(aggregate_opts).toArray()
	 		var layout_component = null
	 		var form_info = null

	 		if(page_info[0].layout){
	 			var layout = await layout_cl.find({_id: page_info[0].layout}).toArray()
	 			layout_component = layout[0].component
	 			if(layout_component == null){
	 				console.log("co null ne ", layout[0])
	 			}
	 			if(layout_component.internal_language){
	 				layout_component.internal_language = await parseInternalLanguage(layout_component.internal_language, lang)
	 			}

	 			await readTObject(layout_component, layout_cl, lang)

	 		}else if(page_info[0].sdkform){


	 			const forminfo = await getFormInfo(page_info[0].sdkform)
	 			form_info = forminfo

	 			var layout = await layout_cl.find({_id: forminfo.layout}).toArray()
	 			layout_component = layout[0].component
	 			await readTObject(layout_component, layout_cl, lang)
	 		}
	 		var out_data = {
	 			meta_data: page_info[0].meta_data,
	 			org_code_default: web_instance_info[0].org_info[0].org_code,
	 			page_config: page_info[0].page_config,
	 			page_type: page_info[0].type,
	 			page_status: page_info[0].status,
	 			permalink: page_info[0].permalink,
	 			layout_component: layout_component
	 		}
	 		if(page_info[0].layout){
	 			
	 			out_data.layout_component = layout_component

	 		}else if(page_info[0].sdkform){

	 			var form_default_define = parseFormDefine(form_info.define)
	 			if(page_info[0].config_id){
	 				const form_config = await getFormConfigInfo(page_info[0].config_id)
	 				if(form_config){
	 					form_default_define = {...form_default_define, ...form_config.config}
	 				}
	 			}
	 			


	 			
	 			out_data.form_info = form_info
	 			out_data.product_config = form_default_define
	 		}
	 		res.send({status: true, data: out_data})
	 	}else{
	 		res.send({status: false, message: "Page not found!"})
	 	}
	} catch (e) {
	  console.log("ERROR ", e);
	  res.send([]);
	}
}


const getLayoutEditor = async (req, res, next)=>{
try {
		let data_editor = []
		let lang = req.query.lng || "vi"
		if(req.cookies.current_language){
			lang = req.cookies.current_language
		}

	 	const database = MongoConnector.database();
	 	const layout_cl = database.collection('layout');
	 	const component_cl = database.collection('component');
	 	const all_cmpnt = await component_cl.find({}).toArray()

	 	if(allcomponent){

	 	}else{
	 		allcomponent = {}
	 		forEach(all_cmpnt, (item, index)=>{
	 			allcomponent[item._type] = item
	 		}, function(){
	 			
	 		})
	 	}


			const layout = await layout_cl.find({_id: ObjectId(req.params.layout_id)}).toArray()

	 		var layout_component = null
	 		var form_info = null

	 		if(layout){
	 		
	 			layout_component = layout[0].component
	 			if(layout_component.internal_language){
	 				layout_component.internal_language = await parseInternalLanguage(layout_component.internal_language, lang)
	 			}
	 			await readEditorObject(layout_component, layout_cl, data_editor)

	 		}
	 		var out_data = {
	 			
	 		}
	 		out_data.layout_component = layout_component


	 		res.send({status: true, data: out_data})
	 
	} catch (e) {
	  console.log("ERROR ", e);
	  res.send([]);
	}
}



const getLayout = async (req, res, next)=>{
try {
		
		let lang = req.query.lng || "vi"
		if(req.cookies.current_language){
			lang = req.cookies.current_language
		}
	 	const database = MongoConnector.database();
	 	const layout_cl = database.collection('layout');
	 	const component_cl = database.collection('component');
	 	const all_cmpnt = await component_cl.find({}).toArray()

	 	if(allcomponent){

	 	}else{
	 		allcomponent = {}
	 		forEach(all_cmpnt, (item, index)=>{
	 			allcomponent[item._type] = item
	 		}, function(){
	 			
	 		})
	 	}


			const layout = await layout_cl.find({_id: ObjectId(req.params.layout_id)}).toArray()

	 		var layout_component = null
	 		var form_info = null

	 		if(layout){
	 		
	 			layout_component = layout[0].component
	 			if(layout_component.internal_language){
	 				layout_component.internal_language = await parseInternalLanguage(layout_component.internal_language, lang)
	 			}
	 			await readTObject(layout_component, layout_cl, lang)

	 		}
	 		
	 		var out_data = {
	 			
	 		}
	 		out_data.layout_component = layout_component


	 		res.send({status: true, data: out_data})
	 
	} catch (e) {
	  console.log("ERROR ", e);
	  res.send([]);
	}
}



const parseFormDefine = (default_form_define)=>{
	var cfg = {}
	Object.keys(default_form_define).map((item, index)=>{
		cfg[item] = default_form_define[item].value
	})
	return cfg;
}

const getForm = async (req, res, next)=>{
	try {
		const lang = req.query.lng || "vi"
	 	const database = MongoConnector.database();
	 	const sdk_cl = database.collection('sdk');
	 	const layout_cl = database.collection('layout');
	 	const component_cl = database.collection('component');
	 	const all_cmpnt = await component_cl.find({}).toArray()

	 	if(allcomponent){

	 	}else{
	 		allcomponent = {}
	 		forEach(all_cmpnt, (item, index)=>{
	 			allcomponent[item._type] = item
	 		}, function(){
	 			
	 		})
	 	}

	 	// const collection2 = database.collection('layout_component');
	 	// const clc_lang_component = database.collection('layout_language'); //get language cho layout component
	 	// const clc_lang_trans = database.collection('language_translation');

	 	const aggregate_opts = [
		    	{	$match:{
		    		_id: ObjectId(req.params.form_sdk_id)
		    	}},
		    	{$lookup:{
			            from: "layout",
			            localField : "layout",
			            foreignField : "_id",
			            as : "layout"
		        	}
		    	}
		]


	 	const form_info = await sdk_cl.aggregate(aggregate_opts).toArray()

	 	if(form_info[0]){

	 		var layout_component = form_info[0].layout[0].component
	 		await readTObject(layout_component, layout_cl, lang)

	 		var from_cfg = {}
	 		Object.keys(form_info[0].define).map((cfg_item, index)=>{
	 			from_cfg[cfg_item] = form_info[0].define[cfg_item].value
	 		})

	 		const out_data = {
	 			form_config: from_cfg,
	 			file_path: form_info[0].file_path,
	 			layout_component: layout_component
	 		}
	 		res.send({status: true, data: out_data})
	 	}else{
	 		res.send({status: false, message: "Page not found!"})
	 	}
	} catch (e) {
	  console.log("ERROR ", e);
	  res.send([]);
	}
}



const changeLanguage = (req, res, next)=>{
	res.cookie("current_language", req.params.lang, { expires: new Date(Date.now() + 900000)})
	res.send({success: true})
}

const getCurrentLanguage = (req, res, next)=>{
	if (req.cookies.current_language) {
		return res.send({language: req.cookies.current_language})
	}else{
		return res.send({language: "vi"})
	}
}




const getDevLang = async (req, res, next) => {
	try {
	 	const database = MongoConnector.database();
		var data_lang = {}
		const clc_lang_trans = database.collection('language_translation');
		const chim = await clc_lang_trans.find({group: ObjectId(req.params.group)}).toArray()
			chim.forEach((item, index)=>{
				data_lang[item.translate_key] = [item.vi, item.en?item.en:item.vi]
			})

		res.send(data_lang)

	}catch(e){
		console.log(e)
		res.send(e)
	}
}




const getLayoutDefine = async (req, res, next) => {
	let client = null
	try {
	 	const database = MongoConnector.database();
	 	const collection = database.collection('layout');
	 	const collection2 = database.collection('layout_component');
	 	const clc_lang_component = database.collection('layout_language'); //get language cho layout component
	 	const clc_lang_trans = database.collection('language_translation');

	 	const web_layout = await collection.find({_id: ObjectId(req.params.id)}).toArray()

	 	if(web_layout[0]){
	 		const web_layout_config = web_layout[0]
	 		const root_component = web_layout_config.root_component
	 		const web_layout_component = await collection2.find({_id: root_component}).toArray()
	 		var root_comp = web_layout_component[0].component
	 		var all_component_id = [web_layout_component[0]._id]
	 		await readObject(root_comp, collection2, all_component_id)

			//Tim ngon ngu theo component ID
			const aggregate_opts = [
			   	{	
			   		"$match": {
						"component": { "$in": all_component_id }
					}
				},
				{ 
				"$lookup": {
				 "from": "language_translation",
				 "let": { "language_group": "$language_group" },
				"pipeline": [
					{ "$match": { "$expr": { "$in": [ "$group", "$$language_group" ] } } }
				],
				 "as": "language_translation"
				}}
				
			]

			// console.log("all_component_id ", all_component_id)

			// const lang = await clc_lang_component.find({ component: { $in: all_component_id }}).toArray()
			var data_lang = {}
			const chim = await clc_lang_component.aggregate(aggregate_opts).toArray()

			chim.forEach((item, index)=>{
				if(item.language_translation){

					item.language_translation.forEach((lang_item, index)=>{
						data_lang[lang_item.translate_key] = [lang_item.vi, lang_item.en?lang_item.en:lang_item.vi]
					})

				}
			})

			var parent_path = ""
			await mapRefObject(root_comp, parent_path)


			res.send({success: true, config: web_layout_config, component: root_comp, language: data_lang})
	 	}else{
	 		console.log("Loi o day")
	 		res.send({success: false})
	 	}
	 	

	 	
	} catch (e) {
	  console.log("CALCULATOR FEE ERROR ", e);
	  res.send([]);
	}
  };









  const getCMSLayoutDefine = async (req, res, next) => {
	try {
	 	const database = MongoConnector.database();
	 	const collection = database.collection('layout');
	 	const collection2 = database.collection('layout_component');
	 	const clc_lang_component = database.collection('layout_language'); //get language cho layout component
	 	const clc_lang_trans = database.collection('language_translation');

	 	const web_layout = await collection.find({_id: ObjectId(req.params.id)}).toArray()

	 	if(web_layout[0]){
	 		const web_layout_config = web_layout[0]
	 		const root_component = web_layout_config.root_component
	 		const web_layout_component = await collection2.find({_id: root_component}).toArray()
	 		var root_comp = web_layout_component[0].component
	 		var all_component_id = [web_layout_component[0]._id]
	 		await readObject(root_comp, collection2, all_component_id)

			//Tim ngon ngu theo component ID
			const aggregate_opts = [
			   	{	
			   		"$match": {
						"component": { "$in": all_component_id }
					}
				},
				{ 
				"$lookup": {
				 "from": "language_translation",
				 "let": { "language_group": "$language_group" },
				"pipeline": [
					{ "$match": { "$expr": { "$in": [ "$group", "$$language_group" ] } } }
				],
				 "as": "language_translation"
				}}
				
			]

			// console.log("all_component_id ", all_component_id)

			// const lang = await clc_lang_component.find({ component: { $in: all_component_id }}).toArray()
			var data_lang = {}
			const chim = await clc_lang_component.aggregate(aggregate_opts).toArray()

			chim.forEach((item, index)=>{
				if(item.language_translation){

					item.language_translation.forEach((lang_item, index)=>{
						data_lang[lang_item.translate_key] = [lang_item.vi, lang_item.en?lang_item.en:lang_item.vi]
					})

				}
			})

			var parent_path = ""
			await mapRefObject(root_comp, parent_path)


			res.send({success: true, config: web_layout_config, component: root_comp, language: data_lang})
	 	}else{
	 		console.log("Loi o day")
	 		res.send({success: false})
	 	}
	} catch (e) {
	  console.log("CALCULATOR FEE ERROR ", e);
	  res.send([]);
	}
  };






 //  const getForm = async (req, res, next) => {
	// try {
	//  	const client = await createConnection()
	//  	const database = client.db(config.mongo.db)
	//  	const collection_form = database.collection('sdk_form');
	//  	const collection_form_config = database.collection('sdk_form_config');
	//  	const layout_component = database.collection('layout_component');
	//  	const lookup_query1 = [
	// 	    	{	$match:{
	// 	    		_id: ObjectId(req.params.formid)
	// 	    	}},
	// 	    	{$lookup:{
	// 		            from: "layout",
	// 		            localField : "layout",
	// 		            foreignField : "_id",
	// 		            as : "layout"
	// 	        	}
	// 	    	}
	// 	]
	//  	const sdk_form = await collection_form.aggregate(lookup_query1).toArray()
	//  	const sdk_result_config = await collection_form_config.find({_id: sdk_form[0].config}).toArray()
	//  	const sdk_result_config_extend = await collection_form_config.find({$and: [{extend_from: sdk_form[0].config}, {org: req.params.org.toUpperCase() }]}).toArray()
	//  	const form_layout = await layout_component.find({_id: sdk_form[0].layout[0].root_component}).toArray()

	//  	var mainconfig = sdk_result_config[0].config_data
	//  	if(sdk_result_config_extend[0]){
	//  		mainconfig = {...mainconfig, ...sdk_result_config_extend[0].config_data};
	//  	}
	//  	var config_object = {}
	//  	for (const [key, value] of Object.entries(mainconfig)) {
	// 	 	config_object[key] = value.value
	// 	}

	// 	var root_comp = form_layout[0].component
	// 	var all_component_id = []
	// 	await readObject(root_comp, layout_component, all_component_id)
		


	//  	res.send({success: true, sdk_form:sdk_form[0], component: root_comp, form_config: config_object })
	 	
	// } catch (e) {
	//   console.log("GET FORM ERROR ", e);
	//   res.send({success: false,  component: {}, form_config: {} })
	// }
 //  };


 const getFormInfo = (form_sdk_id) => {
 	return new Promise(async (resolve, rejected)=>{
	 	const database = MongoConnector.database();
 		const sdk_cl = database.collection('sdk');
 		const db_sdk = await sdk_cl.find({_id: form_sdk_id}).toArray()
 		return resolve(db_sdk[0])
 	})
 }

const getFormConfigInfo = (config_id) => {
 	return new Promise(async (resolve, rejected)=>{
	 	const database = MongoConnector.database();
 		const sdk_cl = database.collection('sdk_config');
 		const db_sdk = await sdk_cl.find({_id: ObjectId(config_id)}).toArray()
 		return resolve(db_sdk[0])
 	})
}
	
	const readEditorObject = async (object, collection2, data_editor)=>{
  	return Promise.all(Object.keys(object).map(async (props, index)=>{
  		if(typeof object[props] == "object"){
  			if(object.type == "sdk_form"){
  				
  				// const form_config = await getFormConfigInfo(object.define.config_id)
  				// // object.form_name = form_.name
  				// // object.file_path = form_.define.file_path
  				// if(form_config){
  				// 	object.define.config = form_config.config
  				// }else{
  				// 	object.define.config = {}
  				// }
  				
  			}else{
  				if(props == "define"){
  					if(allcomponent[object.type]){
  						const component_define = allcomponent[object.type].component
  						if(component_define.define){
  							const resultmaped = await mapDefineEditorConfig(component_define.define, object.define, data_editor)
  							object.define = resultmaped
  						}
  					}else if(object.define.layout){
  						await readEditorObject(object.define.layout, collection2, data_editor)
  					}
  				} else {
  					if(object[props]){
  						await readEditorObject(object[props], collection2, data_editor)
  					}
  					
  				}
  				
  			}
  		}else{

  		}
  	}))

  }


  const readTObject = async (object, collection2, lang)=>{
  	return Promise.all(Object.keys(object).map(async (props, index)=>{
  		if(typeof object[props] == "object"){
  			if(object[props] instanceof ObjectId){
  				const web_layout_component = await collection2.find({_id: object[props]}).toArray()
  				object[props] = web_layout_component[0].component

  			}else if(object.type == "sdk_form"){
  				const form_config = await getFormConfigInfo(object.define.config_id)
  				// object.form_name = form_.name
  				// object.file_path = form_.define.file_path
  				if(form_config){
  					object.define.config = form_config.config
  				}else{
  					object.define.config = {}
  				}
  				
  			}else{
  				if(props == "define"){
  					if(allcomponent[object.type]){
  						const component_define = allcomponent[object.type].component
  						if(component_define.define){
  							const resultmaped = await mapDefineConfig(component_define.define, object.define, lang)
  							object.define = resultmaped
  						}
  					}else if(object.define.layout){
  						await readTObject(object.define.layout, collection2, lang)
  					}
  				} else {
  					if(object[props]){
  						await readTObject(object[props], collection2, lang)
  					}
  					
  				}
  				
  			}
  		}else{

  		}
  	}))

  }

  const mapDefineConfig = (component_define, define, lang)=>{
  	return new Promise((resolve, rejected)=>{
  		
  			var lpos = langposition[lang]
  		  	// console.log(component_define,Object.keys(define))
		  	if(Object.keys(define)){
		  		forEach(Object.keys(define), async(keyitem, index)=>{
			  		if(component_define[keyitem]){
			  			const df_cpn = component_define[keyitem]
			  			
			  			if(typeof df_cpn === 'object'){

			  				if(df_cpn.type == "text" || df_cpn.type == "string"){
			  				if(Array.isArray(define[keyitem])){
			  					
				  					if(!lpos){
				  						lpos = 0
				  					}

				  					if(define[keyitem][lpos]){
				  						define[keyitem] = define[keyitem][lpos]
				  					}else{
				  						define[keyitem] = define[keyitem][0]
				  					}
				  				}else{
				  					
				  				}
				  				
				  			}else if(df_cpn.type == "array"){

				  				if(df_cpn.item){
					  				define[keyitem].map(async (item_param, ind)=>{
					  					if(df_cpn.item.type == "object"){
						  					item_param = await parseArrayObjectParams(df_cpn.item.properties, item_param, lpos)
						  					return item_param
					  					}else{
											item_param = await parseArrayOtherParams(df_cpn.item.properties, item_param, lpos)
						  					return item_param
					  					}

					  				})
				  				}else{
				  					console.log("LAYOUT ERROR ","Missing 'item' config properties")
				  				}
				  			}else if(df_cpn.type == "object"){
				  				item_param = await parseObjectParams(df_cpn.properties, define[keyitem], lpos)
						  		return item_param
				  			}
			  			}

			  			
			  		}
			  		
			  	}, function(done){
			  		return resolve(define);
			  	})
		  	}else{
		  		return resolve(define);
		  	}
		  	
  	})


  }

  const mapDefineEditorConfig = (component_define, define, data_editor)=>{
  	return new Promise((resolve, rejected)=>{
  		
  			
  		  	// console.log(component_define,Object.keys(define))
		  	if(Object.keys(define)){
		  		forEach(Object.keys(define), async(keyitem, index)=>{
			  		if(component_define[keyitem]){
			  			const df_cpn = component_define[keyitem]
			  			
			  			if(typeof df_cpn === 'object'){

			  				if(df_cpn.type == "text" || df_cpn.type == "string"){
			  				if(Array.isArray(define[keyitem])){
			  					
				  					

				  					// if(define[keyitem][lpos]){
				  					// 	define[keyitem] = define[keyitem][lpos]
				  					// }else{
				  					// 	define[keyitem] = define[keyitem][0]
				  					// }
				  				}else{
				  					
				  				}
				  				
				  			}else if(df_cpn.type == "array"){

				  				if(df_cpn.item){
					  				define[keyitem].map(async (item_param, ind)=>{
					  					if(df_cpn.item.type == "object"){
						  					item_param = await parseArrayObjectParams(df_cpn.item.properties, item_param, lpos)
						  					return item_param
					  					}else{
											item_param = await parseArrayOtherParams(df_cpn.item.properties, item_param, lpos)
						  					return item_param
					  					}

					  				})
				  				}else{
				  					console.log("LAYOUT ERROR ","Missing 'item' config properties")
				  				}
				  			}else if(df_cpn.type == "object"){
				  				item_param = await parseObjectParams(df_cpn.properties, define[keyitem], lpos)
						  		return item_param
				  			}
			  			}

			  			
			  		}
			  		
			  	}, function(done){
			  		return resolve(define);
			  	})
		  	}else{
		  		return resolve(define);
		  	}
		  	
  	})


  }

  ///START PARSE BLOCK
  const parseArrayObjectParams = (param_define, param, lang_position)=>{

  	return new Promise((resolve, rejected)=>{
  		forEach(Object.keys(param), (keyitem, index)=>{
  		// console.log("jxxjxx ",param[keyitem])
  		if(param_define[keyitem]){
	  			// console.log(param_define[keyitem])
	  			if(param_define[keyitem].type == "text" || param_define[keyitem].type == "string"){
	  				if(param[keyitem][lang_position]){
	  					param[keyitem] = param[keyitem][lang_position]
	  				}else{
	  					param[keyitem] = param[keyitem][0]
	  				}
	  				
	  				
	  			}
	  		}
	  		
	  	}, function(){
	  		return resolve(param)
	  	})

  	})
  	
  }

   const parseObjectParams = (param_define, param, lang_position)=>{

  	return new Promise((resolve, rejected)=>{
  		forEach(Object.keys(param), (keyitem, index)=>{
  		// console.log("jxxjxx ",param[keyitem])
  		if(param_define[keyitem]){
	  			// console.log(param_define[keyitem])
	  			if(param_define[keyitem].type == "text" || param_define[keyitem].type == "string"){
	  				if(param[keyitem][lang_position]){
	  					param[keyitem] = param[keyitem][lang_position]
	  				}else{
	  					param[keyitem] = param[keyitem][0]
	  				}
	  				
	  				
	  			}
	  		}
	  		
	  	}, function(){
	  		return resolve(param)
	  	})

  	})
  	
  }

  const parseArrayOtherParams = (param_define, param, lang_position)=>{
  	return new Promise((resolve, rejected)=>{
  		return resolve(param)
  	})
  	
  }

  const parseInternalLanguage = (internal_language, lang)=>{
  	return new Promise((resolve, rejected)=>{
  		 var lpos = langposition[lang]
	  	if(!lpos){ lpos = 0}
	  		forEach(Object.keys(internal_language), (keyitem, index)=>{
	  			internal_language[keyitem] = internal_language[keyitem][lpos] || internal_language[keyitem][0]
	  		}, function(){
	  			return resolve(internal_language)
	  		})
  	})


  }

   ///END PARSE BLOCK



  const readObject = async (object, collection2, all_component_id)=>{
  	return Promise.all(Object.keys(object).map(async (props, index)=>{
  		if(typeof object[props] == "object"){
  			if(object[props] instanceof ObjectId){
  				all_component_id.push(object[props])
  				const web_layout_component = await collection2.find({_id: object[props]}).toArray()
  				object[props] = web_layout_component[0].component

  			}else{
  				await readObject(object[props], collection2, all_component_id)
  			}
  		}
  	}))

  }
  const mapRefObject = async (input, parent_path)=>{
  	for (var [key, child] of Object.entries(input)) {
  	// 	if(key.toString() == "component"){
	 		
	 	// 	if(child instanceof ObjectId){
	 	// 		const web_layout_component = await collection2.find({_id: child}).toArray()
	 	// 		child = web_layout_component[0].component

	 	// 	}
 		// }else{
 		// 	// console.log(key)
 		// }

	 	if(typeof child == "object"){
	 		if(Array.isArray(child)){
	 			const path = parent_path+"/p/"+key
	 			mapRefArray(child, path)
	 		}else{
	 			const path = parent_path+"/p/"+key
		 		if(child.type){
		 			child["$ref"] = path
		 		}
		 		mapRefObject(child, path)
	 		}
	 	}else{
	 		
	 	}
	}

  }

  const mapRefArray = (input, parent_path)=>{
  	input.forEach((child, index)=>{
  		if(typeof child == "object"){
	 		if(Array.isArray(child)){
	 			const path = parent_path+"/"+index
	 			mapRefArray(child, path, collection2)
	 		}else{
	 			const path = parent_path+"/i/"+index
		 		if(child.type){
		 			child["$ref"] = path
		 		}
		 		mapRefObject(child, path)
	 		}
	 	}
  	})

  }

  const getPageInstance = async (req, res, next) => {
	try {
	 	const database = MongoConnector.database();
	 	const collection = database.collection('web_instance');
	 	const web_instance = await collection.find({}).toArray()
	 	res.send({success: true, data: web_instance})
	} catch (e) {
	  console.log("GET PAGE INSTANCE ERROR ", e)
	  res.send({success: false, data: []})
	}
  };

  const getPageConfig = async (req, res, next) => {
	try {
	 	const database = MongoConnector.database();

	 	const collection = database.collection('page');
	 	
	 	const web_instance = await collection.find({web_instance_id: ObjectId(req.params.landingpage_id)}, {
							  projection: { _id: 1, permalink: 1 }
							}).toArray()

		res.send({success: true, data: web_instance})
		
	 	
	} catch (e) {
	  console.log("ERROR ", e);
	  res.send({success: false, data: []})
	}
  };

  const getPageFAQ = async (req, res, next)=>{
	try{
		var version="1"
		var lang_position = 0;
		if(req.query.lang){
			lang_position = req.query.lang;
		}
		var cache = true
		if(req.query.version){
			version = req.query.version
		}
		if(req.query.cache){
			cache = false
		}
		if(req.query.version){
			version = req.query.version
		}
		const p_faq = req.params.faq_id
	 	const database = MongoConnector.database();
	 	const collection = database.collection('faq_config');
		const all_faq = await collection.find({_id: ObjectId(p_faq)}).toArray()


		if(all_faq[0]){
			var faq_data = all_faq[0].data.sort(function (a, b) {
				return a.sort_order - b.sort_order;
			});

			faq_data.map((item, index)=>{
				item.title = item.title[lang_position]
				let itemData = item.content.sort(function (c, d) {
					return c.sort_order - d.sort_order;
				});
				itemData.map((child1, index1)=>{
					child1.title = child1.title[lang_position]
					child1.content = child1.content[lang_position]
					return child1;
				})
				return item
			})


			res.send({success: true, data: faq_data})
		}else{
			res.send({success: false, data: null})
		}
		
	}catch(e){
		console.log(e)
		res.send({success: false, data: null})
	}
}




const getPageFAQDEV = async (req, res, next)=>{
	try{
		var version="1"
		var cache = true
		
		const p_faq = req.params.faq_id
	 	const database = MongoConnector.database();
	 	const collection = database.collection('web_faq_content');
	 	const collection_language = database.collection('language_translation');
		const all_faq = await collection.find({faq_id: ObjectId(p_faq)}).toArray()

		var all_faq_result = []
		forEach(all_faq, async function (item, index){
				var done1 = this.async()
					var isx_title =  item.title.replace("<lang>", "");
					isx_title = isx_title.replace("</lang>", "")
					const axv_title = await collection_language.find({translate_key: isx_title}).toArray()
					const axv_title_result = axv_title[0]

					if(item.content){
						var isx_content =  item.content.replace("<lang>", "");
						isx_content = isx_content.replace("</lang>", "")
						const axv_content = await collection_language.find({translate_key: isx_content}).toArray()
						const axv_content_result = axv_content[0]
						item.content = [axv_content_result.vi, axv_content_result.en] //title
					}
					

					
					item.title = [axv_title_result.vi, axv_title_result.en] //title

					all_faq_result.push(item)
				done1()
		}, function(){
				// console.log("nhay vao done", all_faq_result)

				let object_lang_out = {}
				var faq_out = []
				if(all_faq_result){

					var faq_parent = all_faq_result.filter((item2)=>{
						return item2.parent_id == null
					});

					forEach(faq_parent, async function(item3, index){
						

						var done = this.async()
						item3.content = all_faq_result.filter((childitem)=>{
							if(childitem.parent_id){
								return childitem.parent_id.toString() === item3._id.toString();
							}
						});

						


						await timeout(1000);
						faq_out.push(item3);

						done()
					}, async function(){

						var f_out = []
						faq_out.forEach((faq_item, index)=>{
							var d = faq_item
							delete d._id
							delete d.faq_id
							delete d.parent_id
							d.content.map((faq, index)=>{
								delete faq._id
								delete faq.faq_id
								delete faq.parent_id
							})
							f_out.push(d)
						})


						console.log("nhay vao done 3 ", faq_parent.length)


						res.send(faq_out)
					})
				}else{
					// res.send({success: false, data: null})
				}
		})


		
		
	}catch(e){
		console.log(e)
		res.send({success: false, data: null})
	}
}

const timeout = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}




const getPageRelationProduct = async (req, res, next)=>{
	try{
		var version="1"
		var lang_position = 0
		if(req.query.lng){
			if(langposition[req.query.lng]){
				lang_position = langposition[req.query.lng]
			}
		}
		var cache = true
		if(req.query.version){
			version = req.query.version
		}
		
	 	const database = MongoConnector.database();
	 	const collection = database.collection('products');
	 	var query_obj = {}
	 	if(req.query.exc){
	 		query_obj = {product_code:{ $ne: req.query.exc }}
	 	}
		const all_product = await collection.find(query_obj).toArray()
		res.send({success: true, data: all_product})
	}catch(e){
		console.log(e)
		res.send({success: false, data: null})
	}
}


const getPageProduct = async (req, res, next)=>{
	try{
		var version="1"
		var lang_position = 0
		if(req.query.lng){
			if(langposition[req.query.lng]){
				lang_position = langposition[req.query.lng]
			}
		}
		var cache = true
		if(req.query.version){
			version = req.query.version
		}
		
	 	const database = MongoConnector.database();
	 	const collection_product_group = database.collection('product_group');
	 	const collection = database.collection('products');
	 	const group_product = await collection_product_group.find({_id: ObjectId(req.params.group_id)}).toArray()
	 	let id_list = []
	 	if(group_product[0]){
	 		if(group_product[0].list_product){
	 			group_product[0].list_product.forEach((p_item, index)=>{
	 			  id_list.push(p_item.product)
	 			})
	 			var query_obj = {}
			 	if(req.query.exc){
			 		query_obj = {_id: { $in: id_list }, product_code:{ $ne: req.query.exc }}
			 	}
				const all_product = await collection.find(query_obj).toArray()
				return res.send({success: true, data: all_product})
	 		}else{
	 			return res.send({success: false, data: null})
	 		}
	 	}else{
	 		return res.send({success: false, data: null})
	 	}
	 	
		
	}catch(e){
		console.log(e)
		res.send({success: false, data: null})
	}
}



const sendUserRequestContact = async (req, res, next)=>{
	try{
	 	const database = MongoConnector.database();
	 	const collection = database.collection('user_contact_info');

		await collection.insert({
			gender: req.body.gender,
			name: req.body.name,
			email: req.body.email,
			phone: req.body.phone,
			landing: ""
		})
		res.send({success: true, data: null})
	}catch(e){
		console.log(e)
		res.send({success: false, data: null})
	}
}


const testgetTracking = async (req, res, next)=>{
	try{
		var token = jwt.sign({ user_id: '123456' }, 'hdi-secret@2021');
		res.send({token: token})
	}catch(e){
		console.log(e)
		res.send({success: false, data: null})
	}
}





module.exports = {
	getLayout,
	getLayoutDefine,
	getLayoutEditor,
	getPageInstance,
	getPageConfig,
	getPageFAQ,
	getPageFAQDEV,
	sendUserRequestContact,
	getPageRelationProduct,
	getPageProduct,
	//cms
	getDevLang,
	getCMSLayoutDefine,
	getPage,
	getForm,
	changeLanguage,
	getCurrentLanguage,
	testgetTracking
}
