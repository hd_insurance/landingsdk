const HDIRequest = require('../service/request');
const config = require('../config');
const {formbuy} = require('../constant');


const submit = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain2+'/hdi/orders/regisHG', req.body, formbuy.submit)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send("error")
	}
	
}


module.exports = {
	submit: submit
}
