const HDIRequest = require('../service/request');
const config = require('../config');
const RedisConnector = require('../service/redis');
const { bhsk } = require('../constant');

const ttl = 20
const voucherValidation = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const browser_code = req.body.browser_code;
		const redis = new RedisConnector()
		const client = redis.getClient()

		const data = {
						P_CHANNEL: req.body.org,
				        P_USERNAME: config.auth.username,
				        P_ACTIVATION_CODE: req.body.code
				    }
		const response = await rq.send(config.domain1+'/open-api', data, bhsk.voucher_validation)
		
		const uselimit = response.data[0][0].USE_LIMIT
		const count = response.data[0][0].USE_NO
		if((uselimit > 1) && (count+1 < uselimit)){ //ko lock
			return res.send(response)
		}else{ //lock me no lai
			const value = await client.get(`${config.redis.prefix}${req.body.code.toUpperCase()}`);
			if(value!=null){
				if(value!=browser_code){
					const error_response = { 
					  "success": false,
					  "error": null,
					  "error_message": "Voucher tạm thời không khả dụng"}
					return res.send(error_response)
				}else{
					return res.send(response)
				}
			}else{
				await client.set(`${config.redis.prefix}${req.body.code.toUpperCase()}`, browser_code, 'EX', ttl * 60);
				return res.send(response)
			}
		}
	}catch(e){
		console.log(e)
		const error_response = { 
		  "success": false,
		  "error": null,
		  "error_message": "Có lỗi xảy ra! Thử lại sau."}
		  if(e.error_message){
		  	return res.send(e)
		  }else{
		  	return res.send(error_response)
		  }
		
	}
}




const voucherRemoveLock = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const browser_code = req.body.browser_code;
		const redis = new RedisConnector()
		const client = redis.getClient()
		const value = await client.get(`${config.redis.prefix}${req.body.code.toUpperCase()}`);
		if(value!=null){
			await client.del(`${config.redis.prefix}${req.body.code.toUpperCase()}`);
		}
		return res.send({success:true})
	}catch(e){
		return res.send({success:true})
	}
}




const getPackList = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain2+'/OpenApi/Post', {org_code: req.params.org_code}, bhsk.get_packlist)
		if(response.Success){
			var pos = req.query.lang=="en"?1:0;
			if(response.Data[0][pos]){
				res.send(JSON.parse(response.Data[0][pos].JSON_DATA))
			}else{
				console.log("GET PACKAGE ERROR 0")
				res.send([])
			}

		}else{
			console.log("GET PACKAGE ERROR 1")
			res.send([])
		}
		
	}catch(e){
		console.log("GET PACKAGE ERROR ",e)
		res.send([])
	}
}

const register = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain2+'/hdi/orders/regisHG', req.body, bhsk.register)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send(e)
	}
}

const updateregister = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain2+'/hdi/orders/regisHG', req.body, bhsk.update_register)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send(e)
	}
}

const verifyregister = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain2+'/hdi/orders/getStaffByid', {CUS_CODE: req.params.cus_id}, bhsk.verify_register)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send(e)
	}
}


const getRegister = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain2+'/hdi/orders/getStaffByid', {CUS_CODE: req.params.cus_id}, "GET_REGIS")
		res.send(response)
	}catch(e){
		console.log(e)
		res.send(e)
	}
}



const getFAQ = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()

		const response = await rq.send(config.domain1+'/open-api',
			{"channel": "WEB", "org_code": req.params.org_code, "product": "", "pack": "", "lang":req.params.lang}, bhsk.get_faq)
		if(response.data[0]){
			res.send(response.data[0])
		}else{
			res.send([])
		}
	}catch(e){
		console.log("GET FAQ ERROR ",e)
		res.send([])
	}
}

const calculatorFee = async (req, res, next) => {
	try {
	  const rq = new HDIRequest();
	  let paramsCalcFee = {
		A1: req.body.a1 || "",
        A2: req.body.a2 || "",
        A3: req.body.a3 || "",
        A4: req.body.a4 || "",
        A5: req.body.a5 || "",
        A6: req.body.a6 || "",
        A7: req.body.a7 || "",
        A8: req.body.a8 || "",
        A9: req.body.a9 || "",
        A10: req.body.a10 || "",
        A11: req.body.a11 || "",
        A12: req.body.a12 || "",
        A13: req.body.a13 || "",
        A14: req.body.a14 || "",
        A15: req.body.a15 || "",
        A16: req.body.a16 || "",
	  }
	  const params = {
		p_project: "",
		p_username: "",
		p_o_business: JSON.stringify(paramsCalcFee),
	  };
	  const response = await rq.send(
		config.domain1 + "/open-api",
		params,
		bhsk.calc_fee
	  );
	  if (response.data) {
		res.send(response.data);
	  } else {
		res.send([]);
	  }
	} catch (e) {
	  console.log("CALCULATOR FEE ERROR ", e);
	  res.send([]);
	}
  };




module.exports = {
	getPackList: getPackList,
	register: register,
	getRegister:getRegister,
	updateregister: updateregister,
	verifyregister: verifyregister,
	voucherValidation: voucherValidation,
	voucherRemoveLock: voucherRemoveLock,
	getFAQ: getFAQ,
	calculatorFee: calculatorFee,
}
