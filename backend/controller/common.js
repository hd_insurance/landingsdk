const HDIRequest = require('../service/request');
const config = require('../config');
const { common } = require('../constant');



const getLocation = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain1+'/open-api', {p_utype: req.params.utype, p_code:req.params.code}, common.get_location)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send({"success":false, data:[[]]})
	}
	
}

const getBankList = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain1+'/open-api', {p_project: "", p_username: "VIETJET_VN"}, common.get_banklist)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send({"success":false, data:[[]]})
	}
	
}


const getPageInstance = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.normalSend(config.domain1+'/api/pages-instance', {}, common.act_get)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send({"success":false, data: null})
	}
	
}

const getPageConfig = async (req, res, next)=>{
	try{
		var version="1"
		if(req.query.version){
			version = req.query.version
		}
		const rq = new HDIRequest()
		const response = await rq.normalSend(config.domain1+'/api/pages-config/'+req.params.page_id+'?version='+version, {}, common.act_get)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send({"success":false, data: null})
	}
}

const getPageLanguage = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		var version="1"
		if(req.query.version){
			version = req.query.version
		}
		const response = await rq.normalSend(config.domain1+'/api/lang/'+req.params.page_id+'?version='+version, {}, common.act_get)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send({"success":false, data: null})
	}
}


const getPageFAQ = async (req, res, next)=>{
	try{
		var version="1"
		if(req.query.version){
			version = req.query.version
		}
		const rq = new HDIRequest()
		const response = await rq.normalSend(config.domain1+'/api/faq/'+req.params.faq_id+'?version='+version, {}, common.act_get)
		res.send(response)
	}catch(e){
		console.log(e)
		res.send({"success":false, data: null})
	}
}


module.exports = {
	getLocation: getLocation,
	getBankList: getBankList,
	getPageInstance: getPageInstance,
	getPageConfig: getPageConfig,
	getPageLanguage: getPageLanguage,
	getPageFAQ: getPageFAQ
}
