const HDIRequest = require("../service/request");
const config = require("../config");
const RedisConnector = require("../service/redis");
const { sdk } = require("../constant");
const LogService = require("../service/LogService");

const ttl = 20;

const gettempcusinfo = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const response = await rq.send(
      config.domain1 + "/open-api",
      {
        P_REF_ID: req.query.ref_id,
        P_ORDER_ID: "",
      },
      sdk.get_temp_cusinfo
    );
    if (response.success) {
      // const obj_product = response.data[0][0].OBJ_PRODUCT;
      res.send(response.data);
    } else {
      console.log("GET cus infor ERROR 1");
      res.send([]);
    }
  } catch (e) {
    console.log("GET cus infor ERROR ", e);
    res.send([]);
  }
};

const getDefine = async (req, res, next) => {
  const req_id = LogService.makeid()
  let log_request = {
      method: "post",
      url: "/open-api"
    }
  try {
    
    
    const rq = new HDIRequest();

    const defineParams = {
      A1: req.params.org_code,   // ORG_CODE
      A2: req.params.product_code, // PRODUCT_CODE
      A3: req.query.sku || "",
      A4: req.params.channel,
      A5: req.params.language,
      A6: "",
      A7: "",
      A8: "",
      A9: "",
      A10: "",
      A11: "",
      A12: "",
      A13: "",
      A14: "",
      A15: "",
      A15:""
    }
    log_request.request = defineParams
    const response = await rq.send(
      config.domain1 + "/open-api",
      {
        P_PROJECT: "LANDING_SDK",
        P_USERNAME: "X",
        P_O_BUSINESS: JSON.stringify(defineParams)
      },
      "APIY6GZ0T7"
    );
    log_request.response = response
    LogService.insert(req_id, `[${process.env.NODE_ENV}] - GET DEFINE`, "info", "request", log_request)
    if (response.success) {
      res.send(response);
    } else {
      console.log("GET PACKAGE ERROR 1");
      LogService.insert(req_id, "GET Define Error", "error", "request", log_request)
      res.send([]);
    }
  } catch (e) {
    console.log("GET PACKAGE ERROR ", e);
    LogService.insert(req_id, "GET Define Error (1)", "error", "request", {error: e, request: log_request})
    res.send([]);
  }
};




const checkCMND = async (req, res, next) => {
  try {
    const { arr_check } = req.body;
    const rq = new HDIRequest();
    const data = {
      product_code: req.params.product_code, //CSSK_NV
      textsearch: [...arr_check],
    };
    const params = {
      p_project: "",
      p_username: req.params.username,
      p_org_code: req.params.org_code,
      p_o_business: JSON.stringify(data),
    };
    
    const response = await rq.send(
      config.domain1 + "/open-api",
      params,
      sdk.check_cmnd
    );
    console.log(response);
    if (response.success) {
      res.send(response);
    } else {
      console.log("CHECK CMND ERROR 1");
      res.send([]);
    }
  } catch (e) {
    console.log("CHECK CMND ERROR ", e);
    res.send([]);
  }
};
const registerVCX = async (req, res, next) => {
  try {
    const data = req.body;

    const rq = new HDIRequest();
    const params = {
      p_project: "",
      p_username: "landing",
      p_org_code: req.params.org_code,
      p_o_business: JSON.stringify(data),
    };
    
    const response = await rq.send(
      config.domain1 + "/open-api",
      params,
      sdk.registerVCX
    );
    if (response.success) {
      res.send({ success: true, data: response.data });
    }
    // console.log(response)
  } catch (error) {
    console.log(error);
    res.send({ success: false });
  }
};
const createOder = async (req, res, next) => {
  const req_id = LogService.makeid()

  let url = '/OpenApi/v1/mask/insur/create_pay';
  if(req.cookies){
    if(req.cookies.hdi_tracking){
      url+=`?tracking=${req.cookies.hdi_tracking}`;
    }
  }
  
  let log_request = {
    method: "post",
    url: config.domain2 + url
  }
  try {

    const { SELLER, BUYER, HEALTH_INSUR, PAY_INFO, HOUSE_INSUR, BILL_INFO} = req.body;
    const rq = new HDIRequest();
    
    let data = {
      CHANNEL: req.params.channel,
      USERNAME: req.params.username,
      PRODUCT_CODE: req.params.product_code,
      CATEGORY: req.params.category,
      ACTION: req.params.action,
      SELLER: SELLER,
      BUYER: BUYER,
      BILL_INFO: BILL_INFO,
      HEALTH_INSUR: HEALTH_INSUR,
      PAY_INFO: PAY_INFO,
    }

    if(req.params.category == 'TS10'){
      data = {
        CHANNEL: req.params.channel,
        USERNAME: req.params.username,
        PRODUCT_CODE: req.params.product_code,
        CATEGORY: req.params.category,
        ACTION: req.params.action,
        SELLER: SELLER,
        BUYER: BUYER,
        BILL_INFO: BILL_INFO,
        HOUSE_INSUR: HOUSE_INSUR,
        PAY_INFO: PAY_INFO,
      }
    }

    log_request.request = data
    
    const response = await rq.send(
      config.domain2 + url,
      data,
      sdk.create_order_healthy365
    );
    log_request.response = response
    LogService.insert(req_id, `[${process.env.NODE_ENV}] - SDK Create Order`, "info", "request", log_request)
    if (response.Success) {
      const orderId = response.Data.orderId;
      if (req.query.ref_id) {
        const response_update_order = await rq.send(
          config.domain1 + "/open-api",
          {
            P_REF_ID: req.query.ref_id,
            P_ORDER_ID: orderId,
            P_STATUS: "WAIT_PAY",
          },
          sdk.update_cus_contract
        );
      }
      res.send(response);
    } else {
      console.log("CREATE ORDER ERROR 1");
      if(typeof e == "object"){
        log_request.response = e
      }else{
        log_request.content = e.toString()
      }
      
      LogService.insert(req_id, "SDK Create Order Error", "error", "request", log_request)
      res.send(response);
    }
  } catch (e) {
    console.log("CREATE ORDER ERROR", e);
    if(typeof e == "object"){
        log_request.response = e
    }else{
        log_request.content = e.toString()
    }
    LogService.insert(req_id, "SDK Create Order Error (1)", "error", "request", log_request)
    res.send(e);
  }
};







const getLandingMultipleProduct = async (req, res, next) => {
  try {
    const rq = new HDIRequest();

    let data = {
      "UserName": "",
      "Channel": "",
      "lang":"VN",
      "osku_code": req.params.id
    }

    const response = await rq.send(
      config.domain2 + "/OpenApi/hdi/landing/master", data, sdk.get_data_master_screen
    );

    if (response.Success) {
      const orderId = response.Data.orderId;
     
      res.send(response);
    } else {
      console.log("CREATE ORDER ERROR 1");
      res.send([]);
    }
  } catch (e) {
    console.log("CREATE ORDER ERROR ", e);
    res.send([]);
  }
};

const getDetailProduct = async (req, res, next) => {
  try {
    const rq = new HDIRequest();

    let data = {
      "UserName": "",
      "Channel": "",
      "lang":"VI",
      "osku_code": req.params.id,
      "detail_code": req.params.detail_id
    }

    console.log(data)
    const response = await rq.send(
      config.domain2 + "/OpenApi/Post", data, sdk.get_detail_product
    );

    if (response.Success) {
      res.send(response);
    } else {
      console.log("CREATE ORDER ERROR 1");
      res.send([]);
    }
  } catch (e) {
    console.log("CREATE ORDER ERROR ", e);
    res.send([]);
  }
};



module.exports = {
  getDefine,
  createOder,
  gettempcusinfo,
  checkCMND,
  registerVCX,
  getLandingMultipleProduct,
  getDetailProduct
};
