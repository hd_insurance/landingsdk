var needle = require('needle');
const https = require('https');
var CryptoJS = require("crypto-js");
const config = require('../config');
const FormData = require('form-data');
const RedisConnector = require('./redis');

class Request {
	constructor(initconfig = {}) {
		this.login = this.login.bind(this)
		this.getOption = this.getOption.bind(this)
		this.signRequest = this.signRequest.bind(this)
		this.send = this.send.bind(this)
		this.normalSend = this.normalSend.bind(this)

		this.defaultConfig = {
			parent_code: initconfig.parent_code?initconfig.parent_code : config.auth.parent_code,
	        username: initconfig.username?initconfig.username : config.auth.username,
	        client_id: initconfig.client_id?initconfig.client_id : config.auth.CLIENT_ID,
	        secret: initconfig.secret?initconfig.secret : config.auth.secret,
	        password: initconfig.password?initconfig.password : config.auth.password
		}
	}

	normalSend(url, data, method){

		return new Promise(async (resolved, rejected)=>{
			const agent = new https.Agent({  
		        rejectUnauthorized: false
		      }); 
		    var options = {
		  		json:true,
				headers: {
				 'Access-Control-Allow-Headers': 'Content-Type',
				 'Access-Control-Allow-Methods': method.toUpperCase(),
				 "Content-Type": "application/json",
				 'Access-Control-Allow-Origin': '*'
				  }
				}   

			if(process.env.USE_SSL == "true"){
				options.agent = agent
			}

			needle.request(method, url, data, options, async function(err, resp) {
			    if(err){
			    	return rejected(err)
			    }else{
			    	return resolved(resp.body)
			    }
			});
		})
		
	}

	send(url, data, actioncode, isFirstload=true){
		return new Promise(async (resolved, rejected)=>{
			const redis = new RedisConnector()
			const client = redis.getClient()
			let token_value = await client.get("LANDING_TOKEN");

			const signature = this.signRequest(actioncode, data)
			const params = {
					Device: {
						DeviceId: "",
						DeviceCode: "",
						Device_name: "",
						IpPrivate: config.auth.ipPrivate,
						IpPublic: "",
						X: "",
						Y: "",
						province: "",
						district: "",
						wards: "",
						address: "",
						environment: "LIVE",
						browser: "",
						DeviceEnvironment: "WEB"
					},
					Action: {
						ParentCode: this.defaultConfig.parent_code,
				        UserName: this.defaultConfig.username,
				        Secret: this.defaultConfig.secret,
						ActionCode: actioncode
					},
					Data: data,
					Signature: signature
				}
			if(token_value==null){
				const loginresult = await this.login()
				if(loginresult){
					config.auth.token = loginresult
				}else{
					return rejected("Login failed")
				}
			}
			token_value = await client.get("LANDING_TOKEN");
			const options = this.getOption(token_value)
			const that = this
			needle.request('post', url, params, options, async function(err, resp) {
			  if (!err && resp.statusCode == 200){
			  	if(resp.body.success||resp.body.Success){
			  		return resolved(resp.body)
			  	}else{
			  		console.log("HDI-Request Error x : ", resp.body)
			  		return rejected(resp.body)
			  	}
			  }else{
			  	if(resp.body.Error == 'ERROR_2004'){
			  		if(isFirstload){
			  			const loginresult = await that.login()
						if(loginresult){
							config.auth.token = loginresult
							const options = that.getOption(loginresult)

							console.log("loginresult ", loginresult, url)

							needle.request('post', url, params, options, async function(err, resp) {
							  if (!err && resp.statusCode == 200){
							  	if(resp.body.success||resp.body.Success){
							  		return resolved(resp.body)
							  	}else{
							  		console.log("HDI-Request Error x : ",err)
							  		return rejected(resp.body)
							  	}
							  }else{
							  	console.log("HDI-Request Error x3 : ",err)
							  	return rejected(resp.body)
							  }
							    
							});
						}else{
							return rejected("Login failed")
						}
			  		}else{
			  			console.log("HDI-Request Error x4: ",err, resp)
			  			return rejected(false)
			  		}
			  	}else{
			  		console.log("HDI-Request Error x2: ",err, resp.body)
			  		return rejected(false)
			  	}
			  	
			  }
			    
			});



		})
		
	}


	sendFile(url, files, isFirstload=true){
		return new Promise(async (resolved, rejected)=>{
			const signature = null

			console.log(files)


			const form_data = new FormData();
			form_data.append("files", "aaa");
			

			if(config.auth.token==null){
				const loginresult = await this.login()
				if(loginresult){
					config.auth.token = loginresult
				}else{
					return rejected("Login failed")
				}
			}
			const options = this.getOptionUpload(config.auth.token)
			const that = this
			needle.request('post', url, form_data, options, async function(err, resp) {
			  if (!err && resp.statusCode == 200){
			  	if(resp.body.success||resp.body.Success){
			  		return resolved(resp.body)
			  	}else{
			  		// console.log("HDI-Request Error x : ",err)
			  		return rejected(resp.body)
			  	}
			  }else{
			  	if(resp.body.Error == 'ERROR_2004'){
			  		if(isFirstload){
			  			const loginresult = await that.login()
						if(loginresult){
							config.auth.token = loginresult
							const options = that.getOptionUpload(loginresult)
							needle.request('post', url, form_data, options, async function(err, resp) {
							  if (!err && resp.statusCode == 200){
							  	if(resp.body.success||resp.body.Success){
							  		return resolved(resp.body)
							  	}else{
							  		// console.log("HDI-Request Error x : ",err)
							  		return rejected(resp.body)
							  	}
							  }else{
							  	// console.log("HDI-Request Error x3 : ",err)
							  	return rejected(resp.body)
							  }
							    
							});
						}else{
							return rejected("Login failed")
						}
			  		}else{
			  			// console.log("HDI-Request Error x4: ",err, resp)
			  			return rejected(false)
			  		}
			  	}else{
			  		// console.log("HDI-Request Error x2: ",err, resp)
			  		return rejected(false)
			  	}
			  	
			  }
			    
			});



		})
		
	}


	signRequest(actionCode, data){
		const constantsText = "HDI";
		const str_data = JSON.stringify(data)
		const str_md5 = CryptoJS.MD5(str_data)
		const raw = constantsText + config.auth.deviceCode + config.auth.ipPrivate + config.auth.deviceEnvironment + this.defaultConfig.username + this.defaultConfig.secret + actionCode + this.defaultConfig.parent_code + str_md5.toString(CryptoJS.enc.Hex).toUpperCase() + constantsText;
		const encryptedMD5 = CryptoJS.MD5(raw)
		const sha256 = CryptoJS.HmacSHA256(encryptedMD5.toString(CryptoJS.enc.Hex).toUpperCase(), this.defaultConfig.client_id)
		return sha256.toString(CryptoJS.enc.Hex).toUpperCase()
	}
	getOption(token){
		const agent = new https.Agent({  
	        rejectUnauthorized: false
	      }); 
	    var rm = {
	  		json:true,
			headers: {
			 'Access-Control-Allow-Headers': 'Content-Type',
			 'Access-Control-Allow-Methods': 'POST',
			 "Content-Type": "application/json",
			 'Access-Control-Allow-Origin': '*',
			 "Token": ""+token  }
			}   

		if(process.env.USE_SSL == "true"){
			rm.agent = agent
		}
	  	return rm
	}

	getOptionUpload(token){
		const agent = new https.Agent({  
	        rejectUnauthorized: false
	      }); 
	    var rm = {
	  		json:true,
			headers: {
			 'Access-Control-Allow-Headers': 'Content-Type',
			 'Access-Control-Allow-Methods': 'POST',
			 "Content-Type": "application/json",
			 'Access-Control-Allow-Origin': '*',
			 'ParentCode':this.defaultConfig.parent_code,
			 'UserName':this.defaultConfig.username,
			 'Secret': this.defaultConfig.secret,
			 'environment': "LIVE",
			 'DeviceEnvironment': "WEB",
			 'ActionCode': "UPLOAD_VJ",
			 "token": ""+token  }
			}   

		if(process.env.USE_SSL == "true"){
			rm.agent = agent
		}
	  	return rm
	}


	login(){
		return new Promise(async (resolved, rejected)=>{
			try{
				const redis = new RedisConnector()
				const client = redis.getClient()
				// console.log(new Date().getTime() - config.auth.lastLogin)
				const params = {
						Device: {
							DeviceId: "",
							DeviceCode: "",
							Device_name: "",
							IpPrivate: "123",
							IpPublic: "",
							X: "",
							Y: "",
							province: "",
							district: "",
							wards: "",
							address: "",
							environment: config.app.ENVIRONMENT,
							browser: "",
							DeviceEnvironment: "WEB"
						},
						Action: {
							ParentCode: this.defaultConfig.parent_code,
							UserName: this.defaultConfig.username,
							Secret: this.defaultConfig.secret,
							ActionCode: "HDI_API_LOGIN"
						},
						Data: {
							UserName: this.defaultConfig.username,
							Password: this.defaultConfig.password
						},
						Signature: ""
					}

					const options = this.getOption("xxxx")

					needle.request('post', config.domain1+'/api/login', params, options, async function(err, resp) {
					  if (!err && resp.statusCode == 200){
					  	if(resp.body.Success){
					  		config.auth.token = resp.body.Token
					  		config.auth.lastLogin = new Date().getTime()
					  		config.auth.expireIn = resp.body.Expries
					  		await client.set("LANDING_TOKEN", resp.body.Token);
					  		return resolved(resp.body.Token)
					  	}else{
					  		client.close()
					  		return resolved(false)
					  	}
					  }else{
					  	console.log("LoginError", resp)
					  	console.log("LoginError", err)
					  	client.close()
					  	return resolved(false)
					  }
					    
					});
			}catch(e){
				console.log("Login Error  " , e)
				return resolved(false)
			}
		})
	}
}
module.exports = Request