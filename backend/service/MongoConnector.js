// MongoConnector.js
const config = require('../config');
const MongoClient = require('mongodb').MongoClient;
let client;
let _db;
const options = {
  useNewUrlParser: true,
  reconnectTries: 60,
  reconnectInterval: 1000,
  poolSize: 10,
  bufferMaxEntries: 0
}

module.exports = async () => {
  // this gives you client
  const url = `mongodb://${config.mongo.host}:${config.mongo.port}/`
  try {
    client = await MongoClient.connect(url, options);
    _db = client.db(config.mongo.db)

    // const collection = _db.collection('web_instance');
    // const test = await collection.find({}).toArray()
    console.log("Connect mongo success ========================  ")
  } catch (e) {
    console.log("-----------------------------")
    console.log("Could not connect to mongodb");
    console.log(e)
    console.log("-----------------------------")
    client.close()
  }
}

module.exports.get = () => client;
module.exports.database = () => {
  return _db;
}

module.exports.close = () => client.close()