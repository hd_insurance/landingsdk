const HDIRequest = require("../service/request");

const log_service = "https://dev-hyperservices.hdinsurance.com.vn/logs"

const insert = async (req_id, log_title, evt, log_type, content)=>{
	try{
		const rq = new HDIRequest();
		let source = null
 		const e = new Error();
	  const regex = /\((.*):(\d+):(\d+)\)$/
	  const match = regex.exec(e.stack.split("\n")[2]);
	  if(match.input){
	  	source = match.input.toString()
	  }
		const logs_data = {
		    "group_id": req_id,
		    "app": "615f16ab242c430775e52b68",
		    "title": log_title,
		    "source":source,
		    "severity": evt,
		    "type": log_type,
		    "content": content
		}
		await rq.normalSend(log_service, logs_data, "post")
	}catch(e){

	}
}

const makeid = () => {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 16; i++ ) {
      result += characters.charAt(Math.floor(Math.random() *  charactersLength));
   }
   return result;
}

module.exports = {
	insert,
	makeid
}