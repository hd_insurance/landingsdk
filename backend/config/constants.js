const default_org = "HDI";
const envVariables = {
  products: {
    "suc-khoe-365": {
      PRODUCT: "SUCKHOE365",
      CHANNEL: "SDK_HEALTH",
      CATEGORY: "CN.03",
      VIEW: "SUCKHOE_1",
      ORG_CODE: default_org,
      COMPONENT: "60dd84adc8718dd12b8166af"
    },
    "xe-co-gioi-tnds-oto": {
      PRODUCT_BB: "XCG_TNDSBB_NEW",
      PRODUCT_TN: "XCG_TNDSTN",
      PACK_CODE_BB: "TNDSBB",
      PACK_CODE_TN: "TNDSTN",
      TYPE: "car",
      CHANNEL: "SELLING",
      CATEGORY: "XE",
      VIEW: "TNDS_1",
      ORG_CODE: default_org,
      USERNAME: "ADMIN",
      ACTION: "BH_M",
      COMPONENT: "60dbcd17c8718dd12b81669d"
    },
    "xe-co-gioi-tnds-xe-may": {
      PRODUCT_BB: "XCG_TNDSBB_NEW",
      PRODUCT_TN: "XCG_TNDSTN",
      PACK_CODE_BB: "TNDSBB",
      PACK_CODE_TN: "TNDSTN",
      TYPE: "motor",
      CHANNEL: "SELLING",
      CATEGORY: "XE",
      VIEW: "TNDS_1",
      ORG_CODE: default_org,
      USERNAME: "ADMIN",
      ACTION: "BH_M",
      COMPONENT: "60dbcd17c8718dd12b81669d"
    },
    "register-insurance.hdi": {
      PRODUCT: "CSSK_NV",
      CHANNEL: "SDK_HEALTH",
      CATEGORY: "CN.02",
      ACTION: "BH_M",
      USERNAME: "Seller_TN",
      MAX_AGE: 60,
      MIN_AGE: 1
    },
    "nha-tu-nhan": {
      ORG_CODE: default_org,
      PRODUCT: "NHATN",
      CHANNEL: "SDK_HOUSE",
      CATEGORY: "TS10",
      ACTION: "BH_M",
      USERNAME: "ADMIN",
      VIEW: "NTN",
      ORG_CODE_CREATE: default_org,
    },
    "vat-chat-xe": {
      ORG_CODE: default_org,
      COMPONENT: "60e412ac675c614e1cffee08",
      VIEW: "VCX",
    },
  },
};
module.exports = envVariables;