require('dotenv').config()

const envVariables = {
  domain1: process.env.DM1,
  domain2: process.env.DM2,
  app:{
    ENVIRONMENT: "LIVE",
  },
  redis:{
    host: process.env.REDIS_HOST || '103.237.145.159',
    password: process.env.REDIS_PASSWORD || "4rc0x1VLAVpARaCfl0YT",
    port: 6379,
    db: process.env.REDIS_DB || "10",
    prefix:"NJS_VOUCHER_"
  },
  mongo:{
    host: process.env.MG_DB_HOST || '103.237.145.159',
    password: process.env.MG_DB_PASS || "4rc0x1VLAVpARaCfl0YT",
    port: process.env.MG_DB_PORT,
    db: process.env.MG_DB_SCHEME || "10"
  },
  auth:{
      deviceCode: "",
      deviceEnvironment: "WEB",
      ipPrivate: "123",
      parent_code: process.env.PTN_CODE,
      username: process.env.USR_NAME,
      secret: process.env.SECRET,
      password: process.env.PASSWORD,
      CLIENT_ID: process.env.CLIENT_ID,
      token: null,
      lastLogin: 0,
      expireIn: 0
  }
}
module.exports = envVariables;