const bhsk = {
  voucher_validation: "APIX52834X",
  get_packlist: "APIAJL9HJA",
  register: "INSERT_REGIS",
  update_register: "UPDATE_REGIS",
  verify_register: "VERIFY_REGIS",
  get_register: "GET_REGIS",
  get_faq: "APIAB7JLAV",
  calc_fee: "APIXE6WGR9",
}

const sdk = {
  get_define: "APISZ0XSJ1",
  create_order_healthy365: "APIK70NHXK",
  get_temp_cusinfo: "GET_TEMP_CUSINFO",
  update_cus_contract: "UPDATE_CONTRACT_CUS_INFO",
  check_cmnd: "APIGWJB8AG",
  registerVCX: "APITTZIZ47",
  get_data_master_screen: "APIIKCA6QA",
  get_detail_product: "APIH5FONSD"
}

const car = {
  get_define: "APISZ0XSJ1",
  act_car: "APIZTPI8X1",
  preview_gcn: "GET_TEMPLATE",
}

const common = {
  get_location: "GET_LOCATION",
  get_banklist: "HDI_48",
  act_get: "get",
}

const formbuy = {
  submit: "INSERT_REGIS",
}

const visa = {
  search_gcn: "APIPZEJK5E",
}

const vietjet = {
  get_airport: "GET_AIRPORT",
  get_flight_schedule: "GET_FLIGHT_SCHEDULE",
  search_insur_info: "SEARCH_INSUR_INFO",
  submit_claim: "APILWA4PQD",
  hdi_sendmail: "HDI_EMAIL_JSON",
  get_list_docs: "API0SS6MOK",
  billing_request: "APIN2I2D2Y",
}

module.exports = {
  sdk,
  bhsk,
  car,
  common,
  formbuy,
  visa,
  vietjet,
}