const express = require("express");
var bodyParser = require("body-parser");
const cookieParser = require('cookie-parser')
var session = require('express-session')
const MongoConnector = require("./backend/service/MongoConnector");
const app = express();
const server = require("http").Server(app);
const next = require("next");
const routes = require("./backend/router");
const port = parseInt(process.env.PORT, 10) || 6886;
const dev = process.env.NODE_ENV !== "production";

var sess = {
  secret: 'keyboard cat',
  cookie: {}
}

const nextApp = next({ dev });
const nextHandler = nextApp.getRequestHandler();

console.log("process.env.NODE_ENV =  ", process.env.NODE_ENV);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser())
app.use(session(sess))
app.use(routes);
//connect to mongo
MongoConnector()
//Next app init
nextApp.prepare().then(() => {
  app.use(express.static("public"));
  app.use("/_next",express.static(".next"));
  app.get("*", (req, res) => {
    var host = req.headers.host;
    var origin = req.headers.origin;

    return nextHandler(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
