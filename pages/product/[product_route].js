import { useRouter } from "next/router";
import dynamic from 'next/dynamic'
import Layout from "../../components/layout.js";
import { Button } from "react-bootstrap";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Lang from "../../services/Language.js";
import Language from "../../services/Lang.js";
import Utils from '../../services/utils';
import LoadingForm from "../../sdk/src/common/loadingpage";
import globalStore from "../../services/globalStore";
import ErrorPage from "../../components/error_page.js";
const jwt = require('jsonwebtoken');

global.l = new Lang();

const hdg = "WEGQX02PFJ"
const vjg = "W39D2CPV6P"



const Index = (pageProps) => {
  if(pageProps.page_error || pageProps.initPage.page_status == "deactive"){
    return(<ErrorPage/>)
  }

  global.lng = new Language({});
  globalStore.set("pageconfig", pageProps.initPage);
  // globalStore.set("page_version", pageProps.initPage.page_version);
  // global.page_version = pageProps.initPage.page_version;


  // console.log("pageProps.initPage", pageProps.initPage.form_info)


  const pageStatus = pageProps.initPage.page_status;
  const [pageConfig, setPageConfig] = useState(pageProps.initPage);
  const [pageError, setPageError] = useState(pageProps.page_error);

  const [DynamicComponentWithNoSSR, setDynamicComponentWithNoSSR] = useState(false);
  const [preload, setPreload] = useState(true);
  const [prod_define, setProdDefine] = useState(null);
  const [preloadClass, setPreloadClass] = useState("page-loader");
  const [payment, setPayment] = useState(false);
  const [productConfig, setProductConfig] = useState(null);
  const router = useRouter();
  const {product_route, org} = router.query;

  useEffect(() => {
    try {
      if (preload) {
        setPreload(false);
        setTimeout(() => {
          setPreloadClass("page-loader hide");
          setTimeout(() => {
            setPreloadClass("page-loader none");
          }, 500);
        }, 500);
      }
    } catch (e) {
      // setPageState(1)
    }
  });

  useEffect(() => {
      try{

        if(pageConfig.form_info.file_path){
                    var NoSSR = dynamic(
                      () => import('../../sdk/src/sdk_modules/'+pageConfig.form_info.file_path),
                      { ssr: false, loading: () => <div className="center-loading-page"><LoadingForm/></div> }
                    )
                  setDynamicComponentWithNoSSR(NoSSR)
                  setPayment(router.query.payment == "done" ? true : false);
            }else{
              setPageError(true)
            }
      }catch(e){
         setPageError(true)
      }




  }, []);




    return (
    <Layout>
       <Head>
        <title>{pageConfig.meta_data.page_title}</title>
        <meta name="viewport" content="user-scalable=no, width=device-width" />
        <link rel="stylesheet" href="/css/app.css" />
        <meta name="keywords" content={pageConfig.meta_data.page_keywords}/>
        <meta name="description" content={pageConfig.meta_data.page_description}/>
        <meta property="og:title" content={pageConfig.meta_data.page_title}/>
        <meta property="og:description" content={pageConfig.meta_data.page_description}/>
        {pageConfig.meta_data.page_thumbnail&&<meta property="og:image" content={pageConfig.page_thumbnail}/>}
        {pageConfig.meta_data.page_thumbnail&&<meta name="twitter:image" content={pageConfig.page_thumbnail}/>}
      </Head>




      <div className={preloadClass}/>
       {(pageError==false  && DynamicComponentWithNoSSR)?
        <DynamicComponentWithNoSSR
          prod_define={pageConfig.prod_ref}
          payment={payment}
          page_layout={pageConfig.layout_component}
          productConfig={pageConfig.product_config}
        />:<div style={{textAlign:"center", padding: 100}}>
            <h4>Sorry!</h4>
            <p>This page is unavailable. Try again later.</p>
        </div>}

    </Layout>
  );


};


export async function getServerSideProps(ctx){
  var language = "vi"
  try{
    var domainpath = ""
    var domainhost = ""
    var page_error = false
    var prod_ref = null
    var product_config = {}
    var query = {}

    if(ctx.req){
      language = ctx.req.cookies.current_language
      domainpath = ctx.req._parsedOriginalUrl.pathname
      domainhost = ctx.req.headers.host
      query = ctx.req.query
    }else{
      domainpath = ctx.pathname
      domainhost = ctx.headers.host
      query = ctx.query
    }

    var page_component_object = null
    var initPage = await Utils.initPage(domainhost, domainpath, language)

   if(query.ref){
      var decoded = jwt.verify(query.ref, "C5F3AC6BC47D5F69E0530100007F3016", (err, decoded)=>{
         if(decoded){
            prod_ref = decoded
            initPage.prod_ref = prod_ref
            initPage.org = decoded.org
            initPage.product_config.ORG_CODE = decoded.org
         }else{
            page_error = true
         }
      });
    }
    if(query.org){
      initPage.product_config.ORG_CODE = query.org
    }
    if(query.ref_id){
      initPage.prod_ref = {ref_id: query.ref_id}
    }


   
    if(initPage.permalink){
       return { props: { domainpath: domainpath, initPage: initPage, siteurl: domainhost,  page_error: page_error  } }
    }else{
       return { props: { domainpath: null, initPage: {meta_data:{page_title:"Page is error"}}, page_component_object: {}, page_error: true } }
    }


    
   
  }catch(e){
    console.log("page Error ", e)
    return { props: { domainpath: null, initPage: {meta_data:{page_title:"Page is error"}}, page_component_object: {}, page_error: true } }
  }
}

export default Index;
