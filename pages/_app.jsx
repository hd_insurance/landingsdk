import App from "next/app";
import React from "react";
import { CookiesProvider } from "react-cookie";
import https from "https";
import axios from "axios";
import Router from "next/router";
import Language from "../services/Lang.js";
import NProgress from "nprogress";
import globalStore from "../services/globalStore";

import "bootstrap/dist/css/bootstrap.min.css";
import "nprogress/nprogress.css";

import "../components/topbanner/style.css";
// import "../components/appbanner/style.css"
import "../components/cardholderinfo/style.css";
import "../components/horzlayout/style.css";
import "../components/app.css";
import "../components/reponsive.css";
// import "../components/libs/flinput/style.css"
import "../components/needsuport/style.css";
import "../components/products/style.css";
import "../components/qna/style.css";
import "../components/registerform/style.css";
import "../components/upload/style.css";
//pages
import "../landing_modules/module_vj_search_cert/style.css";
import "../landing_modules/module_vj_search_cert/searchform/style.css";

import "../landing_modules/module_visa/style.css";

//bhsk

// import '../components/modules/landing-page-vietjet/css/style.css';
// import '../components/modules/landing-page-vietjet/css/app.css';
// import '../components/modules/landing-page-vietjet/css/main.css';
// import '../components/modules/landing-page-vietjet/css/reponsive.css';
import "../landing_modules/module_bhsk/landing-page-vietjet/css/style.css";
import "../landing_modules/module_bhsk/landing-page-vietjet/css/app.css";
import "../landing_modules/module_bhsk/landing-page-vietjet/css/main.css";
import "../landing_modules/module_bhsk/landing-page-vietjet/css/reponsive.css";

import "../landing_modules/e_claim/style.css";

// import '../components/modules/bhsk/css/style.css';
// import '../components/modules/bhsk/css/app.css';
// import '../components/modules/bhsk/css/main.css';
// import '../components/modules/bhsk/topwizard/style.css';

//indemnify
// import "../components/modules/indemnify/style.css"

//car
// import '../components/modules/car/style.css';

//loading
import "../components/loading/style.css";

// font-icon
import "../public/css/app.css";

import "../public/css/sdk.css";



import '../views/style.module.css'

// import "hdi-uikit/dist/main.css";


// css test landing
import "../components/landingComponent/header/header.css";
import "../components/landingComponent/introducetop/index.css";
import "../components/landingComponent/introduce/index.css";
import "../components/landingComponent/tnds/index.css";
import "../components/landingComponent/form/index.css";
import "../components/landingComponent/contact/index.css";
import "../components/landingComponent/relationProduct/index.css";
import "../components/landingComponent/appintro/index.css";
import "../components/landingComponent/footer/index.css";
import "../components/landingComponent/social/index.css";
import "../components/landingComponent/list_product_promotion/style.css";

// css slick
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import "../sdk/src/css/style.css" // outer css sdk

NProgress.configure({
  minimum: 0.3,
  easing: "ease",
  speed: 800,
  showSpinner: false,
});

class MyApp extends App {
  constructor(props) {
    super(props);
    // global.lng = new Language(props.pageProps.language);
    // globalStore.set("pageconfig", props.pageProps.page_config);
    // globalStore.set("page_version", props.pageProps.page_version);
    // global.page_version = props.pageProps.page_version;
  }

  // static async getInitialProps({ Component, ctx }) {
  //   try {
  //     var domain = "";
  //     const pageProps = Component.getInitialProps
  //       ? await Component.getInitialProps(ctx)
  //       : {};


  //     var page_id = process.env.NEXT_PUBLIC_DEFAULT_WEB_INSTANCE; //default
  //     var version = "1.0.2"; //default
  //     var domainhost = "";
  //     var domainpath = "";
  //     if (pageProps.headers) {
  //       domainhost = pageProps.headers.host;
  //       domainpath = pageProps._parsedOriginalUrl?.pathname;
  //       // console.log(pageProps._parsedOriginalUrl.pathname)
  //     } else if (ctx.req) {
  //       domain = process.env.DM1;
  //       domainhost = ctx.req.headers.host;
  //       domainpath = ctx.req._parsedOriginalUrl?.pathname;
  //       // console.log(ctx.req._parsedOriginalUrl.pathname)
  //     } else {
  //       if (typeof window !== "undefined") {
  //         if (window.location.hostname) {
  //           domainhost = window.location.hostname;
  //         }
  //         domainpath = pageProps.domainpath;
  //       }
  //     }

  //     // console.log("pageProps >>>>>>>>>>>>>> >>>>>>>>>>>>>>>>>>> >>>>>>>>>>>>>. ", pageProps.headers.host, ctx.req)
  //     const instance = axios.create({
  //       httpsAgent: new https.Agent({
  //         rejectUnauthorized: false,
  //       }),
  //     });

  //     const webinstances = await instance.get(`${domain}/api/pages-instance`);

  //     const instance_info = webinstances.data.data.filter(function (page) {
  //       if(domainhost){
  //         return page.DOMAIN == domainhost.toLowerCase().trim();
  //       }
  //     });

  //     if (instance_info[0]) {
  //       console.log("instance_info > ", instance_info[0]);
  //       page_id = instance_info[0].WI_ID;
  //       pageProps.page_org = instance_info[0].ORG_KEY;
  //       pageProps.page_version = instance_info[0].VERSION;
  //       version = instance_info[0].VERSION;
  //     } else {
  //       pageProps.page_org = process.env.NEXT_PUBLIC_DEFAULT_ORG_INSTANCE;
  //       version = process.env.NEXT_PUBLIC_DEFAULT_PAGE_VERSION;
  //       pageProps.page_version = version;
  //     }

  //     const page_config_info = await instance.get(
  //       `${domain}/api/pages-config/${page_id}?version=${version}`
  //     );
  //     pageProps.page_config = page_config_info.data.data;
  //     const data = await instance.get(
  //       `${domain}/api/lang/${page_id}?version=${version}`
  //     );
  //     pageProps.language = data.data.data;

  //     // console.log("pageProps.language", pageProps.language)
  //     global.lng = new Language(pageProps.language);
  //     global.page_version = version;
  //     return { pageProps: pageProps };
  //   } catch (e) {
  //     console.log(e);
  //     return { pageProps: null };
  //   }
  // }

  render() {
    //Page props that were returned  from 'getInitialProps' are stored in the props i.e. pageprops
    const { Component, pageProps } = this.props;
    return <CookiesProvider><Component {...pageProps} /></CookiesProvider>;
  }
}

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

export default MyApp;
