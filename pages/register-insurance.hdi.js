import { useRouter } from "next/router";
import ErrorPage from "../components/error_page.js";
import dynamic from "next/dynamic";
import Layout from "../components/layout.js";
import { Button } from "react-bootstrap";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Lang from "../services/Language.js";
import Language from "../services/Lang.js";
import LoadingForm from "../sdk/src/common/loadingpage";
import config from "../backend/config/constants.js";
import Utils from '../services/utils';
import globalStore from "../services/globalStore";

global.l = new Lang();

const hdg = "WEGQX02PFJ";
const vjg = "W39D2CPV6P";
const DynamicComponentWithNoSSR = dynamic(
   () => import("../landing_modules/module_bhsk/register-insurance"),
   {
      ssr: false,
      loading: () => (
         <div className="center-loading-page">
            <LoadingForm />
         </div>
      ),
   }
);


const getOrgFromURL = (hostname) => {
   if (
      hostname.toLowerCase() == "beta-hdbank.hdinsurance.com.vn" ||
      hostname.toLowerCase() == "suat-hdbank.hdinsurance.com.vn" ||
      hostname.toLowerCase() == "hdbank.hdinsurance.com.vn"
   ) {
      return "HDBANK_VN";
   } else if (
      hostname.toLowerCase() == "beta-hdsaison.hdinsurance.com.vn" ||
      hostname.toLowerCase() == "suat-hdsaison.hdinsurance.com.vn" ||
      hostname.toLowerCase() == "hdsaison.hdinsurance.com.vn"
   ) {
      return "HDSAISON";
   } else if (
      hostname.toLowerCase() == "beta-phulong.hdinsurance.com.vn" ||
      hostname.toLowerCase() == "suat-phulong.hdinsurance.com.vn" ||
      hostname.toLowerCase() == "phulong.hdinsurance.com.vn"
   ) {
      return "PHULONG";
   } else if (
      hostname.toLowerCase() == "beta-vietjetair.hdinsurance.com.vn" ||
      hostname.toLowerCase() == "suat-vietjetair.hdinsurance.com.vn" ||
      hostname.toLowerCase() == "vietjetair.hdinsurance.com.vn"
   ) {
      return "VIETJET_VN";
   } else {
      return "VIETJET_VN";
      // return "PHULONG";
   }
};

const language_json = {
   "lbbl_step_1": [
      "Bước 1",
      "Step 1"
   ],
   "lbbl_step_2": [
      "Bước 2",
      "Step 2"
   ],
   "lbbl_step_3": [
      "Bước 3",
      "Step 3"
   ],
   "lbbl_step_4": [
      "Bước 4",
      "Step 4"
   ],
   "lbbl_title_1": [
      "Thông Tin Nhân Viên",
      "EMPLOYEE INFORMATION"
   ],
   "lbbl_title_2": [
      "Thông Tin Người Được Bảo Hiểm",
      "Information of Insured Persons"
   ],
   "lbbl_title_3": [
      "Xác Nhận",
      "CONFIRM"
   ],
   "lbbl_title_4": [
      "Thành công",
      "Successful"
   ],
   "lbbl_subtitle_1": [
      "Người thân chỉ có thể đăng ký nếu Nhân viên tham gia/ đã tham gia bảo hiểm",
      "Relatives can only register if the Employee participates/ has already participated in insurance"
   ],
   "lbbl_subtitle_2": [
      "Hãy Điền Thông Tin Người Được Bảo Hiểm\n",
      "Please fill Information of Insured Persons"
   ],
   "lbl_staff": [
      "Mã nhân viên",
      "Employee ID"
   ],
   "lbl_title": [
      "Danh xưng",
      "Title"
   ],
   "lbl_fullname": [
      "Họ tên",
      "Full name"
   ],
   "lbl_bank_acc": [
      "Số tài khoản nhận lương",
      "Bank account used for receiving salary"
   ],
   "lbl_phone_number": [
      "Số điện thoại",
      "Phone number"
   ],
   "lbl_passport": [
      "CMND/CCCD/Hộ chiếu",
      "ID/Passport number"
   ],
   "lbl_dob": [
      "Ngày sinh",
      "Date of birth"
   ],
   "street": [
      "Số nhà, ngõ/ngách, đường",
      "House number, Street name"
   ],
   "addd": [
      "Địa chỉ",
      "Address"
   ],
   "address": [
      "Tỉnh/TP-Quận/huyện-Phường/Xã",
      "Province/City - District/Town - Ward/Commune"
   ],
   "lbl_view_benefit": [
      "Xem quyền lợi",
      "View details"
   ],
   "lbl_fee": [
      "Phí",
      "Fee"
   ],
   "lbl_noted_f1": [
      "Tôi đồng ý thanh toán phí bảo hiểm thông qua trừ tài khoản nhận lương tháng kế tiếp.",
      "I agree to pay the insurance premium via my bank account that used for receiving salary in the next month. "
   ],
   "lbl_next": [
      "Tiếp tục",
      "Continue"
   ],
   "lbl_previous": [
      "Quay lại",
      "Previous"
   ],
   "lbl_confirm": [
      "Xác nhận",
      "Confirm"
   ],
   "btn_add_relatives": [
      "Thêm NĐBH",
      "Add Dependent"
   ],
   "btn_add_relatives_question": [
      "Bạn có muốn thêm thông tin của người thân nhận bảo hiểm?",
      "Would you like to buy more for Dependents?"
   ],
   "btn_add_dp": [
      "Thêm người được bảo hiểm",
      "Add Dependent"
   ],
   "lbl_package_title": [
      "Chương trình bảo hiểm",
      "Employee and dependent healthcare"
   ],
   "lbl_total_am": [
      "Tổng tiền dự kiến",
      "Total premium"
   ],
   "lbl_total_am2": [
      "Tổng tiền",
      "Total"
   ],
   "effect_llbl": [
      "Hiệu lực",
      "Effect"
   ],
   "choose_relation_title": [
      "Chọn mối quan hệ với nhân viên",
      "Choose relationship with employee"
   ],
   "choose_relation": [
      "Mối quan hệ",
      "Relation"
   ],
   "insured": [
      "Tôi đã tham gia bảo hiểm",
      "I have been joined insurance"
   ],
   "delete_insured_person": [
      "Xóa NĐBH",
      "Delete Insured person"
   ],
   "lb_title_1": [
      "Bạn mua cho ai",
      "Who are you buying for?"
   ],
   "lb_title_parents": [
      "Bố/mẹ",
      "Parents"
   ],
   "lb_title_father": [
      "Bố",
      "Father"
   ],
   "lb_title_mother": [
      "Mẹ",
      "Mother"
   ],
   "lb_title_children": [
      "Con cái",
      "Children"
   ],
   "lb_title_son": [
      "Con trai",
      "Daughters"
   ],
   "lb_title_legal_spouse": [
      "Vợ/chồng",
      "Legal Spouse"
   ],
   "f3_title1": [
      "Thông tin của những người được bảo hiểm (Bao gồm cả nhân viên)",
      "Information of Insured Persons (including employee)"
   ],
   "f3_title2": [
      "Tổng chi phí bảo hiểm",
      "Total premium"
   ],
   "f3_title3": [
      "Giảm phí",
      "Discount"
   ],
   "f3_title4": [
      "Tổng chi phí bảo hiểm",
      "Total premium after discount"
   ],
   "f3_title5": [
      "Người được bảo hiểm bị mắc một trong các bệnh: tâm thần, ung thư, bệnh phong.",
      "The insured person suffers from: mental illness, cancer, leprosy"
   ],
   "f3_title6": [
      "Người được bảo hiểm bị thương tật vĩnh viễn từ 70% trở lên.",
      "Does any insured person suffer from more than 70% permanent disability?"
   ],
   "f3_title7": [
      "MÃ VOUCHER",
      "VOUCHER CODE"
   ],
   "f3_title8": [
      "Áp dụng",
      "Apply"
   ],
   "f3_title9": [
      "Có",
      "Yes"
   ],
   "f3_title10": [
      "Không",
      "No"
   ],
   "f3_title1x": [
      "Không, tiếp tục",
      "No, continue"
   ],
   "f3_title11": [
      "Tôi cam kết các thông tin khai báo là chính xác, trung thực và hoàn toàn chịu trách nhiệm về các thông tin đã khai báo. Đồng thời tôi đã đọc, hiểu và đồng ý với",
      "I am committed to give authentic, honest information and completely responsible for all the information provided. I have also read, understood and agreed with"
   ],
   "f3_title12": [
      "điều kiện, điều khoản, quy tắc",
      "Conditions, terms and wording"
   ],
   "f3_title13": [
      "của HDI.",
      "of HDI."
   ],
   "f3_title14": [
      "Thông tin người được bảo hiểm",
      "Information of Insured Persons"
   ],
   "label_edit": [
      "Sửa",
      "Edit"
   ],
   "f2_ui_t1": [
      "Lưu ý",
      "Notice"
   ],
   "f2_ui_t2": [
      "NĐBH phải từ 15 ngày tuổi đến 65 tuổi, đối với trẻ em từ 15 ngày tuổi đến 1 tuổi thì áp dụng tăng phí 30%.",
      "Insured Person must be from 15 days old to 65 years old. Loading 30% of premium for infant from 15 days old to 1 year old.."
   ],
   "f2_ui_t3": [
      "Trong trường hợp người thân của Nhân viên chưa có số CMND/CCCD/Hộ chiếu - số điện thoại, vui lòng nhập lại số CMND/CCCD/Hộ chiếu - số điện thoại của Nhân viên đó.",
      "In case Dependent does not have ID/Passport number, phone number yet, please fill in form using the Employee's ID/Passport number, phone number."
   ],
   "popup_q1": [
      "Bạn có muốn thêm người thân được nhận bảo hiểm?",
      "Would you like to buy more for Dependents?"
   ],
   "apply_date_note_q1": [
      "Bảo hiểm có hiệu lực tính từ ngày",
      "Insurance is effective from"
   ],
   "lbll_detail": [
      "Chi tiết",
      "Detail"
   ],
   "year": [
      "năm",
      "year"
   ],
   "f4_btn1": [
      "Khám phá Bảo hiểm HD",
      "Explore HD Insurance"
   ],
   "f4_btn2": [
      "Quay lại trang chủ",
      "Return to the homepage"
   ],
   "f4_label1": [
      "ĐĂNG KÝ MUA THÀNH CÔNG",
      "Successfully Registered"
   ],
   "f4_label2": [
      "CẬP NHẬT THÔNG TIN THÀNH CÔNG",
      "Successfully Updated"
   ],
   "f4_label3": [
      "Bạn vừa thực hiện đăng ký thành công. Phí bảo hiểm sẽ được khấu trừ vào kỳ lương tháng ... /2021 của Quý khách. Sau khi sẽ nhận được email xác nhận từ hệ thống, vui lòng kiểm tra thông tin đăng ký tại email và xác nhận. Xin cám ơn!",
      "You have successfully registered. Premiums will be deducted on your monthly salary period... /2021. After receiving the confirmation email from the system, please check the registration information at the email and confirm. Thank you!"
   ],
   "xr8w782g": [
      "Nhân viên có nhu cầu mua bảo hiểm cho bản thân",
      "Employees want to buy insurance for themselves"
   ],
   "lbl_email": [
      "Email",
      "Email"
   ],
   "lbl_pack_issue": [
      "Gói bảo hiểm",
      "Insurance package"
   ],
   "tbt_tit_step3": [
      "Thông tin của người được bảo hiểm",
      "Information of Insured Persons"
   ],
   "avqtlkta": [
      "Công ty TNHH Bảo hiểm HD (HDI) chân thành cảm ơn Quý khách đã tín nhiệm và sử dụng sản phẩm bảo hiểm của Công ty chúng tôi. Chi tiết đơn bảo hiểm sẽ được gửi đến email của bạn trong giây lát. Xin cám ơn",
      "HD Insurance thank you for your trust in us and for choosing us as your Insurer. Details of Insurance benefits will be sent to your email shortly. Thank you."
   ],
   "0hwucuq2": [
      "Ngày hiệu lực ",
      "Effective date\n"
   ],
   "insured_person": [
      "NGƯỜI ĐƯỢC BẢO HIỂM",
      "Insured Person"
   ],
   "jdxs2lbr": [
      "Thời hạn bảo hiểm",
      "Period of Insurance"
   ],
   "uf7rwxgt": [
      "Thay đổi",
      "Thay đổi"
   ],
   "saldus90": [
      "Người không được bảo hiểm",
      "Người không được bảo hiểm"
   ],
   "dgr5j4xy": [
      "Chọn người không bị bệnh trong danh sách loại trừ, người không điều trị bệnh hoặc thương tật",
      "Chọn người không bị bệnh trong danh sách loại trừ, người không điều trị bệnh hoặc thương tật"
   ],
   "qpzvx6bd": [
      "Những người trong danh sách này sẽ không được tính bảo hiểm",
      "Những người trong danh sách này sẽ không được tính bảo hiểm"
   ],
   "jo2qhj0g": [
      "Phí bảo hiểm",
      "Premium"
   ],
   "sublabel_ex_cond1": [
      "Chọn một người đang trong thời gian điều trị bệnh hoặc điều trị thương tật?",
      "Choose any person who is during treatment for illness or injury?"
   ],
   "ek3jdgna": [
      "Chọn người bị tâm thần, thần kinh, bệnh phong thương tật vĩnh viễn quá 70%. Người đang điều trị bệnh hoặc thương tật",
      "Choose any person suffer from mental illness, leprosy, more than 70% permanent disability?"
   ],
   "confirm_delete": [
      "Xác nhận",
      "Confirm"
   ],
   "86hcacg0": [
      "Bạn có chắc chắn muốn xóa người được bảo hiểm này không?",
      "Are you sure you want to remove this insured?"
   ],
   "acudsxd7": [
      "Không xóa",
      "No"
   ],
   "j1a6fpbl": [
      "Xóa NĐBH",
      "Yes"
   ],
   "mnn76qtj": [
      "Thanh toán",
      "Pay"
   ],
   "3ifaratu": [
      "Người được bảo hiểm phải từ 15 ngày tuổi đến 65 tuổi. Trường hợp trẻ em từ 15 ngày tuổi đến 1 tuổi thì áp dụng phí 3,878,000 VNĐ.\n",
      "The insured must be between 15 days old and 65 years old. In case of children from 15 days old to 1 year old, the fee will be increased by 30%.\n"
   ]
}
const Index = (pageProps) => {


   global.lng = new Language(language_json);
   globalStore.set("pageconfig", pageProps.initPage.page_config);
   globalStore.set("page_version", pageProps.initPage.page_version);
   global.page_version = pageProps.initPage.page_version;

   // const page_init_config = Utils.getPageConfig(pageProps.domainpath, pageProps.initPage.page_config);

   const pageStatus = pageProps.initPage.page_status;

   if (pageProps.page_error || pageStatus == "deactive") {
      return (<ErrorPage title={"Chương trình đã kết thúc"} message={"Thời hạn đăng ký chương trình bảo hiểm này đã kết thúc. Hãy quay lại khi bảo hiểm này được mở bán trở lại. Xin cám ơn !"} />)
   }


   const [pageConfig, setPageConfig] = useState(pageProps.initPage);



   const router = useRouter();
   const [preload, setPreload] = useState(true);
   const [preloadClass, setPreloadClass] = useState("page-loader");
   const [org, setOrg] = useState(pageProps.initPage.org_code_default);
   const [payment, setPayment] = useState(false);
   const [pack_code, setPack_code] = useState("");
   const [productConfig, setProductConfig] = useState(null);
   const product_route = router.pathname.substring(1);
   useEffect(() => {
      try {
         if (preload) {
            setPreload(false);
            setTimeout(() => {
               setPreloadClass("page-loader hide");
               setTimeout(() => {
                  setPreloadClass("page-loader none");
               }, 500);
            }, 500);
         }
      } catch (e) {
         // setPageState(1)
      }
   });

   useEffect(() => {
      setPayment(router.query.payment == "done" ? true : false);
      if (router?.query?.pack_code) {
         setPack_code(router?.query?.pack_code);
      }

      if (config.products[product_route.toLowerCase()]) {
         const prod_config = config.products[product_route.toLowerCase()];
         setProductConfig(prod_config);
      }
      // console.log("pathname", router.pathname.substring(1));
   }, []);

   useEffect(() => {
      try {
         // const host = getOrgFromURL(pageProps.siteurl);
         // setOrg(host);
      } catch (e) {
         console.log(e);
      }
   }, [pageProps.siteurl]);

   return (
      <Layout>

         <Head>
            <title>{pageConfig.meta_data.page_title}</title>
            <meta name="viewport" content="user-scalable=no, width=device-width" />
            <link rel="stylesheet" href="/css/app.css" />
            <meta name="keywords" content={pageConfig.meta_data.page_keywords} />
            <meta name="description" content={pageConfig.meta_data.page_description} />
            <meta property="og:title" content={pageConfig.meta_data.page_title} />
            <meta property="og:description" content={pageConfig.meta_data.page_description} />
            {pageConfig.meta_data.page_thumbnail && <meta property="og:image" content={pageConfig.page_thumbnail} />}
            {pageConfig.meta_data.page_thumbnail && <meta name="twitter:image" content={pageConfig.page_thumbnail} />}

            <script
               async
               src={`https://www.googletagmanager.com/gtag/js?id=G-${org == "vietjet_vn" ? vjg : hdg
                  }`}
            ></script>
            <script
               async
               dangerouslySetInnerHTML={{
                  __html: `window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
            
              gtag('config', 'G-${org == "vietjet_vn" ? vjg : hdg}');`,
               }}
            />


         </Head>



         <div className={preloadClass} />
         <DynamicComponentWithNoSSR
            org={org}
            payment={payment}
            router={router}
            pack_code={pack_code}
            productConfig={productConfig}
         />
      </Layout>
   );
};

export async function getServerSideProps(ctx) {
   try {
      var domainpath = ""
      var domainhost = ""
      var page_error = false
      var prod_ref = null
      var product_config = {}
      var query = {}

      if (ctx.req) {
         domainpath = ctx.req._parsedOriginalUrl.pathname
         domainhost = ctx.req.headers.host
         query = ctx.req.query
      } else {
         domainpath = ctx.pathname
         domainhost = ctx.headers.host
         query = ctx.query
      }

      var page_component_object = null
      var initPage = await Utils.initPage(domainhost, domainpath)

      if (query.ref) {
         var decoded = jwt.verify(query.ref, "C5F3AC6BC47D5F69E0530100007F3016", (err, decoded) => {
            if (decoded) {
               prod_ref = decoded
               initPage.prod_ref
               initPage.org = decoded.org
               initPage.product_config.ORG_CODE = decoded.org

            } else {
               page_error = true
            }
         });
      }
      if (query.org) {
         initPage.product_config.ORG_CODE = query.org
      }

      if (initPage.permalink) {
         return { props: { domainpath: domainpath, initPage: initPage, siteurl: domainhost, page_error: page_error } }
      } else {
         return { props: { domainpath: null, initPage: { meta_data: { page_title: "Page is error" } }, page_component_object: {}, page_error: true } }
      }




   } catch (e) {
      console.log("page Error ", e)
      return { props: { domainpath: null, initPage: { meta_data: { page_title: "Page is error" } }, page_component_object: {}, page_error: true } }
   }
}



export default Index;
