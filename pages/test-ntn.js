import router from "next/router";
import dynamic from 'next/dynamic'
import Layout from "../components/layout.js";
import Main from "../sdk/src/sdk_modules/private_house";
import { Button } from "react-bootstrap";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Language from "../services/Language.js";
global.l = new Language();

const hdg = "WEGQX02PFJ";
const vjg = "W39D2CPV6P";

const DynamicComponentWithNoSSR = dynamic(
  () => import('../sdk/src/sdk_modules/private_house'),
  { ssr: false }
)


const Index = (pageProps) => {
  
  
  const [preload, setPreload] = useState(true);
  const [preloadClass, setPreloadClass] = useState("page-loader");

  useEffect(() => {
    try {
      if (preload) {
        setPreload(false);
        setTimeout(() => {
          setPreloadClass("page-loader hide");
          setTimeout(() => {
            setPreloadClass("page-loader none");
          }, 500);
        }, 500);
      }
    } catch (e) {
      // setPageState(1)
    }
  });

  useEffect(() => {
    try {
    } catch (e) {
      console.log(e);
    }
  }, [pageProps.siteurl]);

  return (
    <Layout>
      <Head>
        <title>HDI - SDK</title>
        <meta name="viewport" content="user-scalable=no, width=device-width" />
        <link rel="stylesheet" href="/css/app.css" />
        <meta
          name="keywords"
          content="Tra cứu giấy chứng nhận, Tra cứu bảo hiểm HDI, Tra cứu bảo hiểm Vietjet, Bảo hiểm an tâm về nhà Vietjet"
        />
        <meta
          name="description"
          content="Tra cứu thông tin giấy chứng nhận bảo hiểm Về nhà an tâm của HDI, Tra cứu bảo hiểm Về nhà An Tâm Vietjet"
        />
      </Head>
      <div className={preloadClass} />
      <DynamicComponentWithNoSSR  />
    </Layout>
  );
};

export async function getServerSideProps(ctx){
    var domainpath = ""
     var domainhost = ""
    if(ctx.req){
      domainpath = ctx.req._parsedOriginalUrl.pathname
      domainhost = ctx.req.headers.host
    }else{
      domainpath = ctx.pathname
      domainhost = ctx.headers.host
    }
    return { props: { domainpath: domainpath } }
  
}

export default Index;
