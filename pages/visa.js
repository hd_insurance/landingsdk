import router from "next/router";
import dynamic from 'next/dynamic'
import Layout from "../components/layout.js";
import { Button } from "react-bootstrap";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Language from "../services/Language.js";
import LoadingForm from "../sdk/src/common/loadingpage";
import globalStore from "../services/globalStore";
// import '../components/modules/landing-page-vietjet/css/style.css';
// import '../components/modules/landing-page-vietjet/css/app.css';
// import '../components/modules/landing-page-vietjet/css/main.css';
// import '../components/modules/landing-page-vietjet/css/reponsive.css';


global.l = new Language();

const hdg = "WEGQX02PFJ"
const vjg = "W39D2CPV6P"

const MainVisa = dynamic(
  () => import('../landing_modules/module_visa'),
  { ssr: false, loading: () => <div className="center-loading-page"><LoadingForm/></div> }
)


const getOrgFromURL = (hostname)=>{
 
    if((hostname.toLowerCase() == "beta-hdbank.hdinsurance.com.vn") || (hostname.toLowerCase() == "suat-hdbank.hdinsurance.com.vn") || (hostname.toLowerCase() == "hdbank.hdinsurance.com.vn")){
      return "hdbank_vn"
    }else if((hostname.toLowerCase() == "beta-hdsaison.hdinsurance.com.vn") || (hostname.toLowerCase() == "suat-hdsaison.hdinsurance.com.vn") || (hostname.toLowerCase() == "hdsaison.hdinsurance.com.vn")){
      return "hdsaison"
    }else{
      return "vietjet_vn"
    }
  }

const Index = (pageProps) => {

  const [preload, setPreload] = useState(true);
  const [preloadClass, setPreloadClass] = useState("page-loader");
  const [org, setOrg] = useState(getOrgFromURL(pageProps.siteurl))

  
  useEffect(() => {
    try {
      if(preload){
          setPreload(false)
          setTimeout(() => {
              setPreloadClass("page-loader hide")
              setTimeout(() => {
                setPreloadClass("page-loader none")
              }, 500);
          }, 500);
      }

    } catch (e) {
      // setPageState(1)
    }
  });

  useEffect(() => {
    try {
      const host = getOrgFromURL(pageProps.siteurl)
      if(host!=org){
        console.log("HOST: ", host)
        setOrg(host)
      }
    } catch (e) {
      console.log(e)
    }
  }, [pageProps.siteurl]);

  return (
    <Layout>
      <Head>
        <title>VISA Doanh Nghiep</title>
        <meta name="viewport" content="user-scalable=no, width=device-width" />
        <link rel="stylesheet" href="/css/app.css"/>
      </Head>
      <div className={preloadClass}/>
      <MainVisa/>
    
    </Layout>
  );
};


export async function getServerSideProps(ctx){
    var domainpath = ""
     var domainhost = ""
    if(ctx.req){
      domainpath = ctx.req._parsedOriginalUrl.pathname
      domainhost = ctx.req.headers.host
    }else{
      domainpath = ctx.pathname
      domainhost = ctx.headers.host
    }
    return { props: { domainpath: domainpath } }
  
}


export default Index;
