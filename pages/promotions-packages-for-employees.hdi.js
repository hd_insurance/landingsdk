import router from "next/router";
import dynamic from 'next/dynamic'
import Layout from "../components/layout.js";
import ErrorPage from "../components/error_page.js";


import { Button } from "react-bootstrap";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Lang from "../services/Language.js";
import Language from "../services/Lang.js";
import globalStore from "../services/globalStore";
import Utils from '../services/utils';


global.l = new Lang();

const hdg = "WEGQX02PFJ"
const vjg = "W39D2CPV6P"
//tam thoi dong Landing Suckhoe VJ
// const MainVietjet = dynamic(
//   () => import('../landing_modules/module_bhsk/landing-page-vietjet/Main'),
//   { ssr: false }
// )
const MainHDBank = dynamic(
  () => import('../landing_modules/module_bhsk/landing-page-hdbank/Main'),
  { ssr: false }
)
const MainHDSaison = dynamic(
  () => import('../landing_modules/module_bhsk/landing-page-hdsaison/Main'),
  { ssr: false }
)
const MainPhuLong = dynamic(
  () => import('../landing_modules/module_bhsk/landing-page-phulong/Main'),
  { ssr: false }
)

const Index = (pageProps) => {


  global.lng = new Language(pageProps.initPage.language);
  globalStore.set("pageconfig", pageProps.initPage.page_config);
  globalStore.set("page_version", pageProps.initPage.page_version);
  global.page_version = pageProps.initPage.page_version;
  // const page_init_config = Utils.getPageConfig(pageProps.domainpath, pageProps.initPage.page_config);
  // const pageStatus = page_init_config.STATUS;
  // const [pageConfig, setPageConfig] = useState(page_init_config);

  const pageStatus = pageProps.initPage.page_status;

  if(pageProps.page_error || pageStatus == "deactive"){
    return(<ErrorPage title={"Chương trình đã kết thúc"} message={"Thời hạn đăng ký chương trình bảo hiểm này đã kết thúc. Hãy quay lại khi bảo hiểm này được mở bán trở lại. Xin cám ơn !"}/>)
  }

  const [pageConfig, setPageConfig] = useState(pageProps.initPage);
  const [pageError, setPageError] = useState(pageProps.page_error);



  
  const [preload, setPreload] = useState(true);
  const [preloadClass, setPreloadClass] = useState("page-loader");
  const [org, setOrg] = useState(pageProps.initPage.org_code_default.toLowerCase());


  
  // const [org, setOrg] = useState('vietjet_vn');
  
  useEffect(() => {
    try {
      if(preload){
          setPreload(false)
          setTimeout(() => {
              setPreloadClass("page-loader hide")
              setTimeout(() => {
                setPreloadClass("page-loader none")
              }, 500);
          }, 500);
      }

    } catch (e) {
      // setPageState(1)
    }
  });

  return (
    <Layout org={org}>
      <Head>
        <title>{pageConfig.meta_data.page_title}</title>
        <meta name="viewport" content="user-scalable=no, width=device-width" />
        <link rel="stylesheet" href="/css/app.css" />
        <meta name="keywords" content={pageConfig.meta_data.page_keywords}/>
        <meta name="description" content={pageConfig.meta_data.page_description}/>
        <meta property="og:title" content={pageConfig.meta_data.page_title}/>
        <meta property="og:description" content={pageConfig.meta_data.page_description}/>
        {pageConfig.meta_data.page_thumbnail&&<meta property="og:image" content={pageConfig.page_thumbnail}/>}
        {pageConfig.meta_data.page_thumbnail&&<meta name="twitter:image" content={pageConfig.page_thumbnail}/>}
      </Head>
      
      <div className={preloadClass}/>
      {/* <MainVietjet/> */}
     {org == "hdbank_vn" && <MainHDBank/>}
     {org == "vietjet_vn" && <MainVietjet/>}
     {org == "hdsaison" && <MainHDSaison/>}
     {org == "phulong" && <MainPhuLong/>}
    </Layout>
  );
};

export async function getServerSideProps(ctx){
  try{
    var domainpath = ""
    var domainhost = ""
    var page_error = false
    var product_config = {}
    var query = {}

    if(ctx.req){
      domainpath = ctx.req._parsedOriginalUrl.pathname
      domainhost = ctx.req.headers.host
      query = ctx.req.query
    }else{
      domainpath = ctx.pathname
      domainhost = ctx.headers.host
      query = ctx.query
    }

    var page_component_object = null
    var initPage = await Utils.initPage(domainhost, domainpath)

    console.log("pageProps.initPage ", initPage.org_code_default)
    if(initPage.permalink){
       return { props: { domainpath: domainpath, initPage: initPage, siteurl: domainhost,  page_error: page_error  } }
    }else{
       return { props: { domainpath: null, initPage: {meta_data:{page_title:"Page is error"}}, page_component_object: {}, page_error: true } }
    }
   
  }catch(e){
    console.log("page Error ", e)
    return { props: { domainpath: null, initPage: {meta_data:{page_title:"Page is error"}}, page_component_object: {}, page_error: true } }
  }
}



export default Index;
