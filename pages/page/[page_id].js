import React, {useState, useEffect, createRef} from 'react';
import dynamic from 'next/dynamic'
import { useRouter } from "next/router";
import Head from "next/head";
import { useCookies } from "react-cookie";
import Lang from "../../services/Language.js";
import Language from "../../services/Lang.js";
import MainLanding from "../../landing_modules/product_landing/tnds.js";
import Utils from '../../services/utils';
import globalStore from "../../services/globalStore";
import ErrorPage from "../../components/error_page.js";
import moment from "moment";
import { PageProvider } from '../../context/page_ctx'
global.l = new Lang();


const LandingPage = (pageProps) => {

  if(pageProps.page_error || pageProps.initPage.page_status == "deactive"){
    return(<ErrorPage />)
  }

  global.lng = new Language({});
  globalStore.set("pageconfig", pageProps.initPage);

  globalStore.set("page_version", pageProps.initPage.page_version);
  global.page_version = pageProps.initPage.page_version;

  const pageStatus = pageProps.initPage.page_status;



  const [pageConfig, setPageConfig] = useState(pageProps.initPage);
  const [pageError, setPageError] = useState(pageProps.page_error);
  const [cookies, setCookie] = useCookies(["hdi_tracking"]);
  const router = useRouter();
  const {page_id, tracking} = router.query;

  const FormBhRef = createRef();


  const handleScrollForm = () => {
    FormBhRef.current.scrollIntoView({behavior: 'smooth'});
  }

  try{
    var NoSSR = dynamic(
        () => import('../../landing_modules/product_landing/tnds.js'),
        { ssr: false, loading: () => <div className="pageLoadFade fadeOut"></div> })
  }catch(e){
    console.log("e ", e)
  }

  useEffect(() => {
    handleCookie()
  });
  const handleCookie = ()=>{
    if(tracking){
      console.log("set cookie", tracking, router)
      // let pathName = router.asPath.split('?')[0];
      let expires = new Date()
      expires.setTime(expires.getTime() + (5 * 60 * 1000));
      setCookie("hdi_tracking", tracking, {path: '/', expires});
    }
  }


  return (
    <>
       <Head>
        <title>{pageConfig.meta_data.page_title}</title>
        <meta name="viewport" content="user-scalable=no, width=device-width" />
        <link rel="stylesheet" href="/css/app.css" />
        <meta name="keywords" content={pageConfig.meta_data.page_keywords}/>
        <meta name="description" content={pageConfig.meta_data.page_description}/>
        <meta property="og:title" content={pageConfig.meta_data.page_title}/>
        <meta property="og:description" content={pageConfig.meta_data.page_description}/>
        {pageConfig.meta_data.page_thumbnail&&<meta property="og:image" content={pageConfig.page_thumbnail}/>}
        {pageConfig.meta_data.page_thumbnail&&<meta name="twitter:image" content={pageConfig.page_thumbnail}/>}
      </Head>
        <div className="fadeLoading">
           {pageError?<div style={{textAlign:"center", padding: 100}}>
            <h4>Sorry!</h4>
            <p>This page is unavailable. Try again later.</p>
        </div>:<PageProvider value={pageConfig.page_config}>
                 <NoSSR
                  page_layout={pageConfig.layout_component}
                  product_config={pageConfig.page_config}
                 />
              </PageProvider>}
        </div>

    </>
  );
}

export async function getServerSideProps(ctx){

  var language = "vi"
  try{
    var domainpath = ""
    var domainhost = ""
    var page_error = false
    var product_config = {}
    var query = {}

    if(ctx.req){
      language = ctx.req.cookies.current_language
      domainpath = ctx.req._parsedOriginalUrl.pathname
      domainhost = ctx.req.headers.host
      query = ctx.req.query
    }else{
      domainpath = ctx.pathname
      domainhost = ctx.headers.host
      query = ctx.query
    }

    var page_component_object = null
    var initPage = await Utils.initPage(domainhost, domainpath, language)

    // console.log(initPage)

    var initTrackingCode = Utils.parseTrackingCode()
    if(query.sku){
      initPage.page_config.sku = query.sku
    }
    if(initPage.permalink){
       return { props: { domainpath: domainpath, initPage: initPage, siteurl: domainhost,  page_error: page_error  } }
    }else{
       return { props: { domainpath: null, initPage: {meta_data:{page_title:"Page is error"}}, page_component_object: {}, page_error: true } }
    }
   
  }catch(e){
    console.log("page Error ", e)
    return { props: { domainpath: null, initPage: {meta_data:{page_title:"Page is error"}}, page_component_object: {}, page_error: true } }
  }
}


export default LandingPage;
