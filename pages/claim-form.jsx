import { useRouter } from "next/router";
import dynamic from 'next/dynamic'
import Layout from "../components/layout.js";
import { Button } from "react-bootstrap";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Language from "../services/Lang.js";
import Lang from "../services/Language.js";
import globalStore from "../services/globalStore";
import Utils from "../services/utils";
import LoadingForm from "../sdk/src/common/loadingpage";

global.l = new Lang();

const hdg = "WEGQX02PFJ";
const vjg = "W39D2CPV6P";
const DynamicComponentWithNoSSR = dynamic(
  () => import('../landing_modules/module_vj_search_cert/claim_form'),
  { ssr: false, loading: () => <div className="center-loading-page"><LoadingForm/></div> }
)


const json_langage = {
   "123231":[
      "ahihiaaac",
      "ahihi"
   ],
   "nloeljgv":[
      "Tìm kiếm, tra cứu thông tin hợp đồng của Bảo hiểm HD hợp tác với Vietjet Air",
      "Tìm kiếm, tra cứu thông tin hợp đồng của Bảo hiểm HD hợp tác với Vietjet Air"
   ],
   "rducp5x2":[
      "Tra cứu",
      "Search"
   ],
   "up8byhyx":[
      "Bảo hiểm Bay an toàn",
      "Fly Safe Insurance"
   ],
   "5rt9iig8":[
      "Ngày đi",
      "Flight start date"
   ],
   "tifiosjq":[
      "Quyền lợi bảo hiểm",
      "Insurance Benefits"
   ],
   "cq1qk8f7":[
      "Bồi thường",
      "Claim"
   ],
   "vietjet_bat_qna_1":[
      "1. Chương trình Bảo hiểm Bay An Toàn là gì?",
      "1. What is Fly Safe insurance program?"
   ],
   "ac091kfo":[
      "sdsds",
      "xxxxsssxsds1"
   ],
   "vietjet_bat_qna1":[
      "1. Chương trình Bảo hiểm Bay An Toàn là gì?",
      "1. Chương trình Bảo hiểm Bay An Toàn là gì?"
   ],
   "uebp29ka":[
      "10. Thời gian để tôi có thể thông báo Sự kiện Bảo hiểm là bao lâu?",
      "10. What is the valid period for me to notifying the Insurance Event?"
   ],
   "deyolsek":[
      "Tên",
      "Middle and Given name"
   ],
   "n5tcm1pu":[
      "Mã đặt chỗ",
      "Reservation code"
   ],
   "lkkie8tq":[
      "Họ",
      "Last name"
   ],
   "vkyp9k4i":[
      "a",
      "a"
   ],
   "baahktzr":[
      "13. Tôi có thể yêu cầu bồi thường bằng cách nào?",
      "13. How can I make a claim request?"
   ],
   "mcu75thn":[
      "11. Thời gian để tôi có thể nộp yêu cầu bồi thường là bao lâu?",
      "11. What is the valid period for me to make a claim?"
   ],
   "muo2hw93":[
      "2. Tôi có phải trả tiền cho chương trình Bảo hiểm Bay An Toàn không?",
      "2. Do I have to pay for Fly Safe insurance program?"
   ],
   "s8jfn4e2":[
      "sd",
      "sds"
   ],
   "hpl3uf7q":[
      "22. Đối với quyền lợi A2 “Chi phí y tế thực tế phát sinh do Tai nạn” thì phải bổ sung giấy tờ gì?",
      "22. What do I need to provide to receive the benefit A2 “Actual Medical Expenses due to Accident(s)”?"
   ],
   "xx9bbfly":[
      "lol",
      "lol"
   ],
   "vfdoxpj6":[
      "1. Đối tượng tham gia bảo hiểm",
      "1. Egilibility"
   ],
   "dmwiqywq":[
      "20. Đối với quyền lợi B “Trợ cấp trong thời gian cách ly”, tôi phải bổ sung thông tin, giấy tờ gì?",
      "20. What do I need to provide to receive the benefit B “Allowance during quarantine”?"
   ],
   "e5ssce4i":[
      "1.Thông tin Quyền lợi bảo hiểm & Số tiền bảo hiểm",
      "1. Insurance Benefits and Sum insured"
   ],
   "uyituyq0":[
      "Thông tin Về tổn thất",
      "Particulars of loss / Occurance"
   ],
   "24sdbgn6":[
      "Chuyển khoản",
      "Bank transfer"
   ],
   "fkqy45yc":[
      "Giấy phép lái xe ",
      "Driver license"
   ],
   "1qaxvmma":[
      "14. Tôi có thể yêu cầu bồi thường nhiều quyền lợi Bảo hiểm cùng lúc nếu các Sự kiện Bảo hiểm đồng thời xảy ra?",
      "14. Can I make a claim for multiple benefits at the same time if insurance events occur?"
   ],
   "b52jzc7h":[
      "asdsd",
      "sdsdds"
   ],
   "urckmnxj":[
      "<p>Đây là chương trình Bảo hiểm được VietJet thiết kế riêng dành tặng với các quyền lợi hấp dẫn:</p>\n<p>- Quý khách được tặng hoàn toàn 100% phí Bảo hiểm.</p>\n<p>- Áp dụng cho tất cả hành khách sử dụng vé máy bay của VietJet, không phân biệt quốc tịch và loại giá vé.</p>\n",
      "<p>This insurance program is specifically designed by VietJet with attractive benefits:</p>\n<p>-\tPassengers get 100% free insurance premium.</p>\n<p>-\tApplicable to all passengers using VietJet's ticket, regardless of nationality and any types of ticket.</p>\n"
   ],
   "62w9yc1c":[
      "1. Giấy tờ chung",
      "1. General documents"
   ],
   "pso6fxqw":[
      "Số tiền yêu cầu bồi thường",
      "Claim Amount Required"
   ],
   "cucfyscu":[
      "Tử vong",
      "Death"
   ],
   "nd99ufs6":[
      "Bản sao Toa thuốc, kết luận y khoa, giấy chứng thương, kết quả xét nghiệm",
      "Prescription for medicine, treatment note, injury note, test results"
   ],
   "sdsddsd":[
      "sdsdsd",
      "asdasd"
   ],
   "vrtxvqva":[
      "<p>&nbsp;Không. Đây là chương trình Bảo hiểm của VietJet mua và tặng cho Quý khách.&nbsp;</p>\n",
      "<p>No. This is an insurance program purchased by Vietjet and offered to all passengers.</p>\n"
   ],
   "zlxsk2ka":[
      "Xem giấy chứng nhận",
      "View Insurance Certificate"
   ],
   "ricskak2":[
      "TRA CỨU CHỨNG NHẬN BẢO HIỂM",
      "SEARCH FOR CERTIFICATE OF INSURANCE"
   ],
   "ssnxjafk":[
      "5. Khách hàng nào không được VietJet tặng chương trình Bảo hiểm Bay An Toàn?",
      "5. Which customers are not offered Fly Safe insurance by VietJet?"
   ],
   "6uu5us0a":[
      "<p>Quyền lợi Bảo hiểm của Quý khách được hưởng theo các Sự kiện Bảo hiểm như sau:</p>\n<p>- Quyền lợi A1. Tử vong hoặc thương tật toàn bộ vĩnh viễn do Tai nạn</p>\n<p>- Quyền lợi A2. Chi phí y tế thực tế phát sinh do Tai nạn</p>\n<p>- Quyền lợi B.    Mất giảm chi phí sinh hoạt, mất giảm thu nhập do bị cách ly bởi dịch bệnh (tối đa 21 (hai mươi mốt) ngày).</p>\n<p>Chi tiết về quyền lợi và điều khoản điều kiện, vui lòng truy cập thêm tại trang thông tin điện tử (website): <a href=\"https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi\" target=\"_self\">https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi</a> , phần \"Thông tin Bảo hiểm Bay An Toàn - Quyền lợi Bảo hiểm\".</p>\n<p></p>\n",
      "<p>Passengers’ insurance benefits are in accordance with the following insured events:&nbsp;</p>\n<p>-\tBenefit A1. Death or Total Permanent Disablement due to Accident(s)</p>\n<p>-\tBenefit A2. Actual Medical Expenses due to Accident(s)</p>\n<p>-\tBenefit B.   Allowance due to loss and reduction of living costs, loss and reduction of income during quarantine (up to 21 (twenty one) days)</p>\n<p>For more details about benefits and term &amp; conditions, please visit  <a href=\"https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi\" target=\"_self\">https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi</a>, section \"Bay An Toan Insurance Information - Insurance Benefits\"</p>\n"
   ],
   "ddakv8ny":[
      "2. Đối với Quyền lợi A1",
      "2. For Benefit A1"
   ],
   "yooywyv9":[
      "Chọn hình thức nhận tiền",
      "Way of receiving money"
   ],
   "e9bgenql":[
      "Biên bản tai nạn",
      "Accident Report"
   ],
   "ej68sgd9":[
      "THÔNG TIN BẢO HIỂM BAY AN TOÀN",
      "FLY SAFE INSURANCE INFORMATION"
   ],
   "xu52wvk3":[
      "sdsdsddsdsds",
      "asddsad"
   ],
   "asdsds":[
      "asdsad",
      "asdsadd"
   ],
   "kpu8a2vk":[
      "<p>Bay An Toàn là Chương trình Bảo hiểm của VietJet dành tặng cho Tất cả hành khách khi sử dụng dịch vụ vận chuyển đường hàng không của VietJet.</p>\n",
      "<p>Fly Safe is an insurance program offered by VietJet to All passengers when buying, using any tickets on all domestic flights operated by VietJet.</p>\n"
   ],
   "ovmyi2x3":[
      "1. Quy tắc bảo hiểm",
      "1. Policy wording"
   ],
   "uqrnye90":[
      "Phải cách ly y tế tập trung do dịch bệnh",
      "Medical quarantine"
   ],
   "7rhbersj":[
      "Hậu quả tai nạn",
      "Accidental aftermath"
   ],
   "twvp6cwq":[
      "Bản chính Hóa đơn/Phiếu thu chi phí y tế, thuốc điều trị",
      "Original of hospital fee invoice, receipts,"
   ],
   "90nhfruf":[
      "Số ghế",
      "Seat No."
   ],
   "2xggpppb":[
      "23. Nếu tôi ở nước ngoài (không cư trú ở Việt Nam) thì tôi có được tham gia chương trình Bay An toàn? Làm cách nào để tôi làm thủ tục Bảo hiểm?",
      "23. If I am in a foreign country (not in Viet Nam), may I join Bay An Toan insurance program? How may I make a claim?"
   ],
   "oajggedf":[
      "Các câu hỏi thường gặp",
      "FAQs"
   ],
   "6awkcgcy":[
      "Địa chỉ mô tả",
      "Address of quarantined place"
   ],
   "lxvrw2sq":[
      "Trích lục khai tử (TH tử vong)",
      "Death Certificate (in case the Insured dies)"
   ],
   "error_claim_msg":[
      "Có lỗi xảy ra, vui lòng thử lại sau",
      "An error occurred, please try again later"
   ],
   "lfdn8sug":[
      "Các câu hỏi thường gặp",
      "FAQs"
   ],
   "tn3hhxnm":[
      "4. Bảo hiểm Bay An toàn có gì nổi bật?",
      "4. What is special about Fly Safe insurance program?"
   ],
   "hassimkx":[
      "15. Nếu tôi không thực hiện Chuyến bay, tôi có được hưởng quyền lợi Bảo hiểm hay không?",
      "15. If I don’t fly with VietJet, may I receive insurance benefits?"
   ],
   "lbdbosdb":[
      "Có xảy ra tai nạn",
      "Information of loss"
   ],
   "jjcvtauo":[
      "Nguyên nhân",
      "Reasons of medical quarantine"
   ],
   "w6xywwe2":[
      "Bản sao Giấy ra viện",
      "Copy of Hospital discharge paper"
   ],
   "tceokqgh":[
      "Số hiệu chuyến bay",
      "Flight No."
   ],
   "13yrcfjg":[
      "21. Đối với quyền lợi A1 “Tử vong hoặc thương tật toàn bộ vĩnh viễn do Tai nạn”, người thừa kế phải bổ sung giấy tờ gì?",
      "21. What do the Insured or the Heir need to provide to receive the benefit A1 “Death or Total Permanent Disablement due to Accident(s)”?"
   ],
   "portal_guidance":[
      "Hướng dẫn tra cứu thông tin",
      "Portal guide"
   ],
   "4dx6lort":[
      "8. Làm sao để tôi xác định được thời gian tôi được hưởng các quyền lợi Bảo hiểm theo chương trình Bay An Toàn?",
      "8. How do I determine when I am eligible for Bay An Toan coverage?"
   ],
   "c8lwlang":[
      "4. Đối với Quyền lợi B",
      "4. For Benefit B"
   ],
   "goptqpze":[
      "Cơ quan ra QĐ cách ly",
      "Name of Authority makes quarantine decision"
   ],
   "tpnbbknm":[
      "Giấy tờ chứng minh quyền thừa kế/ giấy ủy quyền công chứng/ xác nhận bởi UBND cấp Phường/Xã trở lên",
      "Proof of legal inheritance/notarized proxy document or confirmation by People’s Committee at level at Ward/Commune and above or other documents in accordance with the law ( In case the Insured is above 18 years of age)"
   ],
   "link_portal_guide":[
      "https://hyperservices.hdinsurance.com.vn/f/870b0a419e6063ef221a78aaf652aeea",
      "https://hyperservices.hdinsurance.com.vn/f/0a419e6063ef221a78aaf652aeea584a"
   ],
   "link_sky_care_guide": [
      "https://hyperservices.hdinsurance.com.vn/f/6c81a72c44cd7bb03411c660ddd0173e.pdf",
      "https://hyperservices.hdinsurance.com.vn/f/c81a72c44cd7bb03411c660ddd0173ef.pdf"
   ],
   "link_sky_care_dom_guide": [
      "https://hyperservices.hdinsurance.com.vn/f/9edde5a7a02754774e3d74e418030bcf.pdf", //VN
      "https://hyperservices.hdinsurance.com.vn/f/edde5a7a02754774e3d74e418030bcfc.pdf"  //EN
   ],
   "searching_note": [
      "(Lưu ý: Quý khách vui lòng chọn đúng sản phẩm và sử dụng mã đặt chỗ (PNR) trên hệ thống của Vietjet để tra cứu.)",
      "(Note: Please make sure to select the correct product and use the booking code (PNR) on Vietjet's system to search.)"
   ],
   "50pkrlva":[
      "Thành phố",
      "Province"
   ],
   "pcig4nxl":[
      "Giấy tờ tùy thân và giấy tờ khác",
      "I.S. and other documents"
   ],
   "dgg7ke3p":[
      "3. Đối với Quyền lợi A2",
      "3. For Benefit A2"
   ],
   "dk5u5sgr":[
      "4. Loại trừ bảo hiểm",
      "4. Insurance Exclusions"
   ],
   "1pzrys4k":[
      "Yêu cầu bồi thường",
      "Claim request"
   ],
   "loj7azpi":[
      "THÔNG TIN",
      "INFORMATION"
   ],
   "rasdqce9":[
      "sdsdsdsdssdsdsdsdsdsd",
      "asdsdsds"
   ],
   "ounmpont":[
      "Giấy tờ khác",
      "Other Documents"
   ],
   "bapevgyz":[
      "<p>Chương trình Bảo hiểm Bay An Toàn do Công ty TNHH Bảo hiểm HD (HDI) phối hợp thực hiện cùng Công ty cổ phần Hàng không VietJet.</p>",
      "<p>Fly Safe insurance program is created in cooperation between HD Insurance Company Limited (HDI) and Vietjet Aviation Joint Stock Company (VietJet). </p>"
   ],
   "obcgyxam":[
      "6. Các điều kiện loại trừ trong chương trình Bảo hiểm Bay An Toàn này là gì",
      "6. What are the exclusions from this Fly Safe insurance program?"
   ],
   "wmxszedf":[
      "Số tiền yêu cầu",
      "Amount required"
   ],
   "b7peygbc":[
      "Chủ tài khoản",
      "Beneficiary"
   ],
   "k6uwwotz":[
      "Điều Kiện, Điều Khoản",
      "Insurance conditions"
   ],
   "2lk9upe9":[
      "1. Chương trình Bảo hiểm Bay An Toàn là gì?",
      "1. What is Fly Safe insurance program?"
   ],
   "udjzfd8k":[
      "<p>1.   Đối tượng tham gia bảo hiểm (“Người được bảo hiểm”):</p>\n<p>-  Hành khách mua và sử dụng vé máy bay của VietJet, không phân biệt loại giá vé, quốc tịch.&nbsp;</p>\n<p>-  Hành khách đáp ứng theo quy định và Điều lệ vận chuyển của VietJet.</p>\n<p>-  Hành khách được chấp thuận vận chuyển trong Thời Hạn Bảo Hiểm.</p>\n<p>-  Chỉ áp dụng cho hành khách trên chuyến bay nội địa do VietJet khai thác</p>\n<p>2.   Không áp dụng :</p>\n<p>-  Chuyến bay Thuê chuyến</p>\n<p>-  Chuyến bay do Thái VietJet khai thác</p>\n<p>-  Thành viên tổ bay trên chuyến bay</p>\n<p>-  Vé thưởng/tặng, vé ưu đãi (G1,G2)</p>\n<p>-  Vé SFC, vé nhân viên VietJet công tác</p>\n<p></p>\n<p></p>\n",
      "<p>1. Insurance participants (The Insured)</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">-  </span> Applied for all passengers that purchase and use flight tickets of VietJet, without diffentiation between ticket fare types, nationalities.</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">-  </span> Passengers that meet Terms and Conditions of VietJet.</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">-  </span> Passengers that has been approved to be tranported during Insured Period.</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">-  </span> Only applied for passengers on domestic flights operated by VietJet</p>\n<p>2. Not applicable for:</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">-  </span> Charter flights</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">-  </span> Flights operated by Thai VietJet</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">-  </span> Flight crews on flights</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">-  </span> Bonus/gift, sale tickets (G1, G2)</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">-  </span> SFC tickets, business tickets for VietJet employees</p>\n"
   ],
   "y6bgulqu":[
      "Tên khu cách ly",
      "Name of quarantined place"
   ],
   "7mpsinnw":[
      "Quyết định cách ly tập trung, Giấy xác nhận hoàn thành cách ly của cơ quan có thẩm quyền và các giấy",
      "Certificate of completing quarantine issued by authority or other relevant documents"
   ],
   "err_ben_null":[
      "Vui lòng khai báo tổn thất",
      "Please declare Information of Loss"
   ],
   "hq2qyuhn":[
      "Quy tắc bảo hiểm",
      "Policy wording"
   ],
   "wkgimazp":[
      "3. Công ty Bảo hiểm nào cung cấp chương trình Bảo hiểm Bay An toàn?",
      "3. Which Insurance Company provides Fly Safe Insurance Program?"
   ],
   "fxmqumg5":[
      "9. Khi nào tôi tra cứu được thông tin Bảo hiểm của bản thân và hình thức như thế nào?",
      "9. How can I search my insurance information?"
   ],
   "sbnthq8h":[
      "18. Tôi có thể nhận tiền bồi thường bằng những hình thức nào?",
      "18. How may I receive indemnity?"
   ],
   "5sxi0rfk":[
      "Người Được Bảo Hiểm",
      "Insured Person"
   ],
   "fjvajqcl":[
      "Số tài khoản",
      "Account No"
   ],
   "4pqnxwfx":[
      "Ngày nhập viện",
      "Date of admission"
   ],
   "7btjyvcs":[
      "Hồ sơ tai nạn (Trường hợp gặp tai nạn trong lúc điều khiển xe tham gia giao thông)",
      "Accident records (In case of accidents while driving vehicles in traffic)"
   ],
   "vxplhpke":[
      "16. Nếu tôi muốn thay đổi Chuyến bay, tôi có nhận được quyền lợi Bảo hiểm?",
      "16. If I want to change the flight, may I receive insurance benefits?"
   ],
   "ctnjw0bg":[
      "<p>Cám ơn Quý khách đã lựa chọn bay cùng VietJet. Chương trình Bảo hiểm Bay An Toàn của VietJet không áp dụng cho:</p>\n<p>-      Hành khách là em bé dưới 14 (mười bốn) ngày tuổi.</p>\n<p>-\tHành khách sử dụng các Chuyến bay thuê chuyến, chuyến bay được khai thác bởi Thái VietJet.</p>\n<p>-\tHành khách sử dụng vé thưởng/tặng, vé ưu đãi riêng của VietJet.</p>\n<p>-\tHành khách không được chấp nhận vận chuyển trong Thời hạn bảo hiểm.</p>\n<p></p>\n",
      "<p>Thank you for choosing to fly with VietJet. VietJet's Fly Safe insurance does not apply to:</p>\n<p>-\tPassengers who are infants under 14 (fourteenth) days old.</p>\n<p>-\tPassengers using VietJet's charter flights, flights operated by Thai VietJet.</p>\n<p>-\tPassengers using gift vouchers, free of charge ticket.</p>\n<p>-\tNot accepted to be transported during Insured Period.&nbsp;</p>\n<p></p>\n"
   ],
   "dbshfxcg":[
      "Số tiền bồi thường",
      "Claim amount"
   ],
   "8me8rzzi":[
      "Ngày vào",
      "Date of admission"
   ],
   "txy6rphx":[
      "Giấy tờ chứng minh quan hệ với NĐBH (trường hợp khai hộ NĐBH dưới 18t)",
      "Copy of household registration book or birth certificate, guardianship document or other documents in accordance with the law ( In case of reimburse for the Insured under 18 years of age)"
   ],
   "9cflmuee":[
      "<p>Quý Khách có thể nhận tiền bồi thường bằng hình thức chuyển khoản, Quý khách phải cung cấp số tài khoản cá nhân (khi nộp hồ sơ bồi thường trực tuyến).</p>",
      "<p>Passengers may receive indemnity by bank transfer method, the passengers must provide a personal bank account (when making an online claim request)</p>"
   ],
   "4exzym2q":[
      "sdsdsd",
      "sdsdsd"
   ],
   "2stxdrp2":[
      "Thành Công",
      "Success"
   ],
   "dlii8f7l":[
      "Số điện thoại",
      "Tel No"
   ],
   "irayzl19":[
      "Quy tắc bảo hiểm",
      "Policy wording"
   ],
   "lbbypq6d":[
      "Giấy tờ chứng minh quan hệ với NĐBH (trường hợp khai hộ NĐBH dưới 18t)",
      "Copy of household registration book or birth certificate, guardianship document or other documents in accordance with the law ( In case of reimburse for the Insured under 18 years of age)"
   ],
   "nsxf0gt5":[
      "<p>Thời hạn cung cấp hồ sơ, yêu cầu trả tiền bảo hiểm hoặc bồi thường theo hợp đồng bảo hiểm là 365 (ba trăm sáu mươi lăm) ngày, kể từ ngày xảy ra Sự kiện bảo hiểm.&nbsp;</p>\n",
      "<p>Passengers can claim within 365 days from the date of the Insurance Event occurs</p>\n"
   ],
   "ux9pvtza":[
      "<p>Trường hợp Quý Khách thực hiện thay đổi Chuyến bay và được VietJet xác nhận việc thay đổi này thành công, Quý khách sẽ được hưởng quyền lợi và thời hạn Bảo hiểm hiểm khi Quý khách hoàn thành Chuyến bay mới.&nbsp;</p>\n",
      "<p>In case you have an unexpectedly changed flight which is confirmed by VietJet within the period of insurance, you are entitled to the benefits of the new flight.</p>\n"
   ],
   "rhp4sh3k":[
      "<p>Ngoài những giấy tờ yêu cầu cung cấp như các quyền lợi B “Trợ cấp trong thời gian cách ly”, trừ Phiếu xác nhận hoàn thành cách ly y tế tập trung, Quý khách cần bổ sung thêm:</p><p>- Biên bản tai nạn</p><p>- Giấy phép lái xe (Trong trường hợp khách gặp tai nạn trong lúc điều khiển xe tham gia giao thông)</p><p>- Bản chính Hóa đơn/Phiếu thu chi phí y tế, thuốc điều trị.</p><p>- Bản sao Toa thuốc, kết luận y khoa, kết quả xét nghiệm.</p>",
      "<p>All required documents such as benefit B “Allowance during quarantine”, except for Certificate of completion of medical isolation, Passengers need to add:</p><p>-\tAccident report</p><p>-\tValid driving license (In case of an accident while driving a vehicle in traffic)</p><p>-\tOriginal Invoices (hospital fee, drugs…)</p><p>-\tPrescription, medical diagnosis results, X-ray results.</p>"
   ],
   "jcqxauhp":[
      "Số hiệu chuyến bay",
      "Flight No."
   ],
   "5pxdpuwx":[
      "Số hợp đồng bảo hiểm",
      "Policy No"
   ],
   "5ujoh03b":[
      "<p>HDI sẽ không chịu bất kỳ trách nhiệm bồi thường trong những trường hợp dưới đây:</p>\n<p>- Hành vi cố ý của Người được bảo hiểm, bên mua bảo hiểm hoặc người thụ hưởng; Trường hợp hợp đồng bảo hiểm có nhiều Người thụ hưởng thì chỉ Người thụ hưởng có hành vi vi phạm bị từ chối thanh toán. HDI vẫn có trách nhiệm thanh toán tiền bảo hiểm cho những Người thụ hưởng còn lại.</p>\n<p>- Người được bảo hiểm vi phạm pháp luật về các hành vi dưới đây:</p>\n<p>a. Đua xe trái phép;</p>\n<p>b. Điều khiển xe không có Giấy phép lái xe/ hoặc Giấy phép lái xe không hợp lệ;</p>\n<p>c. Đi vào đường cấm, khu vực cấm;</p>\n<p>d. Đi ngược chiều của đường một chiều, đi ngược chiều trên đường có biển “cấm đi ngược chiều”;</p>\n<p>e. Không đi đúng làn đường, phần đường quy định.</p>\n<p>- Người được bảo hiểm có sử dụng rượu, bia vượt quá nồng độ theo quy định của Luật giao thông đường bộ.</p>\n<p>- Người được bảo hiểm có sử dụng ma túy hoặc các chất kích thích khác.</p>\n<p>- Người được bảo hiểm điều trị hoặc sử dụng thuốc không theo sự chỉ dẫn của cơ sở y tế/bác sĩ điều trị (trừ trường hợp bác sĩ tự điều trị bệnh phù hợp với chuyên khoa của mình).</p>\n<p>- Người được bảo hiểm tham gia các hoạt động thể thao chuyên nghiệp, các môn thể thao gồm: đua xe đạp, xe mô tô, đua ngựa, bóng đá, đấm bốc, leo núi, lướt ván, đua thuyền, khảo sát, thám hiểm. Người được bảo hiểm tham gia các cuộc diễn tập, huấn luyện quân sự, tham gia chiến đấu của các lực lượng vũ trang, tham gia các hoạt động hàng không (trừ khi với tư cách là hành khách có vé).</p>\n<p>- Người được bảo hiểm tham gia đánh nhau trừ khi được cơ quan chức năng có thẩm quyền kết luận/ xác nhận đó là hành vi phòng vệ chính đáng.</p>\n<p>- Người được bảo hiểm bị cảm đột ngột, trúng gió, sẩy thai, bệnh nghề nghiệp, những tai biến trong quá trình điều trị bệnh và thai sản.</p>\n<p>- Người được bảo hiểm bị ngộ độc thức ăn, đồ uống hoặc hít phải hơi độc, khí độc, chất độc.</p>\n<p>- Các chi phí cung cấp, bảo dưỡng hay lắp ráp sửa chữa các thiết bị hoặc bộ phận giả, dụng cụ chỉnh hình, các thiết bị trợ thính hoặc thị lực, nạng hay xe lăn hoặc các dụng cụ khác như: đĩa đệm, nẹp, đinh, vít, chốt neo...</p>\n<p>- Trường hợp Người được bảo hiểm bị thương tật thân thể hoặc chết xảy ra do những rủi ro mang tính chất thảm họa như: động đất, núi lửa, sóng thần, nhiễm phóng xạ, chiến tranh, nội chiến, khủng bố, đình công, bạo động, dân biến, phiến loạn các hoạt động dân sự hoặc hành động của bất kỳ người cầm đầu của tổ chức nào nhằm lật đổ, đe dọa chính quyền, kiểm soát bằng vũ lực.</p>\n<p>- Người được bảo hiểm mất tích (trừ trường hợp tại Quyết định, Bản án có hiệu lực pháp luật, Tòa án có kết luận chính thức về nguyên nhân Người được bảo hiểm mất tích do tai nạn xảy ra trong thời hạn bảo hiểm).</p>\n<p>- Người được bảo hiểm đột tử hoặc tử vong không rõ nguyên nhân hoặc bị tử vong mà không thể xác định được nguyên nhân tử vong vì bất kỳ lý do nào</p>",
      "<p>HDI will not bear any liability for compensation in the following cases:</p>\n\n<p>- Willful behavior of the Insured, the Policyholder or the Benefiacry; In case the insurance contract has many Beneficiaries, only the Beneficiary who has violated the act will be refused payment of indemnity, HDI is still responsible for the insurance payment to the remaining Beneficiaries.</p>\n\n<p>- The Insured violates the law on the following acts:</p>\n\n<p>a. Illegal racing;</p>\n\n<p>b. Operating a vehicle without an invalid Driver’s License / Illegal Driver’s License;</p>\n\n<p>c. Entering restricted roads or restricted areas;</p>\n\n<p>d. Going in the opposite direction of a one-way street, going in the opposite direction on a road with a signboard « forbidden to go in the opposite direction » ;</p>\n\n<p>e. Not going to the right lane, road section.</p>\n\n<p>- The Insured uses alcohol in excess of the concentration presribed by the Law on Road Traffic.</p>\n\n<p>- The Insured uses drugs or other stimulants.</p>\n\n<p>- The Insured treats or uses drugs not under the direction of the medical facility / treating doctor (unless the doctor self-medicates for the disease in accordance with his specialty).</p>\n\n<p>- The Insured participants in professional sports activities, sports including: bicycle racing, motorbike racing, horse racing, soccer, boxing, rock climbing, windsurfing, racing, surveying explorers. The Insured participates in military drills, military training, combat participation by the armed forces, and aviation activities (except as a ticketed passenger).</p>\n\n<p>- The Insured participates in a fight unless it is concluded/determined by a competent authority to be a legitimate defense.</p>\n\n<p>- The Insured suffers a sudden cold, apolexy, miscarriage, occupational disease, complications during treatment and pregnancy.</p>\n\n<p>- The Insured suffers from food and drink poisoning or inhalation of noxious vapors, poison gas or poison.</p>\n\n<p>- Expenses for the provision, maintenance or assembly of repair of prosthetic or prosthetic devices, orthopedic appliances, hearing or vision aids, crutches or wheelchair or other devices such as disc, fasteners, nails, screws, anchor bolts, etc.</p>\n\n<p>- Where the Insured suffers bodily injury or death due to catastrophic risks such as earthquake, volcano, tsunami, radioactive contamination, war, civil war, terrorism, strikes, riot, civil commotion, rebellion or civil action or action of any leader of any organization to overthrow, threaten the government, control by force.</p>\n\n<p>- The Insured is missing (except for the cases in the legally effective Decision or judgement, the Court has an official conclusion on the cause of the Insured’s dissappearance due to an accident occuring whitn the insurance period).</p>\n\n<p>- The Insured dies suddenly or dies of unknown cause or dies without identifying the cause of death for any reason.</p>"
   ],
   "kquac0sk":[
      "sdsdsd",
      "sdsdsds"
   ],
   "vffqtx4e":[
      "7. Quyền lợi Bảo hiểm của tôi theo Chương trình Bay An Toàn như thế nào?",
      "7. What are my benefits according to Fly Safe insurance program?"
   ],
   "ieb090ju":[
      "17. Nếu tôi bị thay đổi Chuyến bay không mong muốn do VietJet, tôi có nhận được quyền lợi Bảo hiểm?",
      "17. If my flight is unexpectedly changed caused by VietJet, may I receive insurance benefits?"
   ],
   "wbucetku":[
      "STT",
      "STT"
   ],
   "45btbc0g":[
      "Giấy yêu cầu bồi thường",
      "Claim Form"
   ],
   "uzcnhtjw":[
      "Thêm File",
      "Upload File"
   ],
   "2h8q6jiy":[
      "asdsd",
      "asdsd"
   ],
   "0lgdflcg":[
      "Xác nhận",
      "Confirmation"
   ],
   "1fvj6zzn":[
      "Chọn quan hệ với NĐBH",
      "Choose your relationship with the Insured"
   ],
   "3gcxnaen":[
      "Ngày xảy ra",
      "Date of accident"
   ],
   "not_found_result":[
      "Không tìm thấy kết quả phù hợp\n",
      "No matching results found\n"
   ],
   "8r4r49zk":[
      "<p><strong>Đối với quyền lợi A1 và A2:</strong></p>\n<p>- Trong thời hạn 30 ngày kể từ ngày xảy ra Sự kiện Bảo hiểm, Quý khách phải thông báo cho HDI bằng văn bản về Sự kiện Bảo hiểm đã xảy ra theo mẫu do HDI ban hành.</p>\n<p><strong>Đối với quyền lợi B:</strong></p>\n<p>- Trong thời hạn 180 ngày kể từ ngày xảy ra Sự kiện Bảo hiểm, Quý khách phải thông báo cho HDI bằng văn bản về Sự kiện Bảo hiểm đã xảy ra theo mẫu do HDI ban hành.</p>\n",
      "<p><strong>For benefits A1 and A2:</strong></p>\n<p>- Within 30 days from the date of occurrence of an insurance event, Passengers must notify HDI in writing of the insured event that occurred according to the form issued by HDI.</p>\n<p><strong>For benefit B:</strong></p>\n<p>- Within 180 days from the date of occurrence of an insurance event, Passengers must notify HDI in writing of the insured event that occurred according to the form issued by HDI.</p>\n"
   ],
   "34upgybi":[
      "<p>Không. Quý khách không thực hiện Chuyến bay sẽ không được hưởng quyền lợi Bảo hiểm.</p>",
      "<p>No. Passengers who do not actually fly with VietJet, do not receive insurance benefits.</p>"
   ],
   "wrx3d6xy":[
      "<p>Ngoài những giấy tờ yêu cầu cung cấp như các quyền lợi B “Trợ cấp trong thời gian cách ly”, trừ Phiếu xác nhận hoàn thành cách ly y tế tập trung, Quý khách cần bổ sung thêm:</p><p>- Biên bản tai nạn</p><p>- Giấy phép lái xe (Trong trường hợp khách gặp tai nạn trong lúc điều khiển xe tham gia giao thông)</p><p>- Bản chính Hóa đơn/Phiếu thu chi phí y tế, thuốc điều trị.</p><p>- Bản sao Toa thuốc, kết luận y khoa, kết quả xét nghiệm.</p>",
      "<p>All required documents such as benefit B “Allowance during quarantine”, except for Certificate of completion of medical isolation, Passengers need to add:</p><p>-\tAccident report</p><p>-\tValid driving license (In case of an accident while driving a vehicle in traffic)</p><p>-\tOriginal Invoices (hospital fee, drugs…)</p><p>-\tPrescription, medical diagnosis results, X-ray results.</p>"
   ],
   "gcvppmdy":[
      "Mã đặt chỗ",
      "Reservation Code"
   ],
   "va9clnf0":[
      "Loại bảo hiểm",
      "Program"
   ],
   "lcc1uvh7":[
      "Số tiền yêu cầu bồi thường",
      "Claim Amount Required"
   ],
   "lhfvwjwp":[
      "Bạn yêu cầu bồi thường cho ai?",
      "Who are you claim for?"
   ],
   "bk6alezm":[
      "Giờ xảy ra",
      "Time of accident"
   ],
   "bx63gbxx":[
      "Khám phá Bảo hiểm HD",
      "Discover HD Insurance"
   ],
   "gsfk28hy":[
      "Chứng minh nhân dân, Thẻ căn cước, Passport",
      "Identity card or Citizen identification or Passport"
   ],
   "wiycvo0w":[
      "<p>Ngoài các giấy tờ chung, khách hàng cần cung cấp thêm các giấy tờ sau:</p>\n<p>- Biên bản tai nạn:</p>\n<p>&nbsp;&nbsp;a. Đối với tai nạn giao thông (trường hợp có cảnh sát giao thông lập biên bản): cần cung cấp biên bản kết <br>luận điều tra của công an</p>\n<p>&nbsp;&nbsp;b. Đối với tai nạn giao thông (trường hợp không có cảnh sát giao thông lập biên bản)/ Tai nạn lao động/ Tai nạn sinh hoạt: cần cung cấp biên bản tai nạn lao động hoặc bản tường trình tai nạn có xác nhận của Chủ hợp đồng bảo hiểm/ cơ quan chính quyền địa phương.&nbsp;</p>\n<p>- Giấy phép lái xe, Giấy tờ xe (Trong trường hợp khách gặp tai nạn trong lúc điều khiển xe tham gia giao thông)</p>\n<p>- Bản sao giấy chứng tử/Trích lục khai tử&nbsp;</p>\n<p></p>\n",
      "<p>Other than the general documents, customer must provide additional documents include:</p>\n<p>- Accident Record/Accident Report:</p>\n<p>&nbsp;&nbsp;&nbsp;a. For traffic accident (in case there is a police’s witness): provide a record of the investigation conclusion of the police</p>\n<p>&nbsp;&nbsp;&nbsp;b. For traffic accident (in case of without police’s witness)/ Labor accident / Daily-life accident: provide accident record/ accident report with the policy holder's confirmation/ local government&nbsp;</p>\n<p>- Driver license, vehicle registration (In case the insured person is the driver)</p>\n<p>- Copy of death certificate (in case of death)&nbsp;</p>\n"
   ],
   "65h6eus8":[
      "Thương tật vĩnh viễn",
      "Permanent injury"
   ],
   "xoiaux01":[
      "Ngày sinh",
      "Date Of Birth"
   ],
   "9bvg3zel":[
      "Giờ bay thực tế",
      "Actual Flight Start Time"
   ],
   "email_bt_vj":[
      "boithuong.vietjet@hdinsurance.com.vn",
      "boithuong.vietjet@hdinsurance.com.vn"
   ],
   "nf7yiydd":[
      "Giờ đến dự kiến",
      "Estimated Time Arrival (ETA)"
   ],
   "xo3jqjzl":[
      "12. Thời gian giải quyết bồi thường cho tôi là bao lâu?",
      "12. How long does it take to complete my compensation?"
   ],
   "khb0y4g1":[
      "19. Ai có thể yêu cầu bồi thường khi có Sự kiện Bảo hiểm phát sinh?",
      "19. Who may make a claim request when insurance events occur?"
   ],
   "zyonhh3t":[
      "Ngày gửi yêu cầu",
      "Request Date"
   ],
   "8flfvfio":[
      "Ngân hàng",
      "Bank name"
   ],
   "3c9rvuxs":[
      "Ngày xuất viện",
      "Date of discharge"
   ],
   "wntmjrud":[
      "Giấy yêu cầu bồi thường",
      "Claim Form"
   ],
   "uzqdqnyu":[
      "Sản Phẩm Bảo Hiểm",
      "Program"
   ],
   "zbuqgo7r":[
      "Hồ sơ giấy tờ",
      "Documents"
   ],
   "mcmcphxr":[
      "Quan hệ với NĐBH",
      "RelationShip"
   ],
   "q8f9rjax":[
      "Tỉnh/ TP - Quận/ huyện - Phường/ xã xảy ra",
      "Province-District-Awards of accident"
   ],
   "mk9cz8xc":[
      "<table class=\"faq-table\" style=\"overflow-x: auto;\"><tr class=\"tbhead\"><th>QUYỀN LỢI BẢO HIỂM</th><th>SỐ TIỀN BẢO HIỂM/NGƯỜI (VNĐ)</th></tr><tr><td> <p><b>A.</b> Bảo hiểm tai nạn bao gồm:</p></td><td class=\"text-center bold-price\">20,000,000</td></tr><tr><td><p> <b>A1.</b> Tử vong hoặc Thương tật toàn bộ vĩnh viễn do tai nạn:</p></td><td class=\"text-center bold-price\">Tối đa đến số tiền bảo hiểm</td></tr><tr><td><p><b>A2.</b> Chi phí y tế thực tế phát sinh do Tai nạn</p></td><td class=\"text-center bold-price\">5,000,000</td></tr><tr><td><p><b>B. </b> Bảo hiểm mất giảm chi phí sinh hoạt,mất giảm thu nhập do bị cách ly bởi dịch bệnh <b>(tối đa 21 ngày, áp dụng cho một lần cách ly liên tục trong suốt Thời hạn bảo hiểm)</b></p></td><td class=\"text-center bold-price\">\n            1,000,000/ngày\n        </td></tr></table>\n<p>Hành khách được hưởng tiền bảo hiểm độc lập theo từng quyền lợi bảo hiểm nhưng không vượt quá tổng hạn mức Số tiền bảo hiểm của từng phần</p>",
      "<table class=\"faq-table\" style=\"overflow-x: auto;\"><tr class=\"tbhead\"><th>BENEFITS</th><th>Sum Insured/Person (VND)</th></tr><tr><td><p><b>A.</b> Accident insurance including:</p></td><td class=\"text-center bold-price\">20,000,000</td></tr><tr><td><p> <b>A1.</b> Death or permanently total disablement due to Accident:</p></td><td class=\"text-center bold-price\">Maximum of Sum Insured</td></tr><tr><td><p><b>A2.</b> Actual incurred medical expenses due to Accident</p></td><td class=\"text-center bold-price\">5,000,000</td></tr><tr><td><p><b>B. </b> Insurance for loss and reduction of living expenses, loss and reduction of income due to quarantine by disease <b>(maximum 21 days, applicable for one consecutive quarantine period during Insured period)</b></p></td><td class=\"text-center bold-price\">\n            1,000,000/day\n        </td></tr></table>\n<p>Passenger shall receive indemnity separately for each benefit but not exceed the Sum Insured of each benefit.</p>"
   ],
   "xygtume0":[
      "Hình thức",
      "Way of receiving money"
   ],
   "zzsalhgc":[
      "Bản thân",
      "Yourself"
   ],
   "h82oqgru":[
      "Thông tin về tổn thất",
      "Particulars of loss / Occurance"
   ],
   "fdguycdu":[
      "Thành phố",
      "Province"
   ],
   "osc2y9h7":[
      "Tên người khai hộ",
      "Declare Name"
   ],
   "gua78xva":[
      "Quay lại trang chủ",
      "Back to home"
   ],
   "0oxnt3my":[
      "Số nhà, ngõ / ngách, đường xảy ra",
      "Place of accident"
   ],
   "mnazberz":[
      "Vui lòng bổ sung các thông tin người được bảo hiểm",
      "Please provide aditional Information"
   ],
   "qdizh5gh":[
      "Ngày chi trả",
      "Payment date"
   ],
   "gjdhstda":[
      "Ngày ra",
      "Date of discharge"
   ],
   "uczrbgct":[
      "Hồ sơ y tế (trường hợp có khám, chữa, điều trị, cách ly)",
      "Medical records (in case of examination, treatment, treatment, isolation)"
   ],
   "ertoejrh":[
      "<p>Quý khách vui lòng căn cứ theo Thời hạn Bảo hiểm để xác định các quyền lợi Bảo hiểm.</p>\n<p>- Thời hạn Bảo hiểm là khoảng thời gian được xác định theo Giấy chứng nhận Bảo hiểm mà trong khoảng thời gian đó, Công ty TNHH Bảo hiểm HD có trách nhiệm chi trả Tiền Bảo hiểm hoặc bồi thường thiệt hại cho Người được Bảo hiểm nếu xảy ra Sự kiện Bảo hiểm.</p>\n<p>- Thời hạn Bảo hiểm của chương trình Bay An Toàn như sau:&nbsp;</p>\n<ul>\n<li><strong>Đối với quyền lợi A1 và A2: </strong>bắt đầu từ giờ khởi hành thực tế của Chuyến bay cho đến hết 24 giờ kể từ giờ khởi hành thực tế của Chuyến bay.</li>\n<li><strong>Đối với quyền lợi B:</strong> bắt đầu từ giờ khởi hành thực tế của Chuyến bay cho đến hết 30 ngày kể từ giờ khởi hành thực tế của Chuyến bay.</li>\n</ul>\n",
      "<p>Please follow Insurance period to determine your insurance benefits.</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">- </span> Insured period is a period determined according to the Insurance Certificate during which HD Insurance Company Limited is responsible for paying the insurance indemnity or indemnifying the insured if the insurance event occurs.</p>\n<p><span style=\"color: rgb(90,97,105);background-color: rgb(255,255,255);font-size: 15px;\">- </span> Fly Safe Insured period is as follows:</p>\n<ul>\n<li><strong>For benefits A1 and A2: </strong>starts from the actual time of departure until at the end of the 24 hours since the flight departure actually.&nbsp;</li>\n<li><strong>For benefit B:</strong> starts from the actual time of departure until at the end of the 30 days since the flight departure actually.</li>\n</ul>\n<p></p>\n"
   ],
   "csrwisda":[
      "<p>- Bản thân Người được Bảo hiểm; (Người thực hiện Chuyến bay)</p><p>- Người thân của Người được Bảo hiểm (Vợ/chồng/con/bố/mẹ): phải cung cấp Hình chụp Bản gốc các giấy tờ chứng minh mối quan hệ nhân thân như: giấy khai sinh hoặc sổ hộ khẩu hoặc giấy đăng ký kết hôn hoặc giấy tờ khác theo quy định của pháp luật.</p><p>- Người được Người được Bảo hiểm ủy quyền: phải cung cấp Giấy ủy quyền hợp pháp.</p>",
      "<p>-\tThe Insured (Individual flies with VietJet).</p><p>-\tRelatives of the Insured (spouse/ child/ parent): provide a photo of original documents as a proof of the personal relationship such as: birth certificate or household book or marriage certificate or other documents prescribed by law.</p><p>-\tIndividual authorized by the Insured: provide a “Valid Authorized Letter”.</p>"
   ],
   "4uff4nzx":[
      "Quyền lợi bảo hiểm",
      "Insurance Benefits"
   ],
   "o2bjchek":[
      "Yêu cầu bồi thường",
      "Claim"
   ],
   "u3nifi1w":[
      "Bước",
      "Step"
   ],
   "npdeisdr":[
      "<p>Quý khách có thể thực hiện việc yêu cầu bồi thường quyền lợi qua 01 (một) trong 03 (ba) lựa chọn dưới đây:</p>\n<p>1. Quý khách có thể gọi điện tới số đường dây nóng: (+84) 1900 068 898 để được hỗ trợ; hoặc</p>\n<p>2. Quý khách có thể truy cập địa chỉ <a href=\"https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi\" target=\"_self\">https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi</a>, tra cứu thông tin khách hàng và thực hiện yêu cầu bồi thường trực tuyến; hoặc</p>\n<p>3. Gửi yêu cầu bồi thường cho HDI tới địa chỉ thư điện tử email: boithuong.vietjet@hdinsurance.com.vn</p>\n",
      "<p>Passengers can make a claim through one of the following options:</p>\n<p>1.\tPassengers can call hotline number: (+84) 1900 068 898 for assistance; or</p>\n<p>2.\tPassengers can access website  <a href=\"https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi\" target=\"_self\">https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi</a>, search customer’s information and make an online claim request; or</p>\n<p>3.\tSend a claim request to HDI via email: boithuong.vietjet@hdinsurance.com.vn</p>\n"
   ],
   "4n12tqwj":[
      "Mô tả ngắn gọn tai nạn",
      "Brief description of accident"
   ],
   "btqr8bww":[
      "Nơi đến",
      "Arrival"
   ],
   "xw6tyom0":[
      "<p>\n\t\t\t\t\t\tYêu cầu bồi thường THÀNH CÔNG\n\t\t\t\t\t</p>\n\t\t\t\t\t<p>\n\t\t\t\t\t\tQuý khách vừa thực hiện yêu cầu bồi thường thành công. Bảo Hiểm HD sẽ kiểm tra thông tin, liên lạc lại để hướng dẫn quý khách thu thập chứng từ, bổ sung các giấy tờ còn thiếu. \n\t\t\t\t\t</p>\n\t\t\t\t\t\n\t\t\t\t\t<p> Xin cảm ơn!</p>",
      "<p>Successful claims</p>\n<p>You have just made a successful claim. HD Insurance will check the information, contact you again to guide you to collect documents, supplement the missing documents. </p>\n<p> Thank you!</p>"
   ],
   "b04dtstm":[
      "<p>-\tGiấy chứng nhận bảo hiểm điện tử</p>\n<p>-\tBản sao CMND/ CCCD/ Hộ chiếu</p>\n<p>-\tBản sao Thẻ lên tàu bay (boarding pass)</p>\n<p>-\tBản sao thẻ hành lý (luggage tag)</p>\n<p><strong>Lưu ý: </strong>Trường hợp thông tin trên Thẻ lên tàu bay (boarding pass) khác với thông tin trên giấy chứng nhận bảo hiểm, Người được bảo hiểm phải cung cấp các tài liệu, chứng từ của Hãng hàng không để xác nhận về việc thay đổi thông tin liên quan.&nbsp;</p>\n<p></p>\n",
      "<p>-\tE-Insurance Certificate</p>\n<p>-\tCopy of Identification document(s)</p>\n<p>-\tCopy of Boarding pass</p>\n<p>-\tCopy of Luggage tag</p>\n<p><strong>Notice: </strong>In case the information on the boarding pass is different from the insurance certificate, the Insured must provide proof, document(s) from the Airline confirming the changes.</p>\n<p></p>\n"
   ],
   "s88aukdl":[
      "Yêu cầu hoá đơn thành công",
      "Require invoices success"
   ],
   "ixkcl4az":[
      "Báo cáo bất thường về hành lý (PIR) từ hãng hàng không liên quan",
      "Property irregularity report (PIR) from the airlines related"
   ],
   "lgpn6rzq":[
      "2.\tGiấy tờ chung\n",
      "2.\tGeneral Documents\n"
   ],
   "3adqso4o":[
      "<p>Sau <strong>24 (hai mươi bốn)</strong> <strong>tiếng</strong> kể từ thời điểm Chuyến bay hạ cánh thực tế, Quý khách có thể tra cứu thông tin giấy chứng nhận Bảo hiểm bằng 01 (một) trong 02 (hai) hình thức dưới đây:</p>\n<p>1. Tra cứu trực tuyến tại trang thông tin điện tử: <a href=\"https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi\" target=\"_self\">https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi</a> hoặc</p>\n<p>2. Gọi đến tổng đài: (+84) 1900 068 898 để được hỗ trợ kiểm tra thông tin và hướng dẫn trực tiếp.</p>\n",
      "<p>After <strong>24 (twenty four)  hours </strong>from the actual flight arrival time, Passengers can look up the insurance certificate by 01 (one) of 02 (two) methods as below:</p>\n<p>1.\tLook up at website: <a href=\"https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi\" target=\"_self\">https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi</a> or</p>\n<p>2.\tCall hotline number: (+84) 1900 068 898 for support in checking information and direct instructions.</p>\n"
   ],
   "egdsqhvw":[
      "<p>Trên cùng một Chuyến bay:</p>\n<p>- Đối với các Sự kiện Bảo hiểm thuộc “Quyền lợi A1. Tử vong hoặc thương tật toàn bộ vĩnh viễn do Tai nạn và Quyền lợi A2. Chi phí y tế thực tế phát sinh do Tai nạn”: Quý khách chỉ được lựa chọn hưởng 01 (một) quyền lợi Bảo hiểm có mức chi trả cao nhất.</p>\n<p>- Đối với các Sự kiện Bảo hiểm thuộc “Quyền lợi B.Mất giảm chi phí sinh hoạt, mất giảm thu nhập do bị cách ly bởi dịch bệnh”: Quý khách được chi trả độc lập với các quyền lợi Bảo hiểm như trên.</p>\n",
      "<p>On the same Flight:</p>\n<p>-\tFor events “Benefit A1. Death or Total Permanent Disablement due to Accident(s) and Benefits A2. Actual Medical Expenses due to Accident(s)”: Passengers are only eligible for 01 (one) benefit with the highest indemnity.</p>\n<p>-\tFor Insured events “Benefit B. Allowance during quarantine”: You are paid separately from the above insurance benefits.</p>\n"
   ],
   "vpdxp3i8":[
      "<p>Ngoài những giấy tờ yêu cầu cung cấp như các quyền lợi B “Trợ cấp trong thời gian cách ly”, trừ Phiếu xác nhận hoàn thành cách ly y tế tập trung, Quý khách cần bổ sung thêm:</p><p>- Biên bản tai nạn</p><p>- Giấy phép lái xe (Trong trường hợp khách gặp tai nạn trong lúc điều khiển xe tham gia giao thông)</p><p>- Trích lục khai tử (trong trường hợp Người được Bảo hiểm Tử vong)</p>",
      "<p>All required documents such as benefit B “Allowance during quarantine”, except for Certificate of completion of medical isolation, Passengers need to add:</p><p>-\tAccident report</p><p>-\tValid driving license (In case of an accident while driving a vehicle in traffic)</p><p>-\tDeath Certificate (in case the Insured dies)</p>"
   ],
   "2pohwwwd":[
      "Số Giấy Chứng Nhận",
      "Insurance Certificate No."
   ],
   "5yjtbbp1":[
      "Ngày khởi hành",
      "Departure Date"
   ],
   "qhhb7cfh":[
      "Ngày sinh",
      "Date Of Birth"
   ],
   "8gtrlu9p":[
      "Chủ tài khoản",
      "Beneficiary"
   ],
   "qyb8rbnd":[
      "Trạng thái yêu cầu",
      "Request status"
   ],
   "v9vpkvgg":[
      "Hồ sơ giấy tờ",
      "Documents"
   ],
   "ymiebx7e":[
      "Vé điện tử hoặc Email xác nhận hành trình / Thẻ lên tàu bay",
      "E-ticket or Booking confirmation email / Boarding pass"
   ],
   "56n84idf":[
      "<p>Ngoài các giấy tờ chung, khách hàng cần cung cấp thêm các giấy tờ sau:</p>\n<p>- Bản chính Hóa đơn/Phiếu thu hợp lệ theo quy định của Bộ Tài chính cho chi phí y tế, thuốc điều trị. Trong trường hợp bệnh viện/phòng khám cung cấp hóa đơn điện tử, NĐBH cần yêu cầu cơ sở y tế cung cấp hóa đơn chuyển đổi từ hóa đơn điện tử.</p>\n<p>- Bảng kê chi tiết/ biên lai/phiếu thu đi kèm với hóa đơn.</p>\n<p>- Bản sao Toa thuốc, kết luận y khoa, giấy chứng thương, chỉ định và kết quả siêu âm, xquang, xét nghiệm cận lân sàng đã tiến hành thực hiện.&nbsp;</p>\n<p></p>\n",
      "<p>Other than the general documents, customer must provide additional documents include:</p>\n<p>-  Original invoices/ receipts of hospital treatment/ medication fee according to MOF’s regulation. In case the hospitals/ clinics provide an electronic invoice, the insured person needs to request the medical facility to provide a convert invoice from the electronic invoice.</p>\n<p>-  Detail breakdown tables/receipts of invoices.</p>\n<p>-  Copy of prescription, medical report, injury certificate, test results.&nbsp;</p>\n<p></p>\n"
   ],
   "hofhw3zh":[
      "Rút gọn",
      "Collapse"
   ],
   "rxrrlbfu":[
      "Số CMND/ CCCD/ Hộ Chiếu",
      "ID Number"
   ],
   "whcvqu2a":[
      "Ngày bay thực tế ",
      "Actual Flight Start Date"
   ],
   "8miuekga":[
      "<p>-  Bảo hiểm sẽ bắt đầu có hiệu lực từ thời điểm: hành lý ký gửi hoàn tất thủ tục ký gửi.</p>\n<p>-  Bảo hiểm sẽ hết hiệu lực khi tại lúc kết thúc hành trình bay: hành lý ký gửi được nhận bởi NĐBH tại khu vực nhận hành lý.</p>\n<p></p>\n",
      "<p>-  Insurance is valid starting from: the time the checked baggage finished checking-in</p>\n<p>-  Insurance is valid until the end of flight journey: the Insured receive the baggage at Baggage Claim Area.&nbsp;</p>\n<p></p>\n"
   ],
   "yux3voub":[
      "Ngày đến dự kiến",
      "Estimated Date Arrival (EDA)"
   ],
   "l9kuesyt":[
      "<p>- Giấy yêu cầu bồi thường <a href=\"https://hyperservices.hdinsurance.com.vn/f/907209e54f3198fac5c55ef9be2cd071?download=true\" target=\"_blank\"><span style=\"color: rgb(0,123,255);background-color: transparent;font-size: 15px;\">Tải file</span></a><span style=\"color: rgb(0,123,255);background-color: transparent;font-size: 15px;\"> </span>&nbsp;</p>\n<p>- Bản chính hoặc bản sao “Vé điện tử hoặc Email xác nhận hành trình”,&nbsp;</p>\n<p>- Bản chính hoặc bản sao Thẻ lên tàu bay (boarding pass),</p>\n<p>- Bản sao Chứng minh nhân dân/Thẻ căn cước, Hộ chiếu,</p>\n<p>- Bản sao Giấy ra viện,</p>\n<p>- Trường hợp nhận tiền thay cho NĐBH dưới 18 tuổi, người nhận tiền phải cung cấp giấy tờ chứng minh quan hệ với NĐBH như: bản sao sổ hộ khẩu hoặc giấy khai sinh, giấy tờ chứng minh quyền giám hộ hoặc các giấy tờ khác theo quy định của pháp luật.</p>\n<p>- Trường hợp NĐBH trên 18 tuổi và được người khác nhận tiền thay, người nhận tiền phải cung cấp giấy tờ chứng minh quyền thừa kế/ giấy ủy quyền công chứng hoặc được xác nhận bởi UBND cấp Phường/Xã trở lên hoặc các giấy tờ khác theo quy định của pháp luật.&nbsp;</p>\n<p>- Các chứng từ khác theo yêu cầu của HDI tùy trường hợp cụ thể.</p>\n<p></p>\n",
      "<p>- Claim Form <a href=\"https://hyperservices.hdinsurance.com.vn/f/907209e54f3198fac5c55ef9be2cd071?download=true\" target=\"_blank\"><span style=\"color: rgb(0,123,255);background-color: transparent;font-size: 15px;\">Download</span></a><span style=\"color: rgb(0,123,255);background-color: transparent;font-size: 15px;\"> </span>&nbsp;</p>\n<p>- Original or copy of “E-ticket or Booking confirmation email”,</p>\n<p>- Original or copy of Boarding pass,</p>\n<p>- Copy of Identity card or Citizen identification or Passport,</p>\n<p>- Copy of Hospital discharge certificate,</p>\n<p>- In case the beneficiary is on the behalf of the insured person under 18 years of age to receive the reimbursement, they have to provide documents as proof for the relationship with the insured person include: copy of household registration book or birth certificate, guardianship document or other documents in accordance with the law.</p>\n<p>- In case the insured person is above 18 years of age and the beneficiary is the different one, the receivers have to provide legal inheritance confirmation/ letter of attorney notarized by People’s Committee and other documents in accordance with the law.</p>\n<p>- Other documents required by HDI for each specific case.</p>\n<p></p>\n<p></p>\n"
   ],
   "vweceuxn":[
      "Hậu quả khác, chi phí y tế",
      "Other consequences, Medical expenses"
   ],
   "ombrksc7":[
      "<p>Quý khách mua vé của VietJet sẽ không được hưởng quyền lợi Bảo hiểm của chương trình Bảo hiểm Bay An Toàn trong các trường hợp sau:&nbsp;</p>\n<p>- Hành vi cố ý của Người được Bảo hiểm hoặc Người thụ hưởng; Trường hợp hợp đồng Bảo hiểm có nhiều Người thụ hưởng thì chỉ Người thụ hưởng có hành vi vi phạm bị từ chối thanh toán. HDI vẫn có trách nhiệm thanh toán tiền Bảo hiểm cho những Người thụ hưởng còn lại.</p>\n<p>- Người được Bảo hiểm vi phạm pháp luật về các hành vi dưới đây:&nbsp;</p>\n<ul>\n<li>&nbsp;Đua xe trái phép;&nbsp;&nbsp;</li>\n<li>&nbsp;Điều khiển xe không có Giấy phép lái xe/ hoặc Giấy phép lái xe không hợp lệ;&nbsp;&nbsp;</li>\n<li>&nbsp;Đi vào đường cấm, khu vực cấm;&nbsp;&nbsp;</li>\n<li>&nbsp;Đi ngược chiều của đường một chiều, đi ngược chiều trên đường có biển “cấm đi ngược chiều”;&nbsp;&nbsp;</li>\n<li>&nbsp;Không đi đúng làn đường, phần đường quy định;&nbsp;</li>\n</ul>\n<p>- Người được Bảo hiểm có sử dụng rượu, bia vượt quá nồng độ theo quy định của Luật giao thông đường bộ;</p>\n<p>- Người được Bảo hiểm có sử dụng ma túy hoặc các chất kích thích khác;</p>\n<p>- Người được Bảo hiểm điều trị hoặc sử dụng thuốc không theo sự chỉ dẫn của cơ sở y tế/bác sĩ điều trị (trừ trường hợp bác sĩ tự điều trị bệnh phù hợp với chuyên khoa của mình);</p>\n<p>- Người được Bảo hiểm tham gia các hoạt động thể thao chuyên nghiệp, các môn thể thao gồm: đua xe đạp, xe mô tô, đua ngựa, bóng đá, đấm bốc, leo núi, lướt ván, đua thuyền, khảo sát, thám hiểm. Người được Bảo hiểm tham gia các cuộc diễn tập, huấn luyện quân sự, tham gia chiến đấu của các lực lượng vũ trang, tham gia các hoạt động hàng không (trừ khi với tư cách là Hành khách có vé);</p>\n<p>- Người được Bảo hiểm tham gia đánh nhau trừ khi được cơ quan chức năng có thẩm quyền kết luận/ xác nhận đó là hành vi phòng vệ chính đáng;&nbsp;</p>\n<p>- Người được Bảo hiểm bị ngộ độc thức ăn, đồ uống hoặc hít phải hơi độc, khí độc, chất độc;&nbsp;</p>\n<p>- Các chi phí cung cấp, bảo dưỡng hay lắp ráp sửa chữa các thiết bị hoặc bộ phận giả, dụng cụ chỉnh hình, các thiết bị trợ thính hoặc thị lực, nạng hay xe lăn hoặc các dụng cụ khác như: đĩa đệm, nẹp, đinh, vít, chốt neo... ;&nbsp;</p>\n<p>- Trường hợp Người được Bảo hiểm bị thương tật thân thể hoặc chết xảy ra do những rủi ro mang tính chất thảm họa như: động đất, núi lửa, sóng thần, nhiễm phóng xạ, chiến tranh, nội chiến, khủng bố, đình công, bạo động, dân biến, phiến loạn các hoạt động dân sự hoặc hành động của bất kỳ người cầm đầu của tổ chức nào nhằm lật đổ, đe dọa chính quyền, kiểm soát bằng vũ lực;&nbsp;</p>\n<p>- Người được Bảo hiểm mất tích (trừ trường hợp tại Quyết định, Bản án có hiệu lực pháp luật, Tòa án có kết luận chính thức về nguyên nhân Người được Bảo hiểm mất tích do tai nạn xảy ra trong thời hạn Bảo hiểm);</p>\n<p>- Người được Bảo hiểm đột tử hoặc tử vong không rõ nguyên nhân hoặc bị tử vong mà không thể xác định được nguyên nhân tử vong vì bất kỳ lý do nào.</p>\n",
      "<p>Passengers purchasing all kind of tickets will NOT be entitled to Bay An Toan insurance benefits in the following cases:&nbsp;</p>\n<p>-\tWillful behavior of the Insured or the Beneficiary; In case the insurance contract has many Beneficiaries, only the Beneficiary who has violated the act will be refused payment of indemnity. HDI is still responsible for the insurance payment to the remaining Beneficiaries.</p>\n<p>-\tThe Insured violates the law on the following acts:&nbsp;</p>\n<ul>\n<li>&nbsp;Illegal racing;&nbsp;&nbsp;</li>\n<li>&nbsp;Operating a vehicle without an invalid Driver's License / Illegal Driver's License;&nbsp;&nbsp;</li>\n<li>&nbsp;Entering restricted roads or restricted areas;&nbsp;&nbsp;</li>\n<li>&nbsp;Going in the opposite direction of a one-way street, going in the opposite direction on a road with a signboard forbidden to go in the opposite direction;&nbsp;&nbsp;</li>\n<li>&nbsp;Not going to the right lane, road section;&nbsp;</li>\n</ul>\n<p>-\tThe Insured uses alcohol in excess of the concentration prescribed by the Law on Road Traffic;</p>\n<p>-\tThe Insured uses drugs or other stimulants;&nbsp;</p>\n<p>-\tThe Insured treats or uses drugs not under the direction of the medical facility / treating doctor (unless the doctor self-medicates for the disease in accordance with his specialty);</p>\n<p>-\tThe insured participates in professional sports activities, sports including: bicycle racing, motorbike racing, horse racing, soccer, boxing, rock climbing, windsurfing, racing, surveying explorers. The insured participates in military drills, military training, combat participation by the armed forces, and aviation activities (except as a ticketed passenger);</p>\n<p>-\tThe Insured participates in a fight unless it is concluded / determined by a competent authority to be a legitimate defense;&nbsp;</p>\n<p>-\tThe Insured suffers from food and drink poisoning or inhalation of noxious vapors, poison gas or poison;&nbsp;</p>\n<p>-\tExpenses for the provision, maintenance or assembly of repair of prosthetic or prosthetic devices, orthopedic appliances, hearing or vision aids, crutches or wheelchairs or other devices such as disc, fasteners, nails, screws, anchor bolts, etc;&nbsp;&nbsp;</p>\n<p>-\tWhere the Insured suffers bodily injury or death due to catastrophic risks such as earthquake, volcano, tsunami, radioactive contamination, war, civil war, terrorism, strikes, riot, civil commotion, rebellion or civil action or action of any leader of any organization to overthrow, threaten the government, control by force;&nbsp;</p>\n<p>-\tThe Insured is missing (except for the case in the legally effective Decision or judgment, the Court has an official conclusion on the cause of the Insured's disappearance due to an accident occurring within the insurance period);</p>\n<p>-\tThe Insured dies suddenly or dies of unknown cause or dies without identifying the cause of death for any reason.</p>\n"
   ],
   "outpth0e":[
      "Địa điểm xảy ra tổn thất",
      "Location of Loss"
   ],
   "jmnklciv":[
      "Mô tả ngắn gọn về sự cố",
      "Brief description of the incident"
   ],
   "zaimdjsr":[
      "Ngày nhận hành lý",
      "Baggage receiving date"
   ],
   "nipbig36":[
      "<p>-   Quý Khách phải cung cấp hình chụp bản gốc các giấy tờ sau:</p>\n<p>+  Bản chính hoặc bản sao “Vé điện tử và xác nhận hành trình”;</p>\n<p>+  Bản chính hoặc bản sao Thẻ lên tàu bay;</p>\n<p>+  Bản sao Chứng minh nhân dân/Thẻ căn cước/Passport;</p>\n<p>+  Bản sao Giấy ra viện (nếu có);</p>\n<p>+  Bản chính Giấy yêu cầu bồi thường;</p>\n<p>+  Phiếu xác nhận hoàn thành cách ly y tế tập trung (thể hiện thời gian vào và ra khỏi khu cách ly) và các giấy tờ liên quan khác.</p>\n<p>+  Các chứng từ khác theo yêu cầu của HDI tùy trường hợp cụ thể.</p>\n<p>-   Trường hợp nhận tiền thay cho Người được Bảo hiểm dưới 18 tuổi, người nhận tiền phải cung cấp giấy tờ chứng minh quan hệ với Người được Bảo hiểm như: bản sao sổ hộ khẩu hoặc giấy khai sinh, giấy tờ chứng minh quyền giám hộ hoặc các giấy tờ khác theo quy định của pháp luật.</p>\n<p>-   Trường hợp Người được Bảo hiểm trên 18 tuổi và được người khác nhận tiền thay, người nhận tiền phải cung cấp giấy tờ chứng minh quyền thừa kế/ giấy ủy quyền công chứng hoặc được xác nhận bởi UBND cấp Phường/Xã trở lên hoặc các giấy tờ khác theo quy định của pháp luât.&nbsp;</p>\n<p>-    Các thông tin khác: email, số điện thoại (nếu có).</p>\n",
      "<p>-   Passengers must provide a photo of original documents as following:</p>\n<p>+  Original document or attested copy of “E-ticket”;</p>\n<p>+  Original document or attested copy of “Boarding Pass”;</p>\n<p>+  Notarized copy of Citizen Card/Identity Card/Passport;</p>\n<p>+  Medical document: Hospital discharge paper (If any);</p>\n<p>+  Claim form (HDI form);</p>\n<p>+  Certificate of completion of medical isolation (including date of quarantine start and date of quarantine end) and other relevant documents.</p>\n<p>+  Other documents required by HDI depending on the specific case.</p>\n<p>-   If the Insured under the age of 18 authorizes another person to make a claim, the Insured must provide documents proving the with the Insured Person such as: attested copy of household book, or birth certificate, documents proving guardiancy or other documents as prescribed by law.&nbsp;</p>\n<p>-    If the Insured over the age of 18 authorizes another person to make a claim, the Insured must provide documents proving the right to inheritance/ attested copy of “Valid Authorized Letter” or be certified by the People's Committee of Ward/Commune or higher or other documents as prescribed by law.</p>\n<p>-    Other information: email, phone number (if any).</p>\n<p></p>\n"
   ],
   "gihxcq1u":[
      "THÔNG TIN",
      "INFORMATION"
   ],
   "8jrol4t4":[
      "Tên người được bảo hiểm",
      "Name of the Insured"
   ],
   "mxockjz4":[
      "CMND/ CCCD/ Hộ Chiếu",
      "ID Number"
   ],
   "uiuzuvhv":[
      "Tên ngân hàng",
      "Bank name"
   ],
   "jsmwdctb":[
      "Ghi chú",
      "Notes"
   ],
   "isv8xlds":[
      "<p>Là sự kiện khách quan xảy ra trong Thời Hạn Bảo Hiểm mà HDI phải trả tiền bảo hiểm cho Người thụ hưởng hoặc Người được bảo hiểm.</p>\n<p><strong>-    Đối với quyền lợi A: </strong>Sự kiện bảo hiểm phát sinh khi xảy ra Tai nạn dẫn đến Người được bảo hiểm tử vong hoặc bị thương tật toàn bộ vĩnh viễn và Thương tật tạm thời.&nbsp;</p>\n<p><strong>-     Đối với quyền lợi B:</strong> Sự kiện bảo hiểm phát sinh khi Người được bảo hiểm bị cách ly tập trung (do nghi nhiễm hoặc phải điều trị do Dịch bệnh) theo yêu cầu của cơ quan có thẩm quyền vì có hành trình dịch tễ di chuyển trên Chuyến bay của VietJet theo yêu cầu của cơ quan có thẩm quyền và được thông báo bằng tất cả các hình thức như thông báo qua văn bản, công bố báo chí thông tin người cách ly, …..</p>\n<p></p>\n",
      "<p>Is the unforeseen event occur during the Insured Period that HDI has to pay indemnity to the Beneficiary or the Insured.</p>\n<p><strong>-          For benefit A:</strong> Insured event occurs while Accident happened that lead the Insured to death or to permanently total disablement and Temporary disablement.</p>\n<p><strong>-          For benefit B: </strong>Insured event occurs while the Insured is quarantined by disease (due to suspicion of infection or treatment by Disease) required by the authority because of the medical journey includes the Flight of Vietjet and is publicly notified by official documents, official news reports,…</p>\n"
   ],
   "quhgsesz":[
      "<p>Ngoài các giấy tờ chung, khách hàng cần cung cấp thêm các giấy tờ sau:</p>\n<p>- Trường hợp trả tiền trợ cấp trong thời gian cách ly tập trung do Dịch bệnh, NĐBH phải cung cấp thông báo của cơ quan chức năng ra quyết định cách ly tập trung, giấy xác nhận hoàn thành cách ly của cơ quan có thẩm quyền và các giấy tờ liên quan khác.</p>\n",
      "<p>Other than the general documents, customer must provide additional documents include:</p>\n<p>&nbsp;<span style=\"font-family: Symbol;\">-</span> Certificate of completing quarantine issued by authority or other relevant documents.&nbsp;&nbsp;</p>\n"
   ],
   "crtqbcnx":[
      "<p>Đối với hành lý và các vật dụng bên trong:&nbsp;</p>\n<p>-\tGiá trị tiền mặt ban đầu của các hành lý hoặc vật dụng trừ đi giá trị khấu hao; hoặc&nbsp;</p>\n<p>-\tChi phí thay thế bằng một hành lý hoặc vật dụng có nhãn hiệu hoặc mẫu mã tương tự; hoặc&nbsp;</p>\n<p>-\tChi phí sửa chữa các hành lý hoặc vật dụng đó do quyết định của Bảo hiểm HD; hoặc&nbsp;</p>\n<p>-\tNếu hành lý ký gửi không có dấu hiệu bị cạy phá, Bảo hiểm HD sẽ bồi thường theo cân nặng hành lý theo ký như sau:&nbsp;</p>\n<div style=\"text-align: center;\">\n<img src=\"/img/vietjet/boi_thuong_VNI.png\" alt=\"\" style=\"height: ;width: 90%\"/>\n</div>\n<p>-\tViệc mất mát, thiệt hại phải được trình báo sớm nhất có thể và có Báo cáo bất thường (PIR) xác nhận của nhân viên tại quầy hành lý thất lạc trước khi mang ra khỏi khu vực nhận hành lý&nbsp;</p>\n<p></p>\n",
      "<p>For the luggage and items inside:&nbsp;</p>\n<p>-\tThe original cash value of the luggage and items less depreciation; or</p>\n<p>-\tThe cost of replacement of a similar make and model; or</p>\n<p>-\tThe cost of repair of the items as determined by HD Insurance in its sole discretion; or&nbsp;</p>\n<p>-\tIf the Checked Baggage does not have any sign of forced open, HD Insurance will indemnify based on the weight of baggage by kilogram:</p>\n<div style=\"text-align: center;\">\n<img src=\"/img/vietjet/boi_thuong_ENG.png\" alt=\"\" style=\"height: ;width: 90%\"/>\n</div>\n<p>-\tAny loss or damage must be reported as soon as possible and has to have confirmed Property Irregularity Report (PIR) by the staff at Lost &amp; Found counter before transporting baggage outside of baggage claim area</p>\n<p></p>\n"
   ],
   "gb61crtu":[
      "Xem thêm",
      "Extend"
   ],
   "mzjlvi9d":[
      "3.\tTrường hợp yêu cầu bồi thường thiệt hại/mất hành lý\n",
      "3.\tFor claim request of damaged or lost Checked Baggage\n"
   ],
   "azseyght":[
      "<p><span style=\"font-size: 12pt;\"><strong>Đối với quyền lợi A:</strong> bắt đầu từ giờ khởi hành thực tế của Chuyến bay cho đến hết 24 giờ kể từ giờ khởi hành thực tế của Chuyến bay.</span>&nbsp;</p>\n<p><span style=\"font-size: 12pt;\"><strong>Đối với quyền lợi B:</strong> bắt đầu từ giờ khởi hành thực tế của Chuyến bay</span> <span style=\"font-size: 12pt;\">cho đến hết 30 ngày kể từ giờ khởi hành thực tế của Chuyến bay.</span>&nbsp;</p>\n",
      "<p><strong>For benefit A:</strong> starts from the actual departure time of the Flight till full 24 hours from the actual departure time of the Flight.</p>\n<p><strong>For benefit B: </strong>from the actual departure time of the Flight to full 30 days after the actual departure time of the Flight.</p>\n"
   ],
   "24m1ysco":[
      "Đóng",
      "Close"
   ],
   "1ttvjzct":[
      "Các hoá đơn theo QĐ pháp luật chứng minh giá trị hành lý, tài sản",
      "Invoice/receipt following regulations of TAX in order to prove the baggage, property value "
   ],
   "sfz8vnks":[
      "Email nhận hoá đơn",
      "Email to receive invoice"
   ],
   "ilb2uglv":[
      "6.\tTôi có thể mua nhiều gói bảo hiểm hành lý được không? ",
      "6.\tCan I purchase multiple packages?"
   ],
   "srnb3rzo":[
      "Chương trình Bảo hiểm Hành Lý được Bảo hiểm HD thiết kế đặc biệt cho hành khách của VietJet với các quyền lợi bảo hiểm vượt trội khi ký gửi hành lý.",
      "The Baggage insurance program is specially designed by HD Insurance for all VietJet’s passengers with outstanding benefits for checked baggage."
   ],
   "ebdbrgpb":[
      "Chọn sản phẩm để tra cứu",
      "Choose an insurance product"
   ],
   "5k1jw9e3":[
      "1.\tChương trình bảo hiểm hành lý là gì?",
      "1.\tWhat is the Baggage insurance program?"
   ],
   "yjqvrjgv":[
      "8.\tTôi có thể kiểm tra việc hoàn tất mua bảo hiểm như thế nào?",
      "8.\tHow can I check if my purchase has been complete?"
   ],
   "npajtefs":[
      "Không. Bạn sẽ không được hưởng quyền lợi bảo hiểm nếu như bạn không thực hiện Chuyến bay.",
      "Your baggage will not get insured if you do not make the Flight."
   ],
   "vemktzma":[
      "<p>Chương trình Bảo hiểm Hành Lý dựa trên Quy tắc Bảo hiểm do Bảo hiểm HD ban hành như sau:<br>-\tQuy tắc bảo hiểm Hành Lý (<a href=\"https://hyperservices.hdinsurance.com.vn/f/ed530d77dad1a1a98d5d285c9813f970?download=true\" target=\"_blank\">tải file</a>)</p>\n",
      "<p>The Baggage Insurance Program is based on the Policy Wording issued by HD Insurance below:<br>-\tPromulgation of the Baggage Insurance Wordings (<a href=\"https://hyperservices.hdinsurance.com.vn/f/8aed0c290d182bc2bee43bb2000ec169?download=true\" target=\"_blank\">download</a>)</p>\n"
   ],
   "8zgtxtpe":[
      "Bồi thường",
      "Claim"
   ],
   "add_file":[
      "Thêm File",
      "Upload file"
   ],
   "dlijtrc2":[
      "<p>Thời hạn giải quyết bồi thường là 15 (mười lăm) ngày làm việc kể từ ngày nhận hồ sơ yêu cầu bồi thường đầy đủ, hợp lệ từ phía Người được Bảo hiểm. Trường hợp Quý khách gửi hồ sơ yêu cầu bồi thường qua dịch vụ chuyển phát hoặc bưu điện, thời hạn giải quyết bồi thường được tính từ ngày nhận được hồ sơ của Quý khách từ đơn vị dịch vụ chuyển phát hoặc bưu điện. (dựa theo thông tin lưu trữ của bưu điện)</p>",
      "<p>Time for process of compensation is 15 (fifteenth) working days from the date that the full compensation file is received. In case you send your claim documents via a courier or post, the settlement period limit for compensation is calculated from the date received your file from the courier or post. (according to the post office’s recorded)</p>"
   ],
   "5ovni0hi":[
      "<p>Trường hợp Quý khách bị thay đổi Chuyến bay không mong muốn và được Vietjet xác nhận đã đổi chuyến, Quý Khách được hưởng quyền lợi theo Chuyến bay mới.</p>",
      "<p>In case you have an unexpectedly changed flight which is confirmed by VietJet within the period of insurance, you are entitled to the benefits of the new flight.</p>"
   ],
   "rrpmdpgc":[
      "<p>- Quý khách vẫn được tham gia chương trình Bảo hiểm Bay An toàn ngay cả khi Quý khách không phải là người Việt Nam và/hoặc không sinh sống tại Việt Nam, nếu Quý khách sử dụng dịch vụ vận chuyển hành khách trên các Chuyến bay nội địa do VietJet khai thác. </p><p>- Quý khách có thể làm thủ tục nộp hồ sơ bồi thường Bảo hiểm trực tuyến bằng cách truy cập tại trang thông tin điện tử: <a href=\"https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi\">https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi</a> và làm theo các bước sau:</p><ul>  <li><p>Bước 1: Tra cứu Giấy chứng nhận Bảo hiểm của Quý khách bằng cách điền các thông tin liên quan của Chuyến bay.</p>  </li>  <li><p>Bước 2: Quý khách lựa chọn “Tạo yêu cầu bồi thường” và cung cấp thông tin thủ tục bồi thường theo màn hình hướng dẫn.</p></li>    </ul>",
      "<p>-\tPassengers can still join Fly Safe insurance program even if Passengers are not Vietnamese and/or do not live in Vietnam, if Passengers use ticket on domestic flights operated by VietJet.</p><p>-\tPassengers may make a claim by accessing website: <a href=\"https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi\">https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi</a> and follows:</p><ul>  <li><p>\tStep 1: Search your Insurance Certificate by fill in the information related to the flight.</p>  </li>  <li><p>\tStep 2: Select “Request Claim” and provide the requested information of compensation file displayed on the screen.</p></li>    </ul>"
   ],
   "fdjhx4bj":[
      "Nơi đi - Nơi đến",
      "Departure - Arrival"
   ],
   "spckrdbz":[
      "Số Điện thoại",
      "Tel No"
   ],
   "zofx5xfl":[
      "Số tài khoản",
      "Account No"
   ],
   "9e43wr14":[
      "Khai hộ",
      "Declaration"
   ],
   "mccqlikz":[
      "Tên cơ sở y tế",
      "Name of Hospital"
   ],
   "wjtx7h1c":[
      "2. Thời hạn bảo hiểm\n",
      "2. Insured Period\n"
   ],
   "lchcq2na":[
      "Điều Kiện, Điều Khoản",
      "Insurance conditions"
   ],
   "tuqjexpy":[
      "<p>Chương trình Bảo hiểm Bay An Toàn dựa trên các Quy tắc Bảo hiểm do HDI ban hành như sau:</p>\n<p>- Quy tắc Bảo hiểm Chăm sóc sức khỏe và tai nạn <a href=\"https://hyperservices.hdinsurance.com.vn/f/19e6063ef221a78aaf652aeea584ac21\" target=\"_blank\">(tải file)</a>&nbsp;</p>\n<p>- Quy tắc Bảo hiểm Tai nạn con người <a href=\"https://hyperservices.hdinsurance.com.vn/f/419e6063ef221a78aaf652aeea584ac2\" target=\"_blank\">(tải file)</a>&nbsp;</p>\n<p>- Điều khoản Bảo hiểm mất giảm chi phí sinh hoạt, mất giảm thu nhập do bị cách ly bởi dịch bệnh <a href=\"https://hyperservices.hdinsurance.com.vn/f/a419e6063ef221a78aaf652aeea584ac\" target=\"_blank\">(tải file)</a>&nbsp;</p>\n",
      "<p>Fly Safe Insurance Program is based on Insurance Wordings issued by HDI including:</p>\n<p>-          Promulgation of the Healthcare and Personnal Accident Insurance Wordings <a href=\"https://hyperservices.hdinsurance.com.vn/f/19e6063ef221a78aaf652aeea584ac21\" target=\"_blank\">(download)</a>&nbsp;</p>\n<p>-          Promulgation of the Personal Accident Insurance Wordings <a href=\"https://hyperservices.hdinsurance.com.vn/f/419e6063ef221a78aaf652aeea584ac2\" target=\"_blank\">(download)</a>&nbsp;&nbsp;</p>\n<p>-          Điều khoản Bảo hiểm mất giảm chi phí sinh hoạt, mất giảm thu nhập do bị cách ly bởi dịch bệnh <a href=\"https://hyperservices.hdinsurance.com.vn/f/a419e6063ef221a78aaf652aeea584ac\" target=\"_blank\">(tải file)</a>&nbsp;&nbsp;</p>\n"
   ],
   "rjdk72xg":[
      "3. Sự kiện bảo hiểm",
      "3. Insurance events"
   ],
   "hgter7ur":[
      "Trong trường hợp thay đổi chuyến bay, vui lòng nhập lại các thông tin sau:",
      "In case of flight change, please re-input these following informations:"
   ],
   "tqb0ktct":[
      "Yêu cầu hoá đơn ",
      "Request An Invoice"
   ],
   "6hp2if8d":[
      "Hư hỏng hành lý",
      "Damage of baggage"
   ],
   "kf4qcgfu":[
      "Giờ nhận hành lý",
      "Baggage receiving time"
   ],
   "4epykayo":[
      "<div style=\"text-align: center;\">\n<p><strong>QUY TRÌNH BỒI THƯỜNG</strong></p>\n<img src=\"/img/vietjet/faq1.png\"  alt=\"\" style=\"height: ;width: 100%\"/>\n</div>\n",
      "<div style=\"text-align: center;\">\n<p><strong>CLAIM PROCESS</strong></p>\n<img src=\"/img/vietjet/faq2.png\"  alt=\"\" style=\"height: ;width: 100%\"/>\n</div>\n"
   ],
   "i4fmaro2":[
      "<p>Ngoài các giấy tờ chung, khách hàng cần cung cấp thêm các giấy tờ sau:</p>\n<p>-\tBản chính Báo cáo bất thường về hành lý (PIR) từ Hãng hàng không liên quan đến hành lý thất lạc/mất/hư hỏng bao gồm thông tin chi tiết về Chuyến bay theo lịch trình và/hoặc thông tin chi tiết bằng văn bản xác nhận về việc hành lý thất lạc/mất/hư hỏng,</p>\n<p>-\tCác hoá đơn (bao gồm hóa đơn điện tử) theo quy định pháp luật và/hoặc các loại chứng từ chứng minh giá trị của hành lý, tài sản,</p>\n<p>-\tCác hoá đơn (bao gồm hóa đơn điện tử) về việc sửa chữa hành lý, tài sản,</p>\n<p>-\tCác loại chứng từ khác...</p>\n",
      "<p>Other than the general documents, customer must provide additional documents include:</p>\n<p>-\tOriginal copy of Property Irregularity Report (PIR) from the Airlines relating to the damage/lost including details of the Flight according to the flight journey and/or any other document with details of the damage/lost of baggage,</p>\n<p>-\tReceipt(s) (including e-invoice) by the law regulation and/or proof of luggage and items’ value document(s),</p>\n<p>-\tReceipt(s) (including e-invoice) of repairing luggage and items,</p>\n<p>-\tOther documents …</p>\n"
   ],
   "dgo1hnkg":[
      "Vé điện tử/Email xác nhận hành trình hoặc thẻ tàu bay, Ảnh thẻ hành lý",
      "Digital Air ticket/ Email for route confirmation or boarding pass, picture of baggage tag"
   ],
   "r1icccon":[
      "Các hoá đơn về việc sửa chữa hành lý, tài sản",
      "Invoice/ receipt of baggage, property repairing"
   ],
   "4salp6p0":[
      "Địa chỉ xuất hoá đơn",
      "Address"
   ],
   "fydldq2v":[
      "11.\tTôi có thể hủy bỏ giấy chứng nhận bảo hiểm hành lý của tôi không?",
      "11.\tCan I cancel my insurance certificate?"
   ],
   "3tyh8urq":[
      "3.\tQuyền lợi bảo hiểm của tôi theo Chương trình bảo hiểm hành lý là gì?",
      "3.\tWhich are my benefits for the Baggage insurance program?"
   ],
   "l3qscdhs":[
      "THÔNG TIN BẢO HIỂM HÀNH LÝ",
      "BAGGAGE INSURANCE INFORMATION"
   ],
   "skycareTitle":[
      "THÔNG TIN BẢO HIỂM DU LỊCH SKY CARE",
      "SKY CARE INSURANCE INFORMATION"
   ],
   "skycaredomTitle":[
      "THÔNG TIN BẢO HIỂM DU LỊCH SKY CARE NỘI ĐỊA",
      "SKY CARE INSURANCE INFORMATION"
   ],
   "e0hjuade":[
      "<p>Là chương trình được Công ty TNHH Bảo hiểm HD (Bảo hiểm HD) phối hợp thực hiện cùng Công ty cổ phần Hàng không VietJet (VietJet) nhằm mang đến cho khách hàng trải nghiệm an tâm ký gửi hành lý khi bay cùng VietJet.</p>\n",
      "<p>Is the program collaboratively created by HD Insurance and VietJet with the aim to bring you assurance when having you baggage checked-in while flying with VietJet.</p>\n"
   ],
   "f9e1smfd":[
      "2.\tBảo hiểm hành lý của tôi được cung cấp bởi đơn vị nào?",
      "2.\tWhich is the insurer for this Baggage insurance program?"
   ],
   "yiinsih7":[
      "Mọi thông tin thắc mắc xin liên hệ theo hotline:",
      "For any questions, please contact our hotline:"
   ],
   "qwfjwbbq":[
      "5.\tTôi có thể mua bảo hiểm hành lý bằng hình thức nào?",
      "5.\tHow can I purchase the Baggage insurance?"
   ],
   "r1xakszs":[
      "<p>Bạn có thể thanh toán bằng tiền mặt, QR, thẻ tín dụng hoặc thẻ thanh toán khác.</p>\n<p>Ngay sau khi hoàn tất thanh toán, Bảo hiểm HD sẽ gửi bạn tin nhắn và thư điện tử xác nhận việc thanh toán thành công đồng thời gửi bạn đường dẫn đến Giấy Chứng Nhận Bảo Hiểm.</p>\n<p>Khách hàng sẽ truy cập vào đường dẫn để tải về Giấy Chứng Nhận Bảo Hiểm và yêu cầu xuất hóa đơn.</p>\n",
      "<p>You can pay by cash, QR code, credit card or other payment card.</p>\n<p>Once the purchasing is completed, HD Insurance will send you SMS and email to confirm your successful purchase with the link to retrieve your Insurance Certificate and make request to receive e-invoice</p>\n"
   ],
   "zsqrwdgt":[
      "10.\tNếu tôi không thực hiện Chuyến bay, tôi có được hưởng quyền lợi bảo hiểm hành lý hay không?",
      "10.\tIf I do not make the Flight, will my baggage get insured?"
   ],
   "2fl74q88":[
      "<p>Bảo hiểm HD sẽ không bồi thường cho thiệt hại hành lý của NĐBH trong các trường hợp dưới đây:&nbsp;</p>\n<p>-\tHợp đồng bảo hiểm này không bảo hiểm cho:</p>\n<p>a) \tđộng vật;</p>\n<p>b)\thàng lậu hoặc hàng cấm theo Điều lệ của Hãng Hàng không;</p>\n<p>c)\tgiấy tờ hoặc giấy căn cước hoặc giấy tờ khác (có giá hoặc không có giá);</p>\n<p>d)\tthẻ tín dụng, và thẻ thanh toán;</p>\n<p>e)\tvé vận tải, tiền mặt, cổ phiếu, và chứng khoán, trang sức, đá quý;</p>\n<p>f)\tkính mắt, kính áp tròng, thiết bị trợ thính, chân giả, răng giả, cầu răng giả;</p>\n<p>g)\tchìa khóa hoặc thiết bị khác dùng để mở các loại khóa;</p>\n<p>h)\tnhạc cụ, dụng cụ nghệ thuật, đồ cổ, vật sưu tầm, hoặc đồ đạc sử dụng trong sinh hoạt gia đình nhà,; hàng mau hỏng, dễ vỡ và  tranh ảnh (trừ trường hợp tranh ảnh bị mất);</p>\n<p>i)\tcác loại thức ăn, đồ uống, hóa chất;</p>\n<p>-\tBảo hiểm HD sẽ không chịu trách nhiệm trước bất cứ thiệt hại nào phát sinh từ hoặc gây ra bởi hoặc liên quan đến;</p>\n<p>a)\tCác vật phẩm dễ vỡ hoặc dễ gãy, nhạc cụ, ra-đi-ô và tài sản tương tự bị nứt vỡ, gãy;</p>\n<p>b)\tTịch thu hoặc sung công theo lệnh của bất cứ chính phủ hoặc cơ quan chính quyền nào;</p>\n<p>c)\tCôn trùng hoặc sâu bọ;</p>\n<p>d)\tHư hỏng hoặc khiếm khuyết vốn có;</p>\n<p>e)\tNhững hao mòn và rách vỡ một cách hợp lý của hành lý gây ra bởi sự di chuyển và va đập thông thường khi vận chuyển bằng đường hàng không;</p>\n<p>f)\tVận chuyển hàng lậu hoặc hàng cấm;&nbsp;</p>\n<p>g)\tTịch thu cho dù để tiêu hủy theo quy chế, quy định, quyết định kiểm dịch hoặc quy chế hải quan hay không;</p>\n<p>h)\tBất cứ giao dịch thanh toán nào có thể vi phạm các luật, lệnh cấm hoặc quy định của chính phủ;</p>\n<p>i)\tCản trở, bắt giữ, tịch thu, tiêu hủy, trưng dụng, lưu giữ hoặc giam giữ bởi cơ quan hải quan hoặc bất cứ chính phủ hoặc cơ quan hoặc quan chức chính phủ nào khác;</p>\n<p>j)\tNĐBH có bất kỳ hành vi vi phạm pháp luật hoặc chống cự khi bị cơ quan có thẩm quyền bắt giữ;</p>\n<p>k)\tTrong khi NĐBH tham gia ẩu đả hoặc tham gia kích động ẩu đả;</p>\n<p>l)\tNĐBH không thực hiện các biện pháp hợp lý để bảo vệ, bảo quản hoặc tìm lại hành lý thất lạc;</p>\n<p>m)\tNĐBH không thông báo và không có biên bản xác nhận của nhân viên quầy hành lý thất lạc;</p>\n<p>n)\tCác đồ vật không được hãng hàng không chấp nhận là Hành lý ký gửi và vận chuyển.</p>\n<p>-\tBất cứ thiệt hại hoặc bất cứ nghĩa vụ pháp lý nào gặp phải hoặc xảy ra trực tiếp hoặc gián tiếp bởi NĐBH nếu NĐBH là:</p>\n<p>a)\tngười tham gia khủng bố;</p>\n<p>b)\tthành viên của một tổ chức khủng bố;</p>\n<p>c)\tngười cung cấp vũ khí hạt nhân, hóa học hoặc sinh học; hoặc</p>\n<p>d)\tngười buôn bán ma túy.</p>\n",
      "<p>HD Insurance is not liable for the damage of the Insured’s baggage:</p>\n<p>-\tThis insurance contract is not insured for:</p>\n<p>a) \tanimal;</p>\n<p>b)\tcontraband or illegal goods by the Terms &amp; Conditions of the Airlines;</p>\n<p>c)\tidentification documents or other documents (can be exchanged for money or cannot be exchanged for money);</p>\n<p>d)\tcredit cards, and payment cards;</p>\n<p>e)\ttransportation ticket, money, stock, and security certificates, jewelry, precious stones;</p>\n<p>f)\teye glasses, contact lenses, hearing aids, prosthetic limbs, artificial teeth or dental bridges;&nbsp;</p>\n<p>g)\tkeys or other equipment to open locks;</p>\n<p>h)\tmusical instruments, objects of art, antiques, collector’s items or, furniture; perishables and consumables, paintings and pictures (not applicable for lost case);</p>\n<p>i)\tall types of food, drink and chemicals;</p>\n<p>-\tHD Insurance is not liable for any loss or damage resulting from or caused by or relating to:</p>\n<p>a)\tbreakage of brittle or fragile articles, cameras, musical instruments, radios and such similar property;</p>\n<p>b)\tconfiscation or expropriation by order of any government or public authority;</p>\n<p>c)\tinsects or vermin;</p>\n<p>d)\tinherent vice or damage;</p>\n<p>e)\twear and tear of baggage caused by normal movements and knocks during carriage by air.</p>\n<p>f)\ttransportation of contraband or illegal trade; or</p>\n<p>g)\tseizure whether for destruction under quarantine or custom regulation or not;</p>\n<p>h)\tany payment which would violate any law, government prohibition or regulation;</p>\n<p>i)\tdelay, seizure, confiscation, destruction, requisition, retention or detention by customs or any other government or public authority or official;</p>\n<p>j)\tthe Insured has any violation or attempt of violation of laws or resistance to arrest by appropriate authority;</p>\n<p>k)\tthe Insured is taking part in a brawl or taking part in inciting a brawl;</p>\n<p>l)\tfailure of the Insured to take reasonable measure to protect, save or recover lost luggage;</p>\n<p>m)\tfailure of the Insured Person to notify the relevant airline authorities of missing luggage at the scheduled destination point and to obtain a property irregularity report;</p>\n<p>n)\tAny item that is not accepted by the Airlines to be Checked baggage and to be transported.</p>\n<p>-\tAny loss, injury, damage or legal liability suffered or sustained directly or indirectly by an Insured Person if that Insured Person is:</p>\n<p>a)\ta terrorist;</p>\n<p>b)\ta member of a terrorist organization;</p>\n<p>c)\ta purveyor of nuclear, chemical or biological weapons; or</p>\n<p>d)\ta narcotics trafficker.</p>\n"
   ],
   "ygwol2hm":[
      "Cá nhân",
      "Personal"
   ],
   "qnrcbxjk":[
      "13.\tNếu tôi muốn thay đổi Chuyến bay, tôi có nhận được quyền lợi bảo hiểm?",
      "13.\tIf I change my Flight, will I receive the baggage insurance benefits?"
   ],
   "kkxgodli":[
      "<p>Giấy Chứng Nhận Bảo Hiểm vô hiệu trong các trường hợp sau:</p>\n<p>- Sai thông tin cá nhân nhưng không có bất kỳ chứng từ xác nhận thay đổi thông tin.</p>\n<p>- Vé điện tử và xác nhận hành trình hoặc Thẻ lên tàu bay của hành khách bị hủy và không được hoàn tiền Vé theo chính sách của Hãng hàng không. Hành khách sẽ không được hoàn Phí bảo hiểm trong trường hợp này.</p>\n<p>- Vé điện tử và xác nhận hành trình hoặc Thẻ lên tàu bay của hành khách bị hủy và được hoàn tiền Vé theo chính sách của Hãng hàng không. Hành khách sẽ được hoàn Phí bảo hiểm cùng với tiền Vé. Thủ tục hoàn Phí bảo hiểm sẽ do Hãng hàng không chịu trách nhiệm.</p>\n",
      "<p>Your Insurance Certificate will be invalid in the cases below:</p>\n<p>- E-ticket and flight confirmation or Boarding pass of passenger has been cancelled and not refunded the ticket fare following the policies of the Airline. Passenger will not be refunded the insurance’s premium in this case.</p>\n<p>- E-ticket and flight confirmation or Boarding pass of passenger has been cancelled and will be refunded the ticket fare following the policies of the Airline. Passenger will be refunded the insurance’s premium with the ticket fare. The insurance premium refunding procedure will be under the Airline’s responsibility.</p>\n"
   ],
   "rli98ydj":[
      "<p><strong>Quy trình yêu cầu bồi thường như sau:</strong></p>\n<p><strong><ins>Bước 1: </ins></strong>Tiếp nhận yêu cầu bồi thường</p>\n<p>1.1 Khi có sự cố xảy ra, bạn cần thông báo ngay cho Bảo hiểm HD theo số điện thoại tổng đài 1900 068 898  để được hướng dẫn chi tiết yêu cầu bồi thường.</p>\n<p>1.2 Bảo hiểm HD hướng dẫn khách hàng thu thập hồ sơ, chứng từ yêu cầu bồi thường và điền Giấy yêu cầu bồi thường theo mẫu Bảo hiểm HD \"<a href=\"https://hyperservices.hdinsurance.com.vn/f/7ff8a9006e44d4c775e3da72b2f6de4d?download=true\" target=\"_blank\">tại đây</a>\"</p>\n<p>1.3 Khách hàng thu thập hồ sơ, chứng từ yêu cầu bồi thường theo hướng dẫn và nộp hồ sơ về BẢO HIỂM HD, cụ thể như sau:&nbsp;</p>\n<p>- Cách 1: Gửi toàn bộ hồ sơ, chứng từ về cho Bảo hiểm HD theo địa chỉ:</p>\n<p>Ban Giám định Bồi thường của Bảo hiểm HD:</p>\n<p>Toà nhà Abacus, số 58 Nguyễn Đình Chiểu, Phường Đakao, Quận 1, Tp. HCM</p>\n<p>- Cách 2: Scan/ chụp hình hồ sơ, chứng từ theo hướng dẫn bồi thường (Claim guide) và upload lên hệ thống bồi thường của Bảo hiểm HD.</p>\n<p>Website: https://vietjetair.hdinsurance.com.vn&nbsp;</p>\n<p>1.4 Sau khi đã tiếp nhận Hồ sơ yêu cầu bồi thường, Bảo hiểm HD xem xét hồ sơ, chứng từ thu thập được và email xác nhận với Khách hàng tình trạng hồ sơ, chứng từ đã đầy đủ hoặc hướng dẫn Khách hàng bổ sung các chứng từ còn thiếu.&nbsp;</p>\n<p>Thời gian thực hiện: 01 ngày</p>\n<p><strong><ins>Bước 2:</ins></strong> Xác định số tiền bồi thường</p>\n<p>2.1 Trong vòng 02 (hai) ngày làm việc kể từ khi nhận được đầy đủ hồ sơ chứng từ, Bảo hiểm HD xác định số tiền bồi thường đồng thời gửi thông báo bằng văn bản/ email cho Khách hàng.</p>\n<p>2.2 Trường hợp Khách hàng đồng ý với số tiền bồi thường, chuyển sang bước tiếp theo (Thanh toán bồi thường).</p>\n<p>2.3 Trường hợp Khách hàng không đồng ý số tiền bồi thường, chuyển về lại Bước 2.1 và thông báo lại cho Khách hàng trong vòng 01 ngày làm việc. Khách hàng đồng ý với số tiền bồi thường chuyển sang bước tiếp theo (Thanh toán bồi thường).&nbsp;</p>\n<p><strong><ins>Bước 3: </ins></strong>Thanh toán số tiền bồi thường</p>\n<p>Sau khi nhận được xác nhận đồng ý của Khách hàng bằng văn bản/ Email, Bảo hiểm HD chuyển khoản số tiền bồi thường cho Khách hàng trong vòng 07 ngày làm việc.</p>\n",
      "<p><strong>The claim process is as follow:</strong></p>\n<p><strong><ins>Step 1: </ins></strong>Receiving claim request</p>\n<p>1.1 Right after having the incident, you must contact HD Insurance hotline 1900 068 898 for claim guidance.</p>\n<p>1.2 HD Insurance will instruct you to collect documents, proof for claim request and fulfil Claim form HD Insurance template “<a href=\"https://hyperservices.hdinsurance.com.vn/f/7ff8a9006e44d4c775e3da72b2f6de4d?download=true\" target=\"_blank\">here</a>”</p>\n<p>1.3 You will send all the documents, proof for claim request as instructed and send them to HD Insurance by 1 of 2 options:</p>\n<p>•Option 1: Send all documents, proof to HD Insurance address as:</p>\n<p>HD Insurance Claim Department:</p>\n<p>Abacus Tower, 58 Nguyễn Đình Chiểu street, Đakao ward, District 1, HCMC</p>\n<p>•Option 2: Scan/ take picture of the documents, proof as instructed in the Claim guide and upload to the Claim portal of HD Insurance at</p>\n<p>Website: https://vietjetair.hdinsurance.com.vn</p>\n<p>1.4 After receving the Claim request documents, HD Insurance will examine the collected documents and send email to you to inform the completion or instruct you to send missing documents.</p>\n<p>Time for action: 01 day</p>\n<p><strong><ins>Step 2: </ins></strong>Determine the indemnity amount</p>\n<p>2.1 Within 02 (two) working days from the date of receiving complete documents, HD Insurance will determine the indemnity amount and send you an official notice by paper/email.&nbsp;</p>\n<p>2.2 In case you agree with the indemnity amount, move the next step (Claim payment).</p>\n<p>2.3 In case you do not agree with the indemnity amount, move back to step 2.1 and re-inform you within 01 working day. Once you agree with the indemnity amount, move the next step (Claim payment).&nbsp;</p>\n<p><strong><ins>Step 3: </ins></strong>Claim payment</p>\n<p>After receiving your confirmation by paper/email, HD Insurance will make bank transfer to you within 07 working days.</p>\n<p></p>\n"
   ],
   "mkuoxqaq":[
      "Bà",
      "Bà"
   ],
   "im1es8jz":[
      "Chọn sân bay",
      "Choose Airport"
   ],
   "fvdwj5qt":[
      "Nơi đi",
      "Departure"
   ],
   "pzb04ijc":[
      "2.\tGiấy tờ chung\n",
      "2.\tGeneral Documents\n"
   ],
   "ndm6blwm":[
      "<p>Ngoài các giấy tờ chung, khách hàng cần cung cấp thêm các giấy tờ sau:</p>\n<p>-\tBản chính Thư gửi khách trong trường hợp hành lý về trễ (Letter to passenger of late delivery bag)</p>\n<p>-\tCác loại chứng từ khác...</p>\n<p>Bảo hiểm HD sẽ xử lý việc giải quyết bồi thường cho Bên mua bảo hiểm và/hoặc Người được bảo hiểm trong vòng 15 (mười lăm) ngày kể từ ngày nhận được Hồ sơ yêu cầu bồi thường đầy đủ và hợp lệ của Bên mua bảo hiểm và/hoặc Người được bảo hiểm.</p>\n",
      "<p>Other than the general documents, customer must provide additional documents include:</p>\n<p>-\tOriginal copy of Letter to passenger of late delivery bag</p>\n<p>-\tOther documents …</p>\n<p>HD Insurance will process with the claim for the Policyholder and/or the Insured within 15 (fifteen) days from the date receiving valid and complete Claim request documents from the Policy holder and/or the Insured.</p>\n"
   ],
   "dngyixbb":[
      "Trường hợp bồi thường thiệt hại/ mất hành lý",
      "Baggage damage claim/ Baggage delay claim"
   ],
   "doqsxnvv":[
      "Mã số thuế doanh nghiệp",
      "Tax Identification Number"
   ],
   "utn0uqla":[
      "Bản chính thư gửi khách trong trường hợp hành lý về trễ",
      "The original of the Letter to passenger of late delivery baggage"
   ],
   "yifmnaex":[
      "12.\tNếu tôi muốn thay đổi thông tin cá nhân, tôi có thể yêu cầu sửa đổi thông tin trên giấy chứng nhận bảo hiểm không?",
      "12.\tIf I want to change my personal information, can I request to change my Insurance Certificate?"
   ],
   "wmasbpu0":[
      "4.\tNhững điểm loại trừ của Chương trình bảo hiểm hành lý?",
      "4.\tWhat are the Exclusions for the Baggage insurance program?"
   ],
   "pbkrvb3t":[
      "Doanh nghiệp",
      "Company"
   ],
   "b4n1b8dr":[
      "Nếu bạn thay đổi Chuyến bay và không được hoàn lại tiền Vé theo chính sách của Hãng hàng không, đơn bảo hiểm hành lý theo Chuyến bay này sẽ bị vô hiệu, bạn sẽ không nhận được quyền lợi bảo hiểm. ",
      "If you change your Flight and does not get refunded for the ticket fare following the policies of the Airline, your Insurance Certificate for this Flight will be invalid, you will not receive the benefits."
   ],
   "dq2ajnj3":[
      "<p>Bạn có thể tải Giấy Chứng Nhận Bảo Hiểm điện tử từ email từ Bảo hiểm HD đã gửi hoặc tra cứu và tải từ cổng thông tin trong tin nhắn từ Bảo hiểm HD để chứng mình bạn đã mua bảo hiểm.</p>\n",
      "<p>You can download the Insurance Certificate from the email from HDI or retrieve from the portal shown in the SMS from HDI for the proof of purchase.</p>\n"
   ],
   "bruv0um8":[
      "Bảo hiểm HD sẽ xử lý việc giải quyết bồi thường cho Bên mua bảo hiểm và/hoặc Người được bảo hiểm trong vòng 15 (mười lăm) ngày kể từ ngày nhận được Hồ sơ yêu cầu bồi thường đầy đủ và hợp lệ của Bên mua bảo hiểm và/hoặc Người được bảo hiểm.",
      "HD Insurance will process your claim request within 15 (fifteen) days from the date of receiving complete documents."
   ],
   "o3hkocnz":[
      "Số hiệu chuyến bay thực tế",
      "Actual Flight No."
   ],
   "bes8ksll":[
      "<p>-  Là cá nhân đang cư trú tại Việt Nam hoặc không đang cư trú tại Việt Nam nhưng du lịch qua Việt Nam hoặc quá cảnh thông qua một sân bay tại Việt Nam, và&nbsp;</p>\n<p>-  Là hành khách có tên trên Vé, được Bảo hiểm HD cấp Giấy chứng nhận bảo hiểm cho chuyến bay theo lịch trình của hành khách.</p>\n<p></p>\n",
      "<p>-  Individual residing in Vietnam or not residing in Vietnam but travelling to Vietnam or transiting through airport in Vietnam, and&nbsp;</p>\n<p>-  Identified Passenger on Ticket, is HD Insurance issued Insurance Certificate for the flight according to the passenger’s flight journey.&nbsp;</p>\n<p></p>\n"
   ],
   "16jw6lii":[
      "Chậm hành lý",
      "Baggage delay"
   ],
   "whoiatbu":[
      "<p><strong>1. Thiệt hại, mất hành lý ký gửi:</strong></p>\n<p>Bảo hiểm HD sẽ bồi thường cho NĐBH do:&nbsp;</p>\n<p>a.\tTrộm cắp hoặc thiệt hại do trộm cắp hoặc bất cứ hành vi cố ý trộm cắp nào gây ra thiệt hại cho hành lý với điều kiện:&nbsp;</p>\n<p>-\tPhải có dấu hiệu cạy phá, biến dạng đối với vali hoặc vật chứa đựng hành lý, và có xác nhận của nhân viên quầy hành lý thất lạc tại sân bay nơi NĐBH xuống tàu bay, hoặc;&nbsp;</p>\n<p>-\tKhông có dấu hiệu bị cạy phá, biến dạng nhưng có sự giảm sút về khối lượng căn cứ theo điều lệ Vận chuyển của Hãng hàng không và có xác nhận của nhân viên quầy hành lý thất lạc.&nbsp;</p>\n<p>-\tCó Báo cáo bất thường về hành lý (PIR) của Quầy hành lý thất lạc.&nbsp;</p>\n<p>b.\tThiệt hại đối với hành lý của NĐBH (bao gồm vali, thùng đựng hành lý, hành lý ký gửi cũng                                                                như các đồ đạc bên trong) khi ký gửi và thiệt hại này được gây ra bởi Hãng hàng không.&nbsp;</p>\n<p><strong>2. Chậm trễ hành lý ký gửi:</strong></p>\n<p>Bảo hiểm HD sẽ bồi thường cho hành khách không thể nhận lại Hành lý kí gửi từ 24 tiếng kể từ giờ tàu bay đáp tại điểm đến trong hành trình bay và không phải thường trú cùng Tỉnh, Thành phố mà sân bay đến trực thuộc.&nbsp;</p>\n<p></p>\n",
      "<p><strong>1. Damaged or loss checked baggage:</strong></p>\n<p>HD Insurance will indemnify for the Insured for</p>\n<p>a.\tA theft or damage due to theft or any attempted theft of baggage in condition:</p>\n<p>-\tBaggage has sign of forced open, deformation of suitcase or baggage container and has confirmation of staff at Lost &amp; Found counter at the airport where the Insured finish flight journey, or;</p>\n<p>-\tBaggage has no sign of forced open, deformation but has weight loss according to the Terms and Conditions of the Airlines and has confirmation of staff at Lost &amp; Found counter.</p>\n<p>-\tProperty Irregularity Report (PIR) from Lost &amp; Found counter.</p>\n<p>b.\tDamage to the Insured’s baggage (including suitcase, baggage container, checked baggage and items inside the baggage) caused by the Airlines while the baggage is being checked and transported.&nbsp;</p>\n<p><strong>2. Delayed checked baggage:</strong></p>\n<p>HD Insurance will indemnify for passenger that cannot receive the checked baggage from after 24 hours from the arrival time at the final airport stated in the flight journey and the final airport is not in the same Province, City that the arrival airport located.</p>\n<p></p>\n"
   ],
   "phone_bt_vj":[
      "1900 068 898",
      "1900 068 898"
   ],
   "cybxgii0":[
      "4.\tTrường hợp yêu cầu bồi thường liên quan chậm trễ hành lý\n",
      "4.\tFor claim request of delayed Checked Baggage\n"
   ],
   "givy5qxz":[
      "Ảnh thẻ hành lý",
      "Picture of baggage tag"
   ],
   "tqqui0ui":[
      "Tên doanh nghiệp",
      "Company Name"
   ],
   "tmagdatv":[
      "Trường hợp bồi thường liên quan đến chậm trễ hành lý",
      "In case of claim regarding to baggage delay"
   ],
   "wtk6seid":[
      "Không. Bạn không thể hủy bỏ Giấy Chứng Nhận Bảo Hiểm sau khi bạn đã lựa chọn mua bảo hiểm này.",
      "No. You cannot cancel your insurance certificate once you have finished purchasing."
   ],
   "relgepqf":[
      "<p>Chương trình bảo hiểm hành lý được thiết kế với mục đích mang lại các quyền lợi tối ưu cho hành khách cùng với trải nghiệm đăng ký mua, thanh toán dễ dàng, nhanh chóng ngay tại sân bay.</p>\n<p>Nhân viên mặt đất của VietJet sẽ hỗ trợ bạn lựa chọn và mua gói bảo hiểm hành lý phù hợp trước khi check-in, trong quá trình check-in hoặc sau khi check-in tại các khu vực sau:</p>\n<p>- Quầy counter check-in</p>\n<p>- Các khu vực chờ trong sân bay</p>\n",
      "<p>The Baggage insurance program is design with the aim to bring you the optimum benefits with the effortless, easy and quick purchasing experience right at the airport.</p>\n<p>VietJet Ground officers will support you to choose and purchase suitable insurance package(s) before, while and after check-in time at the areas below:</p>\n<p>-\tCheck-in counter;</p>\n<p>-\tWaiting areas at the airport</p>\n"
   ],
   "zuz8cscq":[
      "<p>Chương trình bảo hiểm hành lý được thiết kế phù hợp các nhu cầu của hành khách với 2 gói:&nbsp;</p>\n<p>-\tBảo hiểm hành lý Tiêu chuẩn</p>\n<p>-\tBảo hiểm hành lý VIP</p>\n<p>Với mỗi gói, quyền lợi bảo hiểm sẽ bao gồm 2 quyền lợi chính bao gồm:</p>\n<p>-\tBảo hiểm cho hành lý ký gửi bị thiệt hại, mất</p>\n<p>-\tBảo hiểm cho hành lý ký gửi bị chậm trễ</p>\n<p>Để xem chi tiết về quyền lợi bảo hiểm, vui lòng truy cập thêm tại trang thông tin điện tử (website): <a href=\"http://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi\" target=\"_blank\">https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi</a>, phần \"Thông tin Bảo hiểm Hành Lý - Quyền lợi Bảo hiểm\".&nbsp;</p>\n<p></p>\n",
      "<p>The Baggage insurance program is designed to suit passengers’ needs with 2 packages:&nbsp;</p>\n<p>-\tHDI Standard baggage insurance</p>\n<p>-\tHDI VIP baggage insurance</p>\n<p>With each package, you get 2 main insurance benefits including:</p>\n<p>-\tInsurance for loss or damaged checked baggage</p>\n<p>-\tInsurance for delayed baggage</p>\n<p>To see more about the details of your benefits, please go to the website <a href=\"http://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi\" target=\"_blank\">vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi</a> and check out part “Baggage insurance information – Insu rance benefits”&nbsp;</p>\n<p></p>\n"
   ],
   "qbpkiusb":[
      "<p>Nhân viên mặt đất sẽ xác nhận với bạn thanh toán thành công và bạn sẽ nhận được tin nhắn và thư điện tử từ Bảo hiểm HD đồng nghĩa việc mua bảo hiểm đã hoàn tất.&nbsp;</p>\n",
      "<p>VietJet Ground officers will confirm with you if the insurance premium has been paid and you will receive SMS and email from HDI confirming your successful purchase.</p>\n"
   ],
   "c5siztek":[
      "Số GCN/Mã đặt chỗ theo GCN",
      "Certificate No/Reservation Code"
   ],
   "p8bqdq6o":[
      "16.\tTôi phải liên hệ ai khi xảy ra sự cố đối với hành lý của tôi? Và liên hệ ai để yêu cầu bồi thường?",
      "16.\tWhich personnel must I contact when I have incident with my baggage? Which personnel must I contact for claim?"
   ],
   "3yyiixmv":[
      "<p>Chương trình Bảo hiểm Hành Lý dựa trên Quy tắc Bảo hiểm do Bảo hiểm HD ban hành như sau:</p>\n<p>-\tQuy tắc bảo hiểm Hành Lý (<a href=\"https://hyperservices.hdinsurance.com.vn/f/ed530d77dad1a1a98d5d285c9813f970?download=true\" target=\"_blank\">tải file</a> )</p>\n",
      "<p>The Baggage Insurance Program is based on the Policy Wording issued by HD Insurance below:</p>\n<p>-\tPromulgation of the Baggage Insurance Wordings (<a href=\"https://hyperservices.hdinsurance.com.vn/f/8aed0c290d182bc2bee43bb2000ec169?download=true\" target=\"_blank\">download</a>)&nbsp;</p>\n"
   ],
   "lost_baggage_guide":[
      "https://hyperservices.hdinsurance.com.vn/f/ebff68766337c7be299ee96aa1005c30",
      "https://hyperservices.hdinsurance.com.vn/f/bff68766337c7be299ee96aa1005c308"
   ],
   "43u9nxge":[
      "Chọn quốc gia",
      "Select Country"
   ],
   "9lkrvpm6":[
      "21.\tHồ sơ bồi thường gồm những gì?",
      "21.\tWhat includes in the claim documents?"
   ],
   "jxx8jtjd":[
      "Bố",
      "Dad"
   ],
   "bycvqmd0":[
      "Bảo hiểm hành lý",
      "Baggage Insurance"
   ],
   "skycare":[
      "Bảo hiểm du lịch Sky Care",
      "Sky Care Insurance"
   ],
   "vt4etm1e":[
      "14.\tNếu tôi bị thay đổi Chuyến bay không mong muốn do VietJet, tôi có nhận được quyền lợi bảo hiểm?",
      "14.\tIf my Flight is changed due to VietJet, will I receive the baggage insurance benefits?"
   ],
   "3ydftfec":[
      "18.\tQuy trình yêu cầu bồi thường của chương trình bảo hiểm hành lý là như thế nào?",
      "18.\tWhat is the claim process?"
   ],
   "2l3axjxg":[
      "20.\tThời hạn nộp yêu cầu bồi thường? ",
      "20.\tDeadline to submit the claim request?"
   ],
   "at7alb76":[
      "Bố",
      "Father"
   ],
   "f75v7aj0":[
      "testt123",
      "testt123"
   ],
   "kc6sci4s":[
      "<p>Bạn hoàn toàn có thể mua nhiều gói bảo hiểm hành lý cho tất cả hành lý ký gửi.&nbsp;</p>\n<p>Đối với gói Bảo hiểm hành lý Tiêu chuẩn, quyền lợi bảo hiểm sẽ áp dụng cho tất cả hành lý ký gửi của hành khách.</p>\n<p>Đối với gói Bảo hiểm hành lý VIP, quyền lợi bảo hiểm sẽ áp dụng cho từng kiện hành lý mà bạn đăng ký mua bảo hiểm.</p>\n<p>Trong trường hợp bạn mua gói Bảo hiểm hành lý Tiêu chuẩn với 1 hoặc nhiều gói Bảo hiểm hành lý VIP, hành lý được bảo hiểm theo gói Bảo hiểm hành lý VIP sẽ chỉ được hưởng quyền lợi theo gói này.</p>\n",
      "<p>You can totally purchase multiple insurance packages for your checked baggage.</p>\n<p>For the Standard baggage insurance package, the benefits are applied for all of your checked baggage.</p>\n<p>For the VIP baggage insurance package, the benefits are applied for each baggage you purchased the insurance for.</p>\n<p>In case you purchase the Standard baggage insurance package with 1 or more VIP baggage insurance package, the baggage that is insured with the VIP baggage insurance will only get the benefits from this package.</p>\n"
   ],
   "essxsm2f":[
      "<p>Bạn cần phải thông báo về việc mất mát và thiệt hại cho nhân viên quầy hành lý thất lạc tại nới xảy ra sự cố sớm nhất có thể.</p>\n<p>Trong mọi trường hợp, Bảo hiểm HD sẽ không chịu trách nhiệm bồi thường khi bạn và hành lý rời khỏi khu vực nhận hành lý ký gửi (Baggage Claim Area).</p>\n<p>Bạn cần phải cung cấp các bằng chứng bằng văn bản cho Bảo hiểm HD trong vòng 30 (ba mươi) ngày kể từ ngày bị tổn thất. Việc không cung cấp các bằng chứng đó trong khoảng thời gian quy định sẽ không làm mất hiệu lực khiếu nại hoặc giảm giá trị bồi thường nếu bạn có lý do hợp lý để không cung cấp các bằng chứng đó sớm nhất có thể và trong mọi trường hợp không được quá 01 (một) năm tính từ thời điểm phải cung cấp bằng chứng trừ trường hợp mất năng lực pháp luật.</p>\n",
      "<p>You must inform about your loss/damage/delay to the Lost &amp; Found counter staff where the incident happened right after the incident.&nbsp;</p>\n<p>HD Insurance shall not be liable for any indemnity if you and your baggage had left the Baggage Claim Area.</p>\n<p>You must send all proof by hard document to HD Insurance within 30 (thirty) days from the date of the incident. Inability to providing the documents within the indicated time period will not affect your complaint capacity or decrease the indemnity amount if you have reasonable proof of delaying the providing of documents and in all cases must submit this proof not later than within 01 (one) year from the time of initial document submission unless in case of losing legal ability.</p>\n"
   ],
   "dyavb4nv":[
      "9.\tKhi nào đơn bảo hiểm của tôi có hiệu lực ?",
      "9.\tWhen will my insurance period start?"
   ],
   "s7vkwdev":[
      "khai báo bồi thường cho bản thân",
      "claim compensation for yourself"
   ],
   "xjvo0tch":[
      "<p>Ngay khi phát hiện sự cố đối với hành lý tại khu vực nhận hành lý, bạn cần đến quầy Hành lý thất lạc (Lost &amp; Found) tại sân bay để thông báo về sự cố và làm các thủ tục theo yêu cầu của nhân viên Hãng hàng không tại quầy. Sau đó, bạn vui lòng liên hệ hotline của Bảo hiểm HD 1900 068 898 để được hướng dẫn cụ thể về quy trình yêu cầu bồi thường.</p>\n",
      "<p>Immediately right after you have incident with your baggage, you must go to the Lost &amp; Found counter at the airport to report the incident and complete the procedures as required from the airport staff at the counter. After that, please contact  HD Insurance hotline 1900 068 898 for claim guidance.</p>\n"
   ],
   "d9fzwfti":[
      "1.\tCơ sở giải quyết bồi thường\n",
      "1.\tClaim basis\n"
   ],
   "pduv5bku":[
      "<p>Bảo hiểm HD sẽ không bồi thường cho thiệt hại hành lý của NĐBH trong các trường hợp dưới đây:&nbsp;</p>\n<p>-\tHợp đồng bảo hiểm này không bảo hiểm cho:</p>\n<p>a) \tđộng vật.</p>\n<p>b)\thàng lậu hoặc hàng cấm theo Điều lệ của Hãng Hàng không.</p>\n<p>c)\tgiấy tờ hoặc giấy căn cước hoặc giấy tờ khác (có giá hoặc không có giá).</p>\n<p>d)\tthẻ tín dụng, và thẻ thanh toán.</p>\n<p>e)\tvé vận tải, tiền mặt, cổ phiếu, và chứng khoán, trang sức, đá quý.</p>\n<p>f)\tkính mắt, kính áp tròng, thiết bị trợ thính, chân giả, răng giả, cầu răng giả.</p>\n<p>g)\tchìa khóa hoặc thiết bị khác dùng để mở các loại khóa.</p>\n<p>h)\tnhạc cụ, dụng cụ nghệ thuật, đồ cổ, vật sưu tầm, hoặc đồ đạc sử dụng trong sinh hoạt gia đình nhà; hàng mau hỏng, dễ vỡ và  tranh ảnh (trừ trường hợp tranh ảnh bị mất).</p>\n<p>i)\tcác loại thức ăn, đồ uống, hóa chất.</p>\n<p>-\tBảo hiểm HD sẽ không chịu trách nhiệm trước bất cứ thiệt hại nào phát sinh từ hoặc gây ra bởi hoặc liên quan đến:</p>\n<p>a)\tCác vật phẩm dễ vỡ hoặc dễ gãy, nhạc cụ, ra-đi-ô và tài sản tương tự bị nứt vỡ, gãy.</p>\n<p>b)\tTịch thu hoặc sung công theo lệnh của bất cứ chính phủ hoặc cơ quan chính quyền nào.</p>\n<p>c)\tCôn trùng hoặc sâu bọ.</p>\n<p>d)\tHư hỏng hoặc khiếm khuyết vốn có.</p>\n<p>e)\tNhững hao mòn và rách vỡ một cách hợp lý của hành lý gây ra bởi sự di chuyển và va đập thông thường khi vận chuyển bằng đường hàng không.</p>\n<p>f)\tVận chuyển hàng lậu hoặc hàng cấm; hoặc</p>\n<p>g)\tTịch thu cho dù để tiêu hủy theo quy chế, quy định, quyết định kiểm dịch hoặc quy chế hải quan hay không.</p>\n<p>h)\tBất cứ giao dịch thanh toán nào có thể vi phạm các luật, lệnh cấm hoặc quy định của chính phủ.</p>\n<p>i)\tCản trở, bắt giữ, tịch thu, tiêu hủy, trưng dụng, lưu giữ hoặc giam giữ bởi cơ quan hải quan hoặc bất cứ chính phủ hoặc cơ quan hoặc quan chức chính phủ nào khác.</p>\n<p>j)\tNĐBH có bất kỳ hành vi vi phạm pháp luật hoặc chống cự khi bị cơ quan có thẩm quyền bắt giữ.</p>\n<p>k)\tTrong khi NĐBH tham gia ẩu đả hoặc tham gia kích động ẩu đả.</p>\n<p>l)\tNĐBH không thực hiện các biện pháp hợp lý để bảo vệ, bảo quản hoặc tìm lại hành lý thất lạc.</p>\n<p>m)\tNĐBH không thông báo và không có biên bản xác nhận của nhân viên quầy hành lý thất lạc.</p>\n<p>n)\tCác đồ vật không được hãng hàng không chấp nhận là Hành lý ký gửi và vận chuyển.</p>\n<p>-\tBất cứ thiệt hại hoặc bất cứ nghĩa vụ pháp lý nào gặp phải hoặc xảy ra trực tiếp hoặc gián tiếp bởi NĐBH nếu NĐBH là:</p>\n<p>a)\tngười tham gia khủng bố.</p>\n<p>b)\tthành viên của một tổ chức khủng bố.</p>\n<p>c)\tngười cung cấp vũ khí hạt nhân, hóa học hoặc sinh học; hoặc</p>\n<p>d)\tngười buôn bán ma túy.</p>\n",
      "<p>HD Insurance is not liable for the damage of the Insured’s baggage:</p>\n<p>-\tThis insurance contract is not insured for:</p>\n<p>a) \tanimal;</p>\n<p>b)\tcontraband or illegal goods by the Terms &amp; Conditions of the Airlines;</p>\n<p>c)\tidentification documents or other documents (can be exchanged for money or cannot be exchanged for money);</p>\n<p>d)\tcredit cards, and payment cards;</p>\n<p>e)\ttransportation ticket, money, stock, and security certificates, jewelry, precious stones;</p>\n<p>f)\teye glasses, contact lenses, hearing aids, prosthetic limbs, artificial teeth or dental bridges;&nbsp;</p>\n<p>g)\tkeys or other equipment to open locks;</p>\n<p>h)\tmusical instruments, objects of art, antiques, collector’s items or, furniture; perishables and consumables, paintings and pictures (not applicable for lost case);</p>\n<p>i)\tall types of food, drink and chemicals;</p>\n<p>-\tHD Insurance is not liable for any loss or damage resulting from or caused by or relating to:</p>\n<p>a)\tbreakage of brittle or fragile articles, cameras, musical instruments, radios and such similar property;</p>\n<p>b)\tconfiscation or expropriation by order of any government or public authority;</p>\n<p>c)\tinsects or vermin;</p>\n<p>d)\tinherent vice or damage;</p>\n<p>e)\twear and tear of baggage caused by normal movements and knocks during carriage by air.</p>\n<p>f)\ttransportation of contraband or illegal trade; or</p>\n<p>g)\tseizure whether for destruction under quarantine or custom regulation or not;</p>\n<p>h)\tany payment which would violate any law, government prohibition or regulation;</p>\n<p>i)\tdelay, seizure, confiscation, destruction, requisition, retention or detention by customs or any other government or public authority or official;</p>\n<p>j)\tthe Insured has any violation or attempt of violation of laws or resistance to arrest by appropriate authority;</p>\n<p>k)\tthe Insured is taking part in a brawl or taking part in inciting a brawl;</p>\n<p>l)\tfailure of the Insured to take reasonable measure to protect, save or recover lost luggage;</p>\n<p>m)\tfailure of the Insured Person to notify the relevant airline authorities of missing luggage at the scheduled destination point and to obtain a property irregularity report;</p>\n<p>n)\tAny item that is not accepted by the Airlines to be Checked baggage and to be transported.</p>\n<p>-\tAny loss, injury, damage or legal liability suffered or sustained directly or indirectly by an Insured Person if that Insured Person is:</p>\n<p>a)\ta terrorist;</p>\n<p>b)\ta member of a terrorist organization;</p>\n<p>c)\ta purveyor of nuclear, chemical or biological weapons; or</p>\n<p>d)\ta narcotics trafficker.</p>\n"
   ],
   "ysmtunkf":[
      "7.\tHình thức thanh toán phí bảo hiểm như thế nào? Nếu muốn nhận hoá đơn, tôi phải làm gì?",
      "7.\tWhat are the purchasing methods available? How can I get invoice for the purchase? "
   ],
   "vzo2yeny":[
      "<p>Hiệu lực của Giấy Chứng Nhận Bảo Hiểm bắt đầu từ thời điểm hoàn tất thủ tục ký gửi hành lý và kết thúc lại lúc kết thúc hành trình bay (khi Người Được Bảo Hiểm nhận hành lý ký gửi tại khu vực nhận hành lý</p>\n\n",
      "<p>Your insurance certificate will valid from the time your checked baggage finished checking-in until the end of your flight journey (when you receive your baggage at the Baggage Claim Area).</p>\n"
   ],
   "a1xhrnpw":[
      "17.\tTôi có thể xuất trình giấy tờ nào để chứng minh tôi đã mua bảo hiểm?",
      "17.\tWhich paper/document must I show as proof of purchase of the insurance?"
   ],
   "c558ju30":[
      "Vui lòng điền đầy đủ các thông tin sau để Bảo Hiểm HD xuất hoá đơn",
      "Please complete the following information for HD Insurance to invoice"
   ],
   "hcr3a3wa":[
      "<p>Thông tin trên Giấy Chứng Nhận Bảo Hiểm sẽ được tích hợp tự động theo thông tin trên Vé, vì vậy khi thông tin trên Vé thay đổi, thông tin trên Giấy Chứng Nhận Bảo Hiểm sẽ được thay đổi tương ứng. Do vậy, nếu bạn muốn thay đổi bất kì thông tin cá nhân nào trước khi mua bảo hiểm hành lý, bạn cần liên hệ trực tiếp đến số điện thoại hotline của VietJet là 19001886 hoặc gửi email về địa chỉ 19001886@vietjetair.com để được hướng dẫn thay đổi thông tin đã nhập khi đặt vé máy bay.&nbsp;&nbsp;</p>\n<p>Bạn sẽ không thể sửa đổi thông tin cá nhân trên Giấy Chứng Nhận Bảo Hiểm sau khi hoàn tất thanh toán phí bảo hiểm.</p>\n",
      "<p>All information on your Insurance Certificate is automatically sync with the information on your Ticket. Therefore, once you change your information on the Ticket, the Insurance Certificate will get updated accordingly.</p>\n<p>If you like to change any personal information before purchasing the baggage insurance, you must contact VietJet hotline number 19001886 or send email to the address 19001886@vietjetair.com to receive guidance how to change your input information when booking the ticket.</p>\n<p>You cannot change your information on the Insurance Certificate after completing purchase.</p>\n"
   ],
   "pfnnvznt":[
      "15.\tKhi nào thì giấy chứng nhận bảo hiểm bị vô hiệu?",
      "15.\tWhen is my Insurance Certificate invalid?"
   ],
   "iakyziuj":[
      "5.\tQuy trình Bồi thường\n",
      "5.\tClaim process\n"
   ],
   "do4ywkjm":[
      "<div style=\"text-align: center;\">\n<img src=\"/img/vietjet/quyen_loi_VNI.png\" alt=\"\" style=\"height: ;width: 100% \"/>\n</div>",
      "<div style=\"text-align: center;\">\n<img src=\"/img/vietjet/quyen_loi_ENG.png\" alt=\"\" style=\"height: ;width: 100%\"/>\n</div>"
   ],
   "s0lvb24i":[
      "Ông",
      "Ông"
   ],
   "0mgkdoh8":[
      "Anh/chị/em",
      "Brother/sister"
   ],
   "jo1t8yth":[
      "tesst",
      "tesst"
   ],
   "f24jscdw":[
      "<p>Tôi cam kết các thông tin khai báo là chính xác, trung thực và hoàn toàn chịu trách nhiệm về các thông tin đã khai báo.</p>",
      "<p> I undertake that the declared information is accurate, truthful and fully responsible for the declared information. </p>"
   ],
   "9zrcuk2i":[
      "khai báo bồi thường hộ cho khách hàng",
      "khai báo bồi thường hộ cho khách hàng"
   ],
   "2shsu8c2":[
      "<p>Giấy Chứng Nhận Bảo Hiểm vẫn có hiệu lực trong các trường hợp sau:&nbsp;</p>\n<p>- Vé điện tử và xác nhận hành trình của hành khách bị thay đổi do các thao tác xử lý của Hãng hàng không;</p>\n<p>- Chuyến bay bị chuyển hướng do điều kiện bất khả kháng (thời tiết, …);</p>\n<p>- Chuyến bay bị thay đổi lịch bay theo lịch bay của Hãng hàng không;</p>\n<p>- Chuyến bay liên danh.</p>\n<p>Hành khách vui lòng cung cấp kèm theo Giấy Chứng Nhận Bảo Hiểm các thông tin sau: Vé điện tử và xác nhận hành trình hoặc Thẻ lên tàu bay, email hoặc tin nhắn xác nhận thay đổi chuyến bay từ Hãng hàng không hoặc các chứng từ khác về việc thông báo thay đổi chuyến bay để đảm bảo đầy đủ hồ sơ bồi thường.&nbsp;</p>",
      "<p>Your Insurance Certificate is still valid in the cases below:</p>\n<p>- E-ticket and flight confirmation of passengers has been changed due to handling operations of the Airlines;</p>\n<p>- Divert flight;</p>\n<p>- Scheduled changed flight;</p>\n<p>- Codeshare flight.</p>\n<p>Please provide documents including: E-ticket and flight confirmation or Boarding pass, email or SMS of confirmation about changing flight journey from the Airline,.. with this Certificate for HD Insurance to process claim.</p>\n"
   ],
   "phvakbjc":[
      "19.\tThời gian để xử lý yêu cầu bồi thường của tôi là bao lâu?",
      "19.\tHow long does it take for the claim process?"
   ],
   "srlhuvic":[
      "Tra cứu thông tin hoá đơn",
      "Search Invoice"
   ],
   "16uaqlrh":[
      "Mẹ",
      "Mother"
   ],
   "h30bxuy0":[
      "<p><span style=\"color: rgb(226,80,65);\">_ Bên mua bảo hiểm và/hoặc Người được bảo hiểm vẫn phải gửi Hồ sơ yêu cầu bồi thường bảo hiểm và bản gốc các giấy tờ theo yêu cầu cho HDI sau khi tập hợp đủ các tài liệu<br>_ Trường hợp thông tin trên thẻ lên tàu bay (boarding pass) khác với thông tin trên giấy chứng nhận bảo hiểm, NĐBH phải cung cấp các tài liệu, chứng từ của Hãng hàng không để xác nhận việc thay đổi thông tin liên quan</span></p>\n",
      "<p></p>\n"
   ],
   "3souckhw":[
      "Bạn muốn xuất hóa đơn cho?",
      "Who do you want to invoice to?"
   ],
   "y6vqiakd":[
      "Nơi nhận hành lý",
      "Place of receiving"
   ],
   "oqilxyny":[
      "<p><strong>Sau khi được Bảo hiểm HD hướng dẫn bồi thường, bạn vui lòng chuẩn bị các hồ sơ/chứng từ sau đây:</strong></p>\n<p>1. Giấy chứng nhận bảo hiểm điện tử</p>\n<p>2. Bản sao CMND/ CCCD/ Hộ chiếu</p>\n<p>3. Bản sao Thẻ lên tàu bay (boarding pass)</p>\n<p>4. Bản sao thẻ hành lý (luggage tag)</p>\n<p><strong><em>Lưu ý:</em></strong> Trường hợp thông tin trên Thẻ lên tàu bay (boarding pass) khác với thông tin trên giấy chứng nhận bảo hiểm, Người được bảo hiểm phải cung cấp các tài liệu, chứng từ của Hãng hàng không để xác nhận về việc thay đổi thông tin liên quan.&nbsp;</p>\n<p><strong>A.Trường hợp yêu cầu bồi thường thiệt hại/ mất hành lý:</strong></p>\n<p>5.Bản chính Báo cáo bất thường về hành lý (PIR) từ Hãng hàng không liên quan đến hành lý thất lạc/mất/hư hỏng bao gồm thông tin chi tiết về Chuyến bay theo lịch trình và/hoặc thông tin chi tiết bằng văn bản xác nhận về việc hành lý thất lạc/mất/hư hỏng</p>\n<p>6. Các hoá đơn (bao gồm hóa đơn điện tử) theo quy định pháp luật và/ hoặc các loại chứng từ chứng minh giá trị của hành lý, tài sản&nbsp;</p>\n<p>7. Các hoá đơn (bao gồm hóa đơn điện tử) về việc sửa chữa hành lý, tài sản</p>\n<p>8. Các loại chứng từ khác theo yêu cầu của BẢO HIỂM HD (nếu có)</p>\n<p><strong>B. Trường hợp yêu cầu bồi thường liên quan chậm trễ hành lý:</strong></p>\n<p>5. Bản chính Thư gửi khách trong trường hợp hành lý về trễ (Letter to passenger of late delivery bag)</p>\n<p>6. Các loại chứng từ khác theo yêu cầu của Bảo hiểm HD (nếu có)</p>\n<p></p>\n",
      "<p><strong>1.\tGeneral Documents:</strong></p>\n<p>-\te-Insurance Certificate</p>\n<p>-\tCopy of Identification document(s)</p>\n<p>-\tCopy of Boarding pass</p>\n<p>-\tCopy of Luggage tag</p>\n<p>Notice: In case the information on the boarding pass is different from the insurance certificate, the Insured must provide proof, document(s) from the Airline confirming the changes.</p>\n<p><strong>2.\tFor claim request of damaged or lost Checked Baggage:</strong></p>\n<p>Other than the general documents, customer must provide additional documents include:</p>\n<p>-\tOriginal copy of Property Irregularity Report (PIR) from the Airlines relating to the damage/lost including details of the Flight according to the flight journey and/or any other document with details of the damage/lost of baggage,</p>\n<p>-\tReceipt(s) (including e-invoice) by the law regulation and/or proof of luggage and items’ value document(s),</p>\n<p>-\tReceipt(s) (including e-invoice) of repairing luggage and items,</p>\n<p>-\tOther documents …</p>\n<p><strong>3.\tFor claim request of delayed Checked Baggage:</strong></p>\n<p>Other than the general documents, customer must provide additional documents include:</p>\n<p>-\tOriginal copy of Letter to passenger of late delivery bag</p>\n<p>-\tOther documents …</p>\n<p>HD Insurance will process with the claim for the Policyholder and/or the Insured within 15 (fifteen) days from the date receiving valid and complete Claim request documents from the Policy holder and/or the Insured.</p>\n"
   ],
   "select_bank_label":[
      "Chọn Ngân Hàng",
      "Choose Bank"
   ]
}


const Index = (pageProps) => {
  global.lng = new Language(json_langage);

  globalStore.set("pageconfig", pageProps.initPage.page_config);
  globalStore.set("page_version", pageProps.initPage.page_version);
  global.page_version = pageProps.initPage.page_version;
  


  const pageStatus = pageProps.initPage.page_status;
  const [pageConfig, setPageConfig] = useState(pageProps.initPage);
  
  const router = useRouter();
  const [preload, setPreload] = useState(true);
  const [preloadClass, setPreloadClass] = useState("page-loader");
  useEffect(() => {
    try {
      if (preload) {
        setPreload(false);
        setTimeout(() => {
          setPreloadClass("page-loader hide");
          setTimeout(() => {
            setPreloadClass("page-loader none");
          }, 500);
        }, 500);
      }
    } catch (e) {
      // setPageState(1)
    }
  });

  useEffect(() => {
    try {
    } catch (e) {
      console.log(e);
    }
  }, [pageProps.siteurl]);

  return (
    <Layout org="vietjet_vn">
        <Head>
            <title>{pageConfig.meta_data.page_title}</title>
            <meta name="viewport" content="user-scalable=no, width=device-width" />
            <link rel="stylesheet" href="/css/app.css" />
            <meta name="keywords" content={pageConfig.meta_data.page_keywords}/>
            <meta name="description" content={pageConfig.meta_data.page_description}/>
            <meta property="og:title" content={pageConfig.meta_data.page_title}/>
            <meta property="og:description" content={pageConfig.meta_data.page_description}/>
            {pageConfig.meta_data.page_thumbnail&&<meta property="og:image" content={pageConfig.page_thumbnail}/>}
            {pageConfig.meta_data.page_thumbnail&&<meta name="twitter:image" content={pageConfig.page_thumbnail}/>}
        </Head>
      <div className={preloadClass} />
      <DynamicComponentWithNoSSR org={"vietjet_vn"} outerRouter={router} />
    </Layout>
  );
};

export async function getServerSideProps(ctx){
  try{
    var domainpath = ""
    var domainhost = ""
    var page_error = false
    var prod_ref = null
    var product_config = {}
    var query = {}

    if(ctx.req){
      domainpath = ctx.req._parsedOriginalUrl.pathname
      domainhost = ctx.req.headers.host
      query = ctx.req.query
    }else{
      domainpath = ctx.pathname
      domainhost = ctx.headers.host
      query = ctx.query
    }
    var page_component_object = null
    var initPage = await Utils.initPage(domainhost, domainpath)
    
   if(query.ref){
      var decoded = jwt.verify(query.ref, "C5F3AC6BC47D5F69E0530100007F3016", (err, decoded)=>{
         if(decoded){
            prod_ref = decoded
            initPage.prod_ref
            initPage.org = decoded.org
            initPage.product_config.ORG_CODE = decoded.org

         }else{
            page_error = true
         }
      });
    }
    if(query.org){
      initPage.product_config.ORG_CODE = query.org
    }

    if(initPage.permalink){
       return { props: { domainpath: domainpath, initPage: initPage, siteurl: domainhost,  page_error: page_error  } }
    }else{
       return { props: { domainpath: null, initPage: {meta_data:{page_title:"Page is error"}}, page_component_object: {}, page_error: true } }
    }
  }catch(e){
    console.log("page Error ", e)
    return { props: { domainpath: null, initPage: {meta_data:{page_title:"Page is error"}}, page_component_object: {}, page_error: true } }
  }
}

export default Index;
