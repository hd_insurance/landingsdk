import { useRouter } from "next/router";
import MainVisa from "../landing_modules/module_visa";
import MainEclaim from "../landing_modules/e_claim";
// import MainTnds from "../components/modules/car/main.js";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Lang from "../services/Language.js";
import Language from "../services/Lang.js";

import Utils from "../services/utils";
import globalStore from "../services/globalStore";
import ErrorPage from "../components/error_page.js";
global.l = new Lang();

const hdg = "WEGQX02PFJ";
const vjg = "W39D2CPV6P";

const json_lang = {
   "2kz0tzrq":[
      "Chi trả bồi thường trong vòng 07 ngày làm việc sau khi đầy đủ hồ sơ.",
      "Chi trả bồi thường trong vòng 07 ngày làm việc sau khi đầy đủ hồ sơ"
   ],
   "49ocwyni":[
      "Giải đáp các thông tin giúp khách hàng hiểu rõ về chính sách, quyền lợi và quy trình bảo hiểm.",
      "Giải đáp các thông tin giúp khách hàng hiểu rõ về chính sách, quyền lợi và quy trình bảo hiểm."
   ],
   "5nabbpt9":[
      "CÂU HỎI THƯỜNG GẶP",
      "CÂU HỎI THƯỜNG GẶP"
   ],
   "ztg39nfh":[
      "Quyền lợi chủ thẻ Master Happy Care",
      "Quyền lợi chủ thẻ Master Happy Care"
   ],
   "ajyf02dw":[
      "Hãy liên lạc với chúng tôi bằng 1 trong các cách bên dưới để được hỗ trợ 1 cách chu đáo và nhiệt tình ",
      "Hãy liên lạc với chúng tôi bằng 1 trong các cách bên dưới để được hỗ trợ 1 cách chu đáo và nhiệt tình \n"
   ],
   "nukfum0j":[
      "Bạn đang cần hỗ trợ ?",
      "Bạn đang cần hỗ trợ ?"
   ],
   "uwcjhzrg":[
      "Số tiền bồi thường cụ thể cho từng quyền lợi Bảo hiểm sức khỏe Visa Happy Care?",
      "Số tiền bồi thường cụ thể cho từng quyền lợi Bảo hiểm sức khỏe Visa Happy Care?"
   ],
   "xkronapk":[
      "Bảo hiểm sức khỏe Visa Happy Care sẽ chi trả những quyền lợi nào?",
      "Bảo hiểm sức khỏe Visa Happy Care sẽ chi trả những quyền lợi nào?"
   ],
   "p1wyckox":[
      "Chi phí bồi thường cho từng hạng mục được quy định như thế nào?",
      "Chi phí bồi thường cho từng hạng mục được quy định như thế nào?"
   ],
   "b0qi8mma":[
      "Bao gồm các quyền lợi bảo hiểm chính:\n- Điều trị nội trú do ốm, bệnh: chi phí nằm viện, chi phí phẫu thuật, chi phí khám điều trị trước sau, dịch vụ xe cứu thương, trợ cấp nằm viện\n- Chi phí y tế điều trị tai nạn.\n- Thương tật, tàn tật toàn bộ, tử vong với số tiền bảo hiểm lên đến 2 tỷ đồng\nQuyền lợi bổ sung: Bảo hiểm thai sản áp dụng độ tuổi 19 - 50 \n- Bảo hiểm điều trị ngoại trú\n- Bảo hiểm nha khoa \n- Trợ cấp nằm viện sau tai nạn\nDịch vụ bảo lãnh viện phí 24/7",
      "Bao gồm các quyền lợi bảo hiểm chính:\n- Điều trị nội trú do ốm, bệnh: chi phí nằm viện, chi phí phẫu thuật, chi phí khám điều trị trước sau, dịch vụ xe cứu thương, trợ cấp nằm viện\n- Chi phí y tế điều trị tai nạn.\n- Thương tật, tàn tật toàn bộ, tử vong với số tiền bảo hiểm lên đến 2 tỷ đồng\nQuyền lợi bổ sung: Bảo hiểm thai sản áp dụng độ tuổi 19 - 50 \n- Bảo hiểm điều trị ngoại trú\n- Bảo hiểm nha khoa \n- Trợ cấp nằm viện sau tai nạn\nDịch vụ bảo lãnh viện phí 24/7"
   ],
   "vompcz9m":[
      "<h2>Vững tâm tiến bước với <b>Happy Digital Life</b></h2>",
      "<h2>Vững tâm tiến bước với <b>Happy Digital Life</b></h2>"
   ],
   "7gwkoch4":[
      "Ứng dụng hàng đầu về bảo hiểm phi nhân thọ tại Việt Nam.\n",
      "Ứng dụng hàng đầu về bảo hiểm phi nhân thọ tại Việt Nam.\n"
   ],
   "hlcv9b2p":[
      "Tận hưởng cuộc sống",
      "Tận hưởng cuộc sống"
   ],
   "bxd9ffgx":[
      "Hướng dẫn sử dụng",
      "Hướng dẫn sử dụng"
   ],
   "j7bjv9ea":[
      "hoặc",
      "hoặc"
   ],
   "jkescdud":[
      "Theo dõi chúng tôi",
      "Theo dõi chúng tôi"
   ],
   "axbemxsz":[
      "Trợ giúp",
      "Trợ giúp"
   ],
   "c5xqhsvs":[
      "Liên hệ",
      "Liên hệ"
   ],
   "xhdirsq1":[
      "Gói bảo hiểm:",
      "Gói bảo hiểm:"
   ],
   "pikeqyg8":[
      "Mã truy cập",
      "Mã truy cập"
   ],
   "2m9mvzzk":[
      "Bồi thường",
      "Bồi thường"
   ],
   "onits43h":[
      "Gọi hotline số 0898 879 229 hoặc gửi email kèm thông tin liên hệ về địa chỉ cskh@hdinsurance.com.vn để được tư vấn.",
      "Gọi hotline số 0898 879 229 hoặc gửi email kèm thông tin liên hệ về địa chỉ cskh@hdinsurance.com.vn để được tư vấn."
   ],
   "jv5xvaa5":[
      "Những điểm loại trừ nào trong Bảo hiểm Sức khỏe Visa Happy Care sẽ không được chi trả bồi thường?",
      "Những điểm loại trừ nào trong Bảo hiểm Sức khỏe Visa Happy Care sẽ không được chi trả bồi thường?"
   ],
   "kcsbk9vq":[
      "Tên chủ thẻ:",
      "Tên chủ thẻ:"
   ],
   "jcfs0b8e":[
      "Công ty TNHH Bảo hiểm HD",
      "Công ty TNHH Bảo hiểm HD"
   ],
   "2grkcxl7":[
      "Điều khoản",
      "Điều khoản"
   ],
   "azzhdnof":[
      "Số giấy chứng nhận:",
      "Số giấy chứng nhận:"
   ],
   "oyzm5cae":[
      "Nhập mã truy cập trên thẻ và số điện thoại hoặc email để tra cứu giấy chứng nhận bảo hiểm",
      "Nhập mã truy cập trên thẻ và số điện thoại hoặc email để tra cứu giấy chứng nhận bảo hiểm"
   ],
   "uyb7yaxg":[
      "Hợp đồng bảo hiểm",
      "Hợp đồng bảo hiểm"
   ],
   "fdzfnxt7":[
      "3. Mở thẻ tín dụng có cần phải chứng minh tài chính, cung cấp sao kê lương hay giấy tờ pháp lý liên quan hay không?",
      "3. Mở thẻ tín dụng có cần phải chứng minh tài chính, cung cấp sao kê lương hay giấy tờ pháp lý liên quan hay không?"
   ],
   "2mvfwhcl":[
      "Tòa nhà Abacus, 58 Nguyễn Đình Chiểu, Quận 1, Tp. Hồ Chí Minh",
      "Tòa nhà Abacus, 58 Nguyễn Đình Chiểu, Quận 1, Tp. Hồ Chí Minh"
   ],
   "wxrcgp88":[
      "Gửi yêu cầu bồi thường thành công",
      "Gửi yêu cầu bồi thường thành công"
   ],
   "myd1feen":[
      "Thanh toán phí",
      "Thanh toán phí"
   ],
   "igpheeaz":[
      "CÂU HỎI THƯỜNG GẶP",
      "CÂU HỎI THƯỜNG GẶP"
   ],
   "ucmysxvv":[
      "<p>Tải App ngay hôm nay để nhận những ưu đãi đặc biệt từ  <b style=\"padding-left:8px\">Happy Digital Life</b></p>",
      "<p>Tải App ngay hôm nay để nhận những ưu đãi đặc biệt từ  <b style=\"padding-left:8px\">Happy Digital Life</b></p>"
   ],
   "kxclgeso":[
      "1. THẺ MASTER HAPPY CARE là gì?",
      "1. THẺ MASTER HAPPY CARE là gì?"
   ],
   "yjmumzaz":[
      "2. Các dịch vụ tiện ích đi kèm thẻ?",
      "2. Các dịch vụ tiện ích đi kèm thẻ?"
   ],
   "0btanp8u":[
      "Nhận thông tin mới nhất",
      "Nhận thông tin mới nhất"
   ],
   "xgu8hy18":[
      "Email của bạn",
      "Email của bạn"
   ],
   "qx8qyad1":[
      "Khách hàng không cần chứng minh tài chính để mở thẻ. Việc mở thẻ cần cung cấp bản sao hộ khẩu và Chứng minh nhân dân/Căn cước công dân.",
      "Khách hàng không cần chứng minh tài chính để mở thẻ. Việc mở thẻ cần cung cấp bản sao hộ khẩu và Chứng minh nhân dân/Căn cước công dân."
   ],
   "t4morzkk":[
      "Miễn phí thường niên, phát hành thẻ, miễn thẩm định hồ sơ tín dụng, tràn ngập các ưu đãi của thẻ Master",
      "Miễn phí thường niên, phát hành thẻ, miễn thẩm định hồ sơ tín dụng, tràn ngập các ưu đãi của thẻ Master"
   ],
   "0ecdhgrm":[
      "Chương trình bảo hiểm",
      "Chương trình bảo hiểm"
   ],
   "1shxqzxa":[
      "Thông tin yêu cầu bồi thường của bạn đã được gửi đến bộ phận bồi thường. Nhân viên bồi thường sẽ liên lạc lại với bạn trong thời gian sớm nhất. Mọi thông tin thắc mắc về bồi thường xin liên hệ hotline: ",
      "Thông tin yêu cầu bồi thường của bạn đã được gửi đến bộ phận bồi thường. Nhân viên bồi thường sẽ liên lạc lại với bạn trong thời gian sớm nhất. Mọi thông tin thắc mắc về bồi thường xin liên hệ hotline: "
   ],
   "he5cmjow":[
      "Hotline Bồi thường:",
      "Hotline Bồi thường:"
   ],
   "drwds08b":[
      "1. Quyền lợi khi tham gia chương trình MASTER HAPPY CARE?\n",
      "1. Quyền lợi khi tham gia chương trình MASTER HAPPY CARE?\n"
   ],
   "qbsprofn":[
      "Thông báo",
      "Thông báo"
   ],
   "phdjpvj7":[
      "Điều kiện tham gia",
      "Điều kiện tham gia"
   ],
   "qpdifz5o":[
      "<p style=\"text-align:justify;\">Bạn có thể khám chữa bệnh tại mọi bệnh viện, phòng khám, cơ sở y tế đạt tiêu chuẩn, chất lượng cao tại Việt Nam như Vinmec, FV, Hoàn Mỹ, Hạnh Phúc,… miễn là có xuất hoá đơn VAT, có đóng dấu pháp nhân của cơ sở y tế trên tài liệu. </p>",
      "<p style=\"text-align:justify;\">Bạn có thể khám chữa bệnh tại mọi bệnh viện, phòng khám, cơ sở y tế đạt tiêu chuẩn, chất lượng cao tại Việt Nam như Vinmec, FV, Hoàn Mỹ, Hạnh Phúc,… miễn là có xuất hoá đơn VAT, có đóng dấu pháp nhân của cơ sở y tế trên tài liệu. </p>"
   ],
   "ceelajtn":[
      ". Xin cám ơn !",
      ". Xin cám ơn !"
   ],
   "o9kxlypn":[
      "Hotline ZutoRide:",
      "Hotline ZutoRide:"
   ],
   "m9zunts8":[
      "<p>MASTER HAPPY CARE bao gồm tất cả những quyền lợi như:&nbsp;</p>\n<p>- Tử vong/thương tật do tai nạn, bệnh tật</p>\n<p>- Chi phí y tế do tai nạn; nằm viện hoặc phẫu thuật</p>\n<p>- Điều trị ngoại trú</p>\n<p>- Nha khoa</p>\n<p>- Thai sản&nbsp;</p>\n<p>Với Hạn mức bảo hiểm được thiết kế dành riêng cho nhân viên của Doanh Nghiệp đáp ứng tiêu chí Quyền lợi tốt và Mức phí bảo hiểm canh trạnh so với các chương trình bảo hiểm cá nhân đang cung cấp trên thị trường.</p>\n",
      "<p>MASTER HAPPY CARE bao gồm tất cả những quyền lợi như:&nbsp;</p>\n<p>- Tử vong/thương tật do tai nạn, bệnh tật</p>\n<p>- Chi phí y tế do tai nạn; nằm viện hoặc phẫu thuật</p>\n<p>- Điều trị ngoại trú</p>\n<p>- Nha khoa</p>\n<p>- Thai sản&nbsp;</p>\n<p>Với Hạn mức bảo hiểm được thiết kế dành riêng cho nhân viên của Doanh Nghiệp đáp ứng tiêu chí Quyền lợi tốt và Mức phí bảo hiểm canh trạnh so với các chương trình bảo hiểm cá nhân đang cung cấp trên thị trường.</p>\n"
   ],
   "3adjas71":[
      "6. Thời gian chờ của bảo hiểm MASTER HAPPY CARE",
      "6. Thời gian chờ của bảo hiểm MASTER HAPPY CARE"
   ],
   "htvhwwig":[
      "4. Làm thế nào để đăng ký tham gia chương trình MASTER HAPPY CARE?",
      "4. Làm thế nào để đăng ký tham gia chương trình MASTER HAPPY CARE?"
   ],
   "utolqtn6":[
      "<p>Thời gian chờ được định nghĩa là khoảng thời gian mà các quyền lợi liên quan để không được thanh toán. Đối với nhân viên và người thân, thời gian chờ được áp dụng như sau:<p>- 30 ngày đối với điều trị bệnh thông thường</p><p>- 30 ngày đối với tử vong do bệnh thông thường</p><p>- 365 ngày đối với điều trị bệnh đặc biệt, bệnh có sẵn</p><p>- 365 ngày đối với tử vong do bệnh đặc biệt, bệnh có sẵn, thai sản</p>",
      "<p>Thời gian chờ được định nghĩa là khoảng thời gian mà các quyền lợi liên quan để không được thanh toán. Đối với nhân viên và người thân, thời gian chờ được áp dụng như sau:<p>- 30 ngày đối với điều trị bệnh thông thường</p><p>- 30 ngày đối với tử vong do bệnh thông thường</p><p>- 365 ngày đối với điều trị bệnh đặc biệt, bệnh có sẵn</p><p>- 365 ngày đối với tử vong do bệnh đặc biệt, bệnh có sẵn, thai sản</p>"
   ],
   "i0usd0ri":[
      "THÔNG TIN CHỦ THẺ",
      "THÔNG TIN CHỦ THẺ"
   ],
   "3milyxb0":[
      "3. Quy trình yêu cầu bồi thường với trường hợp khám bệnh ngoài hệ thống bảo lãnh",
      "3. Quy trình yêu cầu bồi thường với trường hợp khám bệnh ngoài hệ thống bảo lãnh"
   ],
   "759hwajs":[
      "<p>- Các dịch vụ ưu đãi đi kèm có hiệu lực cùng với hiệu lực bảo hiểm.</p>\n<p>- Khi bảo hiểm hết hiệu lực và khách hàng không tham gia tái tục, tất cả các dịch vụ đi kèm cũng như thẻ tín dụng cũng sẽ tự động ngưng hoạt động.&nbsp;</p>\n<p>- Riêng đối với thẻ tín dụng, HDBank sẽ liên hệ/thông báo tới Khách hàng để xác nhận Khách hàng có muốn tiếp tục sử dụng dịch vụ thẻ tín dụng của HDBank hay không.</p>\n",
      "<p>- Các dịch vụ ưu đãi đi kèm có hiệu lực cùng với hiệu lực bảo hiểm.</p>\n<p>- Khi bảo hiểm hết hiệu lực và khách hàng không tham gia tái tục, tất cả các dịch vụ đi kèm cũng như thẻ tín dụng cũng sẽ tự động ngưng hoạt động.&nbsp;</p>\n<p>- Riêng đối với thẻ tín dụng, HDBank sẽ liên hệ/thông báo tới Khách hàng để xác nhận Khách hàng có muốn tiếp tục sử dụng dịch vụ thẻ tín dụng của HDBank hay không.</p>\n"
   ],
   "kdwkhnh1":[
      "2. Khi tham gia MASTER HAPPY CARE có bắt buộc mở thẻ tín dụng hay không? Và các chi phí liên quan đến thẻ tín dụng quy định như thế nào?\n",
      "2. Khi tham gia MASTER HAPPY CARE có bắt buộc mở thẻ tín dụng hay không? Và các chi phí liên quan đến thẻ tín dụng quy định như thế nào?\n"
   ],
   "6dl6umrp":[
      "2. Quy trình bảo lãnh viện phí",
      "2. Quy trình bảo lãnh viện phí"
   ],
   "nv5agc7p":[
      "3. Nếu hết hạn bảo hiểm và không tham gia tái tục bảo hiểm thì thẻ tín dụng và các dịch vụ ưu đãi đi kèm còn hiệu lực không?",
      "3. Nếu hết hạn bảo hiểm và không tham gia tái tục bảo hiểm thì thẻ tín dụng và các dịch vụ ưu đãi đi kèm còn hiệu lực không?"
   ],
   "f9sumyn0":[
      "<p>Để xem chi tiết về hồ sơ bồi thường, quý khách vui lòng xem <a href=\"https://hyperservices.hdinsurance.com.vn/f/ec57f58dbba3cc3c83fb5a2f51f792bf\" target=\"_blank\">tại đây</a>.</p>\n<p>Để tải mẫu giấy yêu cầu trả tiền bảo hiểm, vui lòng nhấn nút <a href=\"https://dev-hyperservices.hdinsurance.com.vn/f/2c13427da13521334e953ded53f1460d?download=true\" target=\"_blank\">tải file</a> .</p>\n",
      "<p>Để xem chi tiết về hồ sơ bồi thường, quý khách vui lòng xem <a href=\"https://hyperservices.hdinsurance.com.vn/f/ec57f58dbba3cc3c83fb5a2f51f792bf\" target=\"_blank\">tại đây</a> .</p>\n<p>Để tải mẫu giấy yêu cầu trả tiền bảo hiểm, vui lòng nhấn nút <a href=\"https://dev-hyperservices.hdinsurance.com.vn/f/2c13427da13521334e953ded53f1460d?download=true\" target=\"_blank\">tải file</a> .</p>\n"
   ],
   "htktqum9":[
      "<p>Bảo hiểm HD bảo lãnh viện phí cho khách hàng khi điều trị ngoại và nội trú tại những bệnh viện có liên kết của Bảo hiểm HD và đơn vị giải quyết bồi thường Insmart.&nbsp;</p>\n<p>Quy trình bảo lãnh viện phí:</p>\n<p>1.\tNgười được bảo hiểm xuất trình thẻ Master Happy Care và giấy tờ tùy thân khi đi khám tại bệnh viện</p>\n<p>2.\tBệnh viện kiểm tra thẻ bảo hiểm, thông tin bảo hiểm và gửi thông tin cho Insmart/bảo hiểm HDI</p>\n<p>3.\tInsmart/Bảo hiểm HD kiểm tra thông tin để xác nhận bảo lãnh cho người được bảo hiểm. Sau đó tiến hành thanh toán cho bệnh viện.</p>\n",
      "<p>Bảo hiểm HD bảo lãnh viện phí cho khách hàng khi điều trị ngoại và nội trú tại những bệnh viện có liên kết của Bảo hiểm HD và đơn vị giải quyết bồi thường Insmart.&nbsp;</p>\n<p>Quy trình bảo lãnh viện phí:</p>\n<p>1.\tNgười được bảo hiểm xuất trình thẻ Master Happy Care và giấy tờ tùy thân khi đi khám tại bệnh viện</p>\n<p>2.\tBệnh viện kiểm tra thẻ bảo hiểm, thông tin bảo hiểm và gửi thông tin cho Insmart/bảo hiểm HDI</p>\n<p>3.\tInsmart/Bảo hiểm HD kiểm tra thông tin để xác nhận bảo lãnh cho người được bảo hiểm. Sau đó tiến hành thanh toán cho bệnh viện.</p>\n"
   ],
   "06pqs6rh":[
      "5. Địa chỉ nhận hồ sơ bồi thường",
      "5. Địa chỉ nhận hồ sơ bồi thường"
   ],
   "uheyw3ed":[
      "Quyền lợi bảo hiểm:",
      "Quyền lợi bảo hiểm:"
   ],
   "va2qpeiv":[
      "Ấn để xem tóm tắt quyền lợi",
      "Ấn để xem tóm tắt quyền lợi"
   ],
   "fp7dyzvl":[
      "<p>- Bảo lãnh viện phí là thoả thuận giữa Công ty bảo hiểm và Bệnh viện (hoặc cơ sở y tế khác).</p>\n<p>- Theo đó, bệnh viện gửi hoá đơn dịch vụ y tế trực tiếp đến công ty bảo hiểm và công ty bảo hiểm sẽ thanh toán trực tiếp toàn bộ hoặc một phần chi phí y tế của bạn.Bảo hiểm MASTER HAPPY CARE được thanh toán với bệnh nhân nội trú (nhập viện và phẫu thuật) và ngoại trú tại tất cả các bệnh viện, cơ sở y tế trong danh mục mạng lưới thanh toán trực tiếp của HDI tại Việt Nam. Đối với các trường hợp khác vui lòng thu thập đầy đủ giấy tờ để được hoàn tiền. Mỗi người được bảo hiểm sẽ có một thẻ bảo lãnh viện phí thể hiện những nội dung cơ bản như số hợp đồng, thời hạn bảo hiểm.</p>\n<p>- Đối với thủ tục bảo lãnh viện phí:</p>\n<p>&nbsp;&nbsp;+  Trước khi nhập viện: Xuất trình thẻ bảo hiểm y tế và CMND/Hộ chiếu/ Giấy khai sinh khi đến bệnh viện.</p>\n<p>&nbsp;&nbsp;+  Trước khi xuất viện: Ký tên phiếu điều trị và thanh toán các chi phí vượt quá hoặc các khoản không được bảo hiểm trước khi ra viện.</p>\n",
      "<p>- Bảo lãnh viện phí là thoả thuận giữa Công ty bảo hiểm và Bệnh viện (hoặc cơ sở y tế khác).</p>\n<p>- Theo đó, bệnh viện gửi hoá đơn dịch vụ y tế trực tiếp đến công ty bảo hiểm và công ty bảo hiểm sẽ thanh toán trực tiếp toàn bộ hoặc một phần chi phí y tế của bạn.Bảo hiểm MASTER HAPPY CARE được thanh toán với bệnh nhân nội trú (nhập viện và phẫu thuật) và ngoại trú tại tất cả các bệnh viện, cơ sở y tế trong danh mục mạng lưới thanh toán trực tiếp của HDI tại Việt Nam. Đối với các trường hợp khác vui lòng thu thập đầy đủ giấy tờ để được hoàn tiền. Mỗi người được bảo hiểm sẽ có một thẻ bảo lãnh viện phí thể hiện những nội dung cơ bản như số hợp đồng, thời hạn bảo hiểm.</p>\n<p>- Đối với thủ tục bảo lãnh viện phí:</p>\n<p>&nbsp;&nbsp;+  Trước khi nhập viện: Xuất trình thẻ bảo hiểm y tế và CMND/Hộ chiếu/ Giấy khai sinh khi đến bệnh viện.</p>\n<p>&nbsp;&nbsp;+  Trước khi xuất viện: Ký tên phiếu điều trị và thanh toán các chi phí vượt quá hoặc các khoản không được bảo hiểm trước khi ra viện.</p>\n"
   ],
   "htj6rnwd":[
      "Hủy đăng ký",
      "Hủy đăng ký"
   ],
   "8sbivizu":[
      "Số điện thoại/ Email",
      "Số điện thoại/ Email"
   ],
   "podse396":[
      "4. Hồ sơ yêu cầu bồi thường ",
      "4. Hồ sơ yêu cầu bồi thường "
   ],
   "sltgzd2m":[
      "cskh@hdinsurance.com.vn",
      "cskh@hdinsurance.com.vn"
   ],
   "lixkghlg":[
      "<p>Quý Khách hàng vui lòng truy cập vào đường link <a href=\"https://insmart.com.vn/danh-sach-co-so-y-te/\" target=\"_blank\">https://insmart.com.vn/danh-sach-co-so-y-te/</a>  để tra cứu thông tin bệnh viện có trong danh sách bảo lãnh.</p>\n",
      "<p>Quý Khách hàng vui lòng truy cập vào đường link <a href=\"https://insmart.com.vn/danh-sach-co-so-y-te/\" target=\"_blank\">https://insmart.com.vn/danh-sach-co-so-y-te/</a>  để tra cứu thông tin bệnh viện có trong danh sách bảo lãnh.</p>"
   ],
   "tibpt6yf":[
      "MASTER HAPPY CARE",
      "MASTER HAPPY CARE"
   ],
   "bf4gof86":[
      "Quét mã QR hoặc ấn vào nút bên cạnh để xem chi tiết Giấy chứng nhận",
      "Quét mã QR hoặc ấn vào nút bên cạnh để xem chi tiết Giấy chứng nhận"
   ],
   "v2rpg2td":[
      "Quyền lợi chủ thẻ Master Happy Care",
      "Quyền lợi chủ thẻ Master Happy Care"
   ],
   "nkvsqkoa":[
      "Hệ thống bảo lãnh viện phí tại hơn 300 cơ sở y tế ",
      "Hệ thống bảo lãnh viện phí tại hơn 300 cơ sở y tế "
   ],
   "wdv7pusk":[
      "<p>- Khách hàng tham gia chương trình MASTER HAPPY CARE không bắt buộc mở thẻ tín dụng.&nbsp;</p>\n<p>- Bảo hiểm HD sẽ chỉ phát hành mở thẻ tín dụng dựa trên xác nhận đồng ý mở thẻ của khách hàng.&nbsp;</p>\n<p>- Khi khách hàng tham gia MASTER HAPPY CARE và đồng ý phát hành thẻ, chi phí phát hành thẻ cùng phí thường niên hoàn toàn miễn phí.&nbsp;</p>\n",
      "<p>- Khách hàng tham gia chương trình MASTER HAPPY CARE không bắt buộc mở thẻ tín dụng.&nbsp;</p>\n<p>- Bảo hiểm HD sẽ chỉ phát hành mở thẻ tín dụng dựa trên xác nhận đồng ý mở thẻ của khách hàng.&nbsp;</p>\n<p>- Khi khách hàng tham gia MASTER HAPPY CARE và đồng ý phát hành thẻ, chi phí phát hành thẻ cùng phí thường niên hoàn toàn miễn phí.&nbsp;</p>\n"
   ],
   "dasqrqly":[
      "7. Những bệnh viện, phòng khám nào được áp dụng bảo hiểm?",
      "7. Những bệnh viện, phòng khám nào được áp dụng bảo hiểm?"
   ],
   "to5gabje":[
      "<p>- Thẻ Master Happy Care là dòng thẻ đặc biệt và duy nhất trên thị trường hiện nay, kết hợp hoàn hảo giữa thẻ bảo hiểm chăm sóc sức khỏe chất lượng cao của Bảo hiểm HD và thẻ tín dụng Master của Ngân hàng HDBank nhằm tạo sự thuận tiện và các tiện ích tối ưu cho khách hàng.</p>\n<p>- Thẻ Master Happy Care được Ngân hàng HDBank phát hành dựa trên hợp đồng bảo hiểm của Bảo hiểm HD và các khách hàng, với các đặc tính và quyền lợi như sau dành cho chủ thẻ:</p>\n<ul>\n<li>Cấp hạn mức thẻ tín dụng thẻ Master tối thiểu từ 20tr – 30tr&nbsp;&nbsp;</li>\n<li>Miễn phí phát hành thẻ;</li>\n<li>Miễn phí thường niên;</li>\n<li>Mua bảo hiểm chăm sóc sức khỏe Master Happy Care cho người thân (bố/mẹ/vợ/chồng/con) với chương trình trả góp lãi suất 0%;</li>\n<li>Mua các loại hình bảo hiểm khác của Bảo hiểm HD với chương trình ưu đãi đặc biệt.</li>\n</ul>\n<p></p>\n",
      "<p>- Thẻ Master Happy Care là dòng thẻ đặc biệt và duy nhất trên thị trường hiện nay, kết hợp hoàn hảo giữa thẻ bảo hiểm chăm sóc sức khỏe chất lượng cao của Bảo hiểm HD và thẻ tín dụng Master của Ngân hàng HDBank nhằm tạo sự thuận tiện và các tiện ích tối ưu cho khách hàng.</p>\n<p>- Thẻ Master Happy Care được Ngân hàng HDBank phát hành dựa trên hợp đồng bảo hiểm của Bảo hiểm HD và các khách hàng, với các đặc tính và quyền lợi như sau dành cho chủ thẻ:</p>\n<ul>\n<li>Cấp hạn mức thẻ tín dụng thẻ Master tối thiểu từ 20tr – 30tr&nbsp;&nbsp;</li>\n<li>Miễn phí phát hành thẻ;</li>\n<li>Miễn phí thường niên;</li>\n<li>Mua bảo hiểm chăm sóc sức khỏe Master Happy Care cho người thân (bố/mẹ/vợ/chồng/con) với chương trình trả góp lãi suất 0%;</li>\n<li>Mua các loại hình bảo hiểm khác của Bảo hiểm HD với chương trình ưu đãi đặc biệt.</li>\n</ul>\n"
   ],
   "aeoal7bv":[
      "<p>- Các dịch vụ gia tăng được tích hợp miễn phí cho Chủ thẻ bao gồm các dịch vụ sau:</p>\n<ul>\n<li>Cấp hạn mức thẻ tín dụng thẻ Master tối thiểu từ 20tr – 30tr&nbsp;&nbsp;</li>\n<li>Mua bảo hiểm chăm sóc sức khỏe Master Happy Care cho người thân (bố/mẹ/vợ/chồng/con) với chương trình trả góp lãi suất 0%;</li>\n<li>Mua các loại hình bảo hiểm khác của Bảo hiểm HD với chương trình ưu đãi đặc biệt.</li>\n<li>Tư vấn và khám bác sĩ trực tuyến 24/24 tiêu chuẩn Singapre – không giới hạn số lần khám trong năm;</li>\n<li>Dịch vụ cứu hộ xe gắn máy (&lt;175 CC) tiêu chuẩn Nhật Bản – phạm vi 63 tỉnh thành trên cả nước – thời hạn 01 năm;</li>\n<li>Dịch vụ cứu hộ ô tô tiêu chuẩn Đức - thời hạn 01 năm.&nbsp;</li>\n<li>Nhiều ưu đãi khác trong tập đoàn như: mua vé máy bay giá rẻ, mua bất động sản giá ưu đãi, nghỉ dưỡng giá ưu đãi,...</li>\n</ul>\n",
      "<p>- Các dịch vụ gia tăng được tích hợp miễn phí cho Chủ thẻ bao gồm các dịch vụ sau:</p>\n<ul>\n<li>Cấp hạn mức thẻ tín dụng thẻ Master tối thiểu từ 20tr – 30tr&nbsp;&nbsp;</li>\n<li>Mua bảo hiểm chăm sóc sức khỏe Master Happy Care cho người thân (bố/mẹ/vợ/chồng/con) với chương trình trả góp lãi suất 0%;</li>\n<li>Mua các loại hình bảo hiểm khác của Bảo hiểm HD với chương trình ưu đãi đặc biệt.</li>\n<li>Tư vấn và khám bác sĩ trực tuyến 24/24 tiêu chuẩn Singapre – không giới hạn số lần khám trong năm;</li>\n<li>Dịch vụ cứu hộ xe gắn máy (&lt;175 CC) tiêu chuẩn Nhật Bản – phạm vi 63 tỉnh thành trên cả nước – thời hạn 01 năm;</li>\n<li>Dịch vụ cứu hộ ô tô tiêu chuẩn Đức - thời hạn 01 năm.&nbsp;</li>\n<li>Nhiều ưu đãi khác trong tập đoàn như: mua vé máy bay giá rẻ, mua bất động sản giá ưu đãi, nghỉ dưỡng giá ưu đãi,...</li>\n</ul>\n"
   ],
   "l7irj2rc":[
      "1. Bảo lãnh viện phí là gì?",
      "1. Bảo lãnh viện phí là gì?"
   ],
   "josy5xaw":[
      "<p>Đơn vị Bồi thường: <strong>Công ty TNHH Insmart </strong><br>Hotline:<strong> 1900.636.730</strong><br>Địa chỉ: <br>-   Thành phố Hồ Chí Minh: Lầu 9, Tòa nhà Đinh Lễ, 01 Đinh Lễ, Phường 12, Quận 4, TPHCM<br>-   Thành phố Hà Nội: Lầu 25, Tòa nhà Ngọc Khánh, 01 Phạm Huy Thông, Quận Ba Đình, Hà Nội<br>Trong vòng 365 ngày kể từ ngày điều trị, khách hàng gửi toàn bộ các chứng từ đã được hướng dẫn nêu trên về địa chỉ của Đơn vị bồi thường. <br>Thời gian xét duyệt hồ sơ bồi thường là tối đa 15 ngày làm việc kể từ ngày nhận được đầy đủ hồ sơ hợp lệ và thông tin cần thiết.</p>\n",
      "<p>Đơn vị Bồi thường: <strong>Công ty TNHH Insmart </strong><br>Hotline:<strong> 1900.636.730</strong><br>Địa chỉ: <br>-   Thành phố Hồ Chí Minh: Lầu 9, Tòa nhà Đinh Lễ, 01 Đinh Lễ, Phường 12, Quận 4, TPHCM<br>-   Thành phố Hà Nội: Lầu 25, Tòa nhà Ngọc Khánh, 01 Phạm Huy Thông, Quận Ba Đình, Hà Nội<br>Trong vòng 365 ngày kể từ ngày điều trị, khách hàng gửi toàn bộ các chứng từ đã được hướng dẫn nêu trên về địa chỉ của Đơn vị bồi thường. <br>Thời gian xét duyệt hồ sơ bồi thường là tối đa 15 ngày làm việc kể từ ngày nhận được đầy đủ hồ sơ hợp lệ và thông tin cần thiết.</p>"
   ],
   "faq_t2":[
      "Điều kiện tham gia bảo hiểm",
      "Điều kiện tham gia bảo hiểm"
   ],
   "i7xqk3zl":[
      "Quyền lợi chủ thẻ Master Happy Care",
      "Quyền lợi chủ thẻ Master Happy Care"
   ],
   "flob8q3d":[
      "từ đội ngũ CSKH của Bảo hiểm HD",
      "từ đội ngũ CSKH của Bảo hiểm HD"
   ],
   "sqcgvcsn":[
      "Tra cứu",
      "Tra cứu"
   ],
   "yrxrpiur":[
      "Giới thiệu chương trình",
      "Giới thiệu chương trình"
   ],
   "adfqb50i":[
      "Xem giấy chứng nhận",
      "Xem giấy chứng nhận"
   ],
   "40vyan8k":[
      "2. Tôi có cần phải kiểm tra sức khỏe trước khi tham gia đơn bảo hiểm này không?",
      "2. Tôi có cần phải kiểm tra sức khỏe trước khi tham gia đơn bảo hiểm này không?"
   ],
   "kuga7gsp":[
      "https://hdinsurance.com.vn",
      "https://hdinsurance.com.vn"
   ],
   "o5igm7hr":[
      "Miễn phí cứu hộ xe gắn máy, xe Ô Tô 24/7 tiêu chuẩn Quốc Tế",
      "Miễn phí cứu hộ xe gắn máy, xe Ô Tô 24/7 tiêu chuẩn Quốc Tế"
   ],
   "6vuapatp":[
      "Hướng dẫn yêu cầu bồi thường",
      "Hướng dẫn yêu cầu bồi thường"
   ],
   "xfzregcg":[
      "Credit Card + Heath Care = 1, sản phẩm đột phá và duy nhất tại Việt Nam ",
      "Credit Card + Heath Care = 1, sản phẩm đột phá và duy nhất tại Việt Nam "
   ],
   "faq_t1":[
      "Thông tin chung",
      "Thông tin chung"
   ],
   "t9joldao":[
      "Miễn phí, không giới hạn khám bác sĩ online tiêu chuẩn quốc tế",
      "Miễn phí, không giới hạn khám bác sĩ online tiêu chuẩn quốc tế"
   ],
   "mao0ps6u":[
      "Tư vấn bồi thường 24/7 bởi Insmart",
      "Tư vấn bồi thường 24/7 bởi Insmart"
   ],
   "rb6erxbe":[
      "Số tiền tối đa mà Khách hàng được hưởng hỗ trợ cho chi phí phát sinh?",
      "Số tiền tối đa mà Khách hàng được hưởng hỗ trợ cho chi phí phát sinh?"
   ],
   "obhetddv":[
      "<p><strong>Quy trình yêu cầu bồi thường với trường hợp khám ngoài hệ thống bảo lãnh viện phí:</strong></p>\n<p>1.\tNgười được bảo hiểm khám chữa bệnh tại bệnh viện và tự trả tiền bằng tiền mặt hoặc thẻ Master Happy Care. Sau đó thu thập bản gốc hóa đơn, các chứng từ liên quan và gửi yêu cầu bồi thường cho Insmart/Bảo hiểm HD</p>\n<p>2.\tInsmart/Bảo hiểm HD kiểm tra hóa đơn, chứng từ người được bảo hiểm gửi. Insmart sẽ gọi điện cho khách hàng/người được bảo hiểm để xử lý bồi thường.</p>\n<p>3.\tInsmart/Bảo hiểm HD thanh toán tiền bồi thường cho người được bảo hiểm hoặc hoàn tiền qua thẻ Master Happy Care</p>\n<p></p>\n",
      "<p><strong>Quy trình yêu cầu bồi thường với trường hợp khám ngoài hệ thống bảo lãnh viện phí:</strong></p>\n<p>1.\tNgười được bảo hiểm khám chữa bệnh tại bệnh viện và tự trả tiền bằng tiền mặt hoặc thẻ Master Happy Care. Sau đó thu thập bản gốc hóa đơn, các chứng từ liên quan và gửi yêu cầu bồi thường cho Insmart/Bảo hiểm HD</p>\n<p>2.\tInsmart/Bảo hiểm HD kiểm tra hóa đơn, chứng từ người được bảo hiểm gửi. Insmart sẽ gọi điện cho khách hàng/người được bảo hiểm để xử lý bồi thường.</p>\n<p>3.\tInsmart/Bảo hiểm HD thanh toán tiền bồi thường cho người được bảo hiểm hoặc hoàn tiền qua thẻ Master Happy Care</p>\n<p></p>\n"
   ],
   "natwm9d7":[
      "<p>- Mọi công dân Việt Nam hoặc người nước ngoài cư trú tại Việt Nam từ 18 tuổi đến 65 năm tuổi tại thời điểm bắt đầu hiệu lực của Đơn bảo hiểm. Người được bảo hiểm có thể được tham gia tái tục đến 70 tuổi.&nbsp;</p>\n<p>- Trẻ em từ đủ 15 ngày tuổi đến dưới 18 tuổi chỉ được tham gia bảo hiểm với điều kiện bố hoặc mẹ mua bảo hiểm cùng con và Chương trình bảo hiểm của bố hoặc mẹ phải tương đương hoặc cao hơn Chương trình bảo hiểm của con.&nbsp;</p>\n<p>- Không bị bệnh tâm thần, bệnh ung thư, bệnh phong vào ngày bắt đầu thời hạn bảo hiểm.&nbsp;</p>\n<p>- Không bị thương tật vĩnh viễn từ 70% trở lên tại Ngày hiệu lực bảo hiểm.</p>\n",
      "<p>- Mọi công dân Việt Nam hoặc người nước ngoài cư trú tại Việt Nam từ 18 tuổi đến 65 năm tuổi tại thời điểm bắt đầu hiệu lực của Đơn bảo hiểm. Người được bảo hiểm có thể được tham gia tái tục đến 70 tuổi.&nbsp;</p>\n<p>- Trẻ em từ đủ 15 ngày tuổi đến dưới 18 tuổi chỉ được tham gia bảo hiểm với điều kiện bố hoặc mẹ mua bảo hiểm cùng con và Chương trình bảo hiểm của bố hoặc mẹ phải tương đương hoặc cao hơn Chương trình bảo hiểm của con.&nbsp;</p>\n<p>- Không bị bệnh tâm thần, bệnh ung thư, bệnh phong vào ngày bắt đầu thời hạn bảo hiểm.&nbsp;</p>\n<p>- Không bị thương tật vĩnh viễn từ 70% trở lên tại Ngày hiệu lực bảo hiểm.</p>\n"
   ],
   "eg74suka":[
      "6. Danh sách bệnh viện được bảo lãnh viện phí",
      "6. Danh sách bệnh viện được bảo lãnh viện phí"
   ],
   "rpsxzfgm":[
      "Bồi thường",
      "Bồi thường"
   ],
   "bbn5ylyq":[
      "Đóng",
      "Đóng"
   ],
   "hklly7fi":[
      "Thời gian hiệu lực:",
      "Thời gian hiệu lực:"
   ],
   "287yieqb":[
      "ĐẶC QUYỀN CHO THÀNH VIÊN",
      "ĐẶC QUYỀN CHO THÀNH VIÊN"
   ],
   "yujdkcfo":[
      "Phạm vi bảo hiểm cho mọi nhu cầu doanh nghiệp",
      "Phạm vi bảo hiểm cho mọi nhu cầu doanh nghiệp"
   ],
   "dxahmeau":[
      "Yêu cầu bồi thường",
      "Yêu cầu bồi thường"
   ],
   "fmcn21fv":[
      "<p>- Khách hàng không cần phải kiểm tra sức khỏe trước khi tham gia bảo hiểm trừ trường hợp đặc biệt.&nbsp;</p>\n",
      "<p>- Khách hàng không cần phải kiểm tra sức khỏe trước khi tham gia bảo hiểm trừ trường hợp đặc biệt.&nbsp;</p>\n"
   ],
   "kwtluwwr":[
      "1. Độ tuổi và điều kiện nào được phép tham gia đơn bảo hiểm MASTER HAPPY CARE? ",
      "1. Độ tuổi và điều kiện nào được phép tham gia đơn bảo hiểm MASTER HAPPY CARE? "
   ]
}
const Index = (pageProps) => {
 const router = useRouter();
 if(pageProps.page_error || pageProps.initPage.page_status == "deactive"){

    return(<ErrorPage/>)
  }


  global.lng = new Language(json_lang);
  globalStore.set("pageconfig", pageProps.initPage.page_config);
  globalStore.set("page_version", pageProps.initPage.page_version);
  global.page_version = pageProps.initPage.page_version;

  const pageStatus = pageProps.initPage.page_status;
  const [pageConfig, setPageConfig] = useState(pageProps.initPage);
  const [pageError, setPageError] = useState(pageProps.page_error);

  const [preload, setPreload] = useState(true);
  const [preloadClass, setPreloadClass] = useState("page-loader");
  const [org, setOrg] = useState(pageProps.initPage.org_code_default?pageProps.initPage.org_code_default.toUpperCase():"");


  useEffect(() => {

  if (org == "VIETJET_VN") {
      window.location.href = "/search-insurance-policy.hdi";
  }else  if (org == "HDBANK_VN" || org == "PHULONG" || org == "HDSAISON") {
      window.location.href = "/promotions-packages-for-employees.hdi";
  } 

  }, [pageProps.page_org]);



  




    return (
      <>
        <Head>
          <title>{pageConfig.meta_data.page_title}</title>
          <meta name="viewport" content="user-scalable=no, width=device-width" />
          <link rel="stylesheet" href="/css/app.css" />
          <meta name="keywords" content={pageConfig.meta_data.page_keywords}/>
          <meta name="description" content={pageConfig.meta_data.page_description}/>
          <meta property="og:title" content={pageConfig.meta_data.page_title}/>
          <meta property="og:description" content={pageConfig.meta_data.page_description}/>
          <meta property="og:name" content={org}/>
          {pageConfig.meta_data.page_thumbnail&&<meta property="og:image" content={pageConfig.page_thumbnail}/>}
          {pageConfig.meta_data.page_thumbnail&&<meta name="twitter:image" content={pageConfig.page_thumbnail}/>}
        </Head>
        {org == "HDIMASTER" && <MainVisa />}
        {org == "ECLAIM" && <MainEclaim pageConfig={pageProps.initPage} router_query={router.query}/>}


      </>
    );
  
};

export async function getServerSideProps(ctx){

  var language = "vi"
  try{
    var domainpath = ""
    var domainhost = ""
    var page_error = false
    var product_config = {}
    var query = {}

    if(ctx.req){
      language = ctx.req.cookies.current_language
      domainpath = ctx.req._parsedOriginalUrl.pathname
      domainhost = ctx.req.headers.host
      query = ctx.req.query
    }else{
      domainpath = ctx.pathname
      domainhost = ctx.headers.host
      query = ctx.query
    }

    var page_component_object = null
    var initPage = await Utils.initPage(domainhost, domainpath, language)
    if(initPage.permalink){
       return { props: { domainpath: domainpath, initPage: initPage, siteurl: domainhost,  page_error: page_error  } }
    }else{
       return { props: { domainpath: null, initPage: {meta_data:{page_title:"Page is error"}}, page_component_object: {}, page_error: true } }
    }
   
  }catch(e){
    console.log("page Error ", e)
    return { props: { domainpath: null, initPage: {meta_data:{page_title:"Page is error"}}, page_component_object: {}, page_error: true } }
  }
}


export default Index;
